package in.nogyo.nogyoapp.mystore;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import in.nogyo.nogyoapp.Constants;
import in.nogyo.nogyoapp.R;
import in.nogyo.nogyoapp.SKUresultList.SKUListActivity;
import in.nogyo.nogyoapp.SKUresultList.SKUTabbed.TabbedSKUActivity;
import in.nogyo.nogyoapp.common.CommonUtils;
import in.nogyo.nogyoapp.models.Store;
import in.nogyo.nogyoapp.models.User;
import in.nogyo.nogyoapp.order.OrderHistoryActivity;
import in.nogyo.nogyoapp.rest.RestUtil;
import in.nogyo.nogyoapp.store.StoreUtil;
import lombok.Getter;
import lombok.Setter;

public class MyStoreActivity extends AppCompatActivity {

    private User storeUser;
    private GetUserTask task;
    private Store store;
    private User sessionUser;
    private PopupWindow pw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_my_store);

            store = RestUtil.mapper.readValue((String) getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).
                    getAll().get(Constants.SELECTED_STORE), Store.class);

            sessionUser = RestUtil.mapper.readValue(
                    (String) getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).getAll().get(Constants.SESSION_USER), User.class);

            ((TextView)(findViewById(R.id.storeName))).setText(store.getName());
            ((TextView)(findViewById(R.id.storeOwnerName))).setText(store.getContactPerson());
            ((TextView)(findViewById(R.id.storeContactNumber))).setText(store.getPhoneNo());
            ((TextView)(findViewById(R.id.storeAddress))).setText(store.getAddress());
            ((TextView)(findViewById(R.id.storeCity))).setText(store.getLocality().getCity().getName());
            ((TextView)(findViewById(R.id.storeState))).setText(store.getLocality().getCity().getState());
            ((TextView)(findViewById(R.id.storePincode))).setText(store.getLocality().getPincode());
            ((Button)(findViewById(R.id.showCollectCash))).setVisibility((sessionUser.isAdmin() || sessionUser.isSalesPerson()) ? View.VISIBLE : View.GONE);
            task = new GetUserTask(store,this);
            task.execute((Void)null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void goToSkuList(View v){
//        startActivity(CommonUtils.navigate(MyStoreActivity.this, SKUListActivity.class));
        startActivity(CommonUtils.navigate(MyStoreActivity.this, TabbedSKUActivity.class));
    }

    public void goToOrderHistory(View v){
        startActivity(CommonUtils.navigate(MyStoreActivity.this, OrderHistoryActivity.class));
    }


    public void showCollectCash(View v){

        if(this.task.getStatus().equals(AsyncTask.Status.FINISHED)) {

            ColorDrawable dw = new ColorDrawable(0xb0000000);
            pw = new PopupWindow(this.getLayoutInflater().inflate(R.layout.collect_cash_popup, null), ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, true);
            pw.setBackgroundDrawable(new ColorDrawable(0xbFFFFFFF));
            pw.setOutsideTouchable(false);
            pw.showAtLocation(this.findViewById(R.id.activityMyStore), Gravity.CENTER, 0, 0);
        }
    }

    public void collectCash(View v){

        try{
            ((ProgressBar)pw.getContentView().findViewById(R.id.cashCollectProgress)).setVisibility(View.VISIBLE);
            Double amount = Double.parseDouble(((EditText) pw.getContentView().findViewById(R.id.amount)).getText().toString());
            CollectCashTask task = new CollectCashTask(amount,this);
            task.execute((Void) null);
        }catch (Exception e){
            Toast.makeText(getBaseContext(), "Unable to add credit", Toast.LENGTH_SHORT).show();
            Log.e("ERROR","Exception in collection cash",e);
        }
    }

    @Override
    public void onBackPressed() {

        if(pw != null && pw.isShowing()){
            pw.dismiss();
            return;
        }
        SharedPreferences.Editor editor = getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).edit();
        editor.remove(Constants.SELECTED_STORE);
        editor.commit();
        super.onBackPressed();
    }

    @Getter @Setter
    public class GetUserTask extends AsyncTask<Void, Void, User> {

        private Store store;
        private Activity activity;

        public GetUserTask (Store store, Activity activity){
            this.store = store;
            this.activity = activity;
        }

        @Override
        protected User doInBackground(Void... params) {

            if(sessionUser.isStore())
                return  sessionUser;

            return StoreUtil.getStoreUser(store,activity);
        }

        @Override
        protected void onPostExecute(User result){
            storeUser = result;
            ((TextView)(findViewById(R.id.storePendingBalance))).setText(result.getUserWallet().getCurrentBalance().toString());
        }
    }

    @Getter @Setter
    public class CollectCashTask extends AsyncTask<Void,Void,Boolean>{

        private Double amount;
        private Activity activity;

        public CollectCashTask(Double amount, Activity activity){
            this.amount = amount;
            this.activity = activity;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            StoreUtil.collectCash(store,activity,amount);
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result){
            GetUserTask task = new GetUserTask(store,activity);
            try {
                ((ProgressBar)pw.getContentView().findViewById(R.id.cashCollectProgress)).setVisibility(View.GONE);
                ((TextView)pw.getContentView().findViewById(R.id.cashCollectStatus)).setText("SUCCESSFULL");
                task.execute((Void)null);
                if (pw.isShowing()) {
                    pw.dismiss();
                }
                Toast.makeText(getBaseContext(), "credit added successfully!!!!", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
