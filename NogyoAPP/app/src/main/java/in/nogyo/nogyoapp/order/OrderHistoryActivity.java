package in.nogyo.nogyoapp.order;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import in.nogyo.nogyoapp.Constants;
import in.nogyo.nogyoapp.R;
import in.nogyo.nogyoapp.common.CommonUtils;
import in.nogyo.nogyoapp.models.Order;
import in.nogyo.nogyoapp.models.Store;
import in.nogyo.nogyoapp.mystore.MyStoreActivity;
import in.nogyo.nogyoapp.rest.RestUtil;
import lombok.Getter;
import lombok.Setter;


@Getter @Setter
public class OrderHistoryActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private ListView listView;
    private List<Order> orderList;
    private OrderHistoryAdapter orderHistoryAdapter;
    private boolean fetchData = true;
    private boolean loadingMore = false;
    private Store store;
    private TextView noOrdersText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_order_history);
            listView = (ListView)findViewById(R.id.ordersList);
            this.orderList = new ArrayList<>();
            progressBar = (ProgressBar)findViewById(R.id.orderHistoryProgress);
            noOrdersText = (TextView)findViewById(R.id.no_orders_text);
            this.store = RestUtil.mapper.readValue((String) getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).
                    getAll().get(Constants.SELECTED_STORE), Store.class);
            listView.setOnScrollListener(new OrderHistoryScrollChangeListener(this));
            this.orderHistoryAdapter = new OrderHistoryAdapter(orderList,this);
            listView.setAdapter(orderHistoryAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View v, int position,
                                        long id) {

                    Order order = (Order) parent.getAdapter().getItem(position);
                    try {
                        SharedPreferences.Editor editor = getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).edit();
                        editor.putString(Constants.ORDER_SELECTED, RestUtil.mapper.writeValueAsString(order));
                        editor.commit();
                        Intent intent = CommonUtils.navigate(OrderHistoryActivity.this, OrderActivity.class);
                        intent.putExtra(Constants.CALLER_ACTIVITY, Constants.ACTIVITY_ORDER_HISTORY);
                        startActivity(intent);
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), R.string.unable_to_select_store, Toast.LENGTH_LONG).show();
                        Log.v("SCHEMA", "onItemClick Nuked!");
                    }
                }
            });
//            OrderHistoryPopulateTask task = new OrderHistoryPopulateTask(store.getId(),30,0,this);
//            task.execute((Void) null);

        } catch (Exception e) {
            Log.e("ERROR","Something went wrong terribly",e);
        }

    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }
}
