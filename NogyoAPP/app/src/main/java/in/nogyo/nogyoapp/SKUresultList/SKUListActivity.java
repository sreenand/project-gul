package in.nogyo.nogyoapp.SKUresultList;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import in.nogyo.nogyoapp.Constants;
import in.nogyo.nogyoapp.R;
import in.nogyo.nogyoapp.checkout.CheckoutActivity;
import in.nogyo.nogyoapp.common.CommonUtils;
import in.nogyo.nogyoapp.models.Product;
import in.nogyo.nogyoapp.models.Store;
import in.nogyo.nogyoapp.rest.RestUtil;
import in.nogyo.nogyoapp.store.StoreActivity;
import lombok.Getter;
import lombok.Setter;


@Getter @Setter
public class SKUListActivity extends AppCompatActivity {

    private List<Product> skuList;
    private SKUPopulateTask skuPopulateTask;
    private LinearLayout listView;
    private View mProgressView;
    private String language;
    private boolean postExecuteComplete;
    private Store store;

    public View getmProgressView() {
        return mProgressView;
    }

    public void setmProgressView(View mProgressView) {
        this.mProgressView = mProgressView;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_skulist);
            mProgressView = findViewById(R.id.sku_list_progress);
            mProgressView.setVisibility(View.VISIBLE);
            store = RestUtil.mapper.readValue(
                    (String)getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).getAll().get(Constants.SELECTED_STORE), Store.class);
            language = (String)getSharedPreferences(Constants.SESSION_DATA,Context.MODE_PRIVATE).getAll().get(Constants.LANGUAGE_SELECTED);
            listView = (LinearLayout)findViewById(R.id.sku_list);
            skuList = new ArrayList<Product>();
            skuPopulateTask = new SKUPopulateTask(this);
            skuPopulateTask.execute((Void) null);
//            List<ImageView> imageViews = skuPopulateTask.getImagesList();
//            List<String> urls = skuPopulateTask.getUrls();
//            for(ImageView imageView: imageViews){
//                Picasso.with(this).load(urls.get(imageViews.indexOf(imageView))).into(imageView);
//            }
        } catch (Exception e) {
            startActivity(CommonUtils.navigate(this, StoreActivity.class));
        }
    }


    @Override
    public void onBackPressed() {
        skuPopulateTask.getImagePopulateTask().cancel(true);
        skuPopulateTask.cancel(true);
        super.onBackPressed();
        //startActivity(CommonUtils.navigate(SKUListActivity.this,StoreActivity.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_skulist, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_checkout:
                List<Product> cart = new ArrayList<>();
                for(Product sku : skuList){
                    if(sku.isSelected()){
                        cart.add(sku);
                    }
                }
                if(cart.size() == 0){
                    Toast.makeText(this, R.string.please_select_something, Toast.LENGTH_SHORT)
                            .show();
                    return false;
                }
                SharedPreferences.Editor editor = getSharedPreferences(Constants.SESSION_DATA,Context.MODE_PRIVATE).edit();
                try {
                    editor.putString(Constants.SESSION_CART, RestUtil.mapper.writeValueAsString(cart));
                    editor.commit();
                    startActivity(CommonUtils.navigate(SKUListActivity.this, CheckoutActivity.class));
                } catch (IOException e){
                    Log.e("ERROR","Error in serializing cart",e);
                }

                break;
            default:
                break;
        }

        return true;
    }


}
