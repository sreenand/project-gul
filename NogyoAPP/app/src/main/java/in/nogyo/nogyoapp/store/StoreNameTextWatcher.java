package in.nogyo.nogyoapp.store;

import android.app.Activity;
import android.os.AsyncTask;
import android.text.Editable;
import android.text.TextWatcher;

import java.util.List;

import in.nogyo.nogyoapp.models.Store;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by srinand.pc on 23/02/16.
 */

@Getter @Setter
public class StoreNameTextWatcher implements TextWatcher {

    private StorePopulateTask storePopulateTask;


    public StoreNameTextWatcher(StorePopulateTask task){
        this.storePopulateTask = task;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
//        StoreActivity.adapter.getFilter().filter(cs);
    }

    @Override
    public void afterTextChanged(Editable editable) {

//        storePopulateTask = new StorePopulateTask(storePopulateTask);
//        if (editable.toString() != null & editable.toString().length() > 3) {
//            storePopulateTask.setStoreName(editable.toString());
//            storePopulateTask.execute((Void) null);
//        }
    }
}
