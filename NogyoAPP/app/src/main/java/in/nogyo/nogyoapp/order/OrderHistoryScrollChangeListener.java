package in.nogyo.nogyoapp.order;

import android.content.Context;
import android.util.Log;
import android.widget.AbsListView;

import in.nogyo.nogyoapp.Constants;
import in.nogyo.nogyoapp.models.Store;
import in.nogyo.nogyoapp.rest.RestUtil;

/**
 * Created by srinandchallur on 06/04/16.
 */
public class OrderHistoryScrollChangeListener implements AbsListView.OnScrollListener {

    private OrderHistoryActivity orderHistoryActivity;

    public OrderHistoryScrollChangeListener(OrderHistoryActivity orderHistoryActivity) {
        this.orderHistoryActivity = orderHistoryActivity;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        int lastInScreen = firstVisibleItem + visibleItemCount;
        if((lastInScreen == totalItemCount) && !(orderHistoryActivity.isLoadingMore()) && orderHistoryActivity.isFetchData()){
            try{
                OrderHistoryPopulateTask task =
                        new OrderHistoryPopulateTask(orderHistoryActivity.getStore().getId(),10,orderHistoryActivity.getOrderList().size(),orderHistoryActivity);
                task.execute((Void) null);

            } catch (Exception e){
                Log.e("ERROR", "Unable to get order Data", e);
            }
        }
    }

}
