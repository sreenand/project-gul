package in.nogyo.nogyoapp.SKUresultList;

import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import in.nogyo.nogyoapp.common.CommonUtils;

/**
 * Created by srinandchallur on 04/04/16.
 */
public class ImagePopulateTask extends AsyncTask<Void, Void, Boolean> {


    private List<ImageView> imageViews;
    private List<String> urls;
    private Activity activity;


    public ImagePopulateTask(List<ImageView> views, List<String> urls, Activity activity){
        this.imageViews = views;
        this.urls = urls;
        this.activity = activity;
    }

    @Override
    protected Boolean doInBackground(Void... params) {

        for(ImageView imageView: imageViews){
            if(isCancelled())
                break;
            CommonUtils.setImage(imageView, urls.get(imageViews.indexOf(imageView)));
        }
        return true;
    }
}
