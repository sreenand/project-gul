package in.nogyo.nogyoapp.SKUresultList;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import java.util.List;
import java.util.Set;

import in.nogyo.nogyoapp.R;
import in.nogyo.nogyoapp.models.Product;
import in.nogyo.nogyoapp.models.ProductPricing;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by srinand.pc on 24/02/16.
 */

@Getter @Setter
public class SKUAddListener implements OnClickListener {

    private ProductPricing product;
    private Set<ProductPricing> selectedItems;
    private EditText quantity;

    public SKUAddListener(ProductPricing product, Set<ProductPricing> selectedItems,EditText quantity){
        this.product = product;
        this.quantity = quantity;
        this.selectedItems = selectedItems;
    }

    @Override
    public void onClick(View view) {
        Switch button = (Switch)view.findViewById(R.id.groceryAdd);
        int quantity = -1;
        try{
            quantity = Integer.parseInt(this.quantity.getText().toString());
        }catch (Exception e){

        }
        if(button.isChecked()){
            if(quantity <= 0){
                button.setChecked(false);
                return;
            }
            product.getProduct().setSelected(true);
            this.product.getProduct().setQuantity(quantity);
            this.quantity.setNextFocusDownId(R.id.groceryQuantity);
            this.quantity.setBackgroundResource(R.drawable.no_edittext_style);
            this.quantity.setEnabled(false);
            this.selectedItems.add(product);
        } else if(!button.isChecked()){
            this.quantity.setFocusable(true);
            this.quantity.requestFocus();
            this.quantity.setBackgroundResource(R.drawable.edittext_style);
            product.getProduct().setSelected(false);
            this.quantity.setEnabled(true);
            this.quantity.setText("");
            this.selectedItems.remove(product);
            this.getProduct().getProduct().setQuantity(0);
        }

    }
}
