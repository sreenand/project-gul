package in.nogyo.nogyoapp.menu;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;

import android.support.v4.widget.DrawerLayout;
import android.support.v4.app.ActionBarDrawerToggle;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toolbar;


import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import in.nogyo.nogyoapp.Constants;
import in.nogyo.nogyoapp.R;
import in.nogyo.nogyoapp.common.CommonUtils;
import in.nogyo.nogyoapp.login.LoginActivity;
import in.nogyo.nogyoapp.store.StoreActivity;

/**
 * Created by srinand.pc on 04/03/16.
 */

public abstract class BaseActivityWithMenu extends AppCompatActivity {


    protected enum SliderMenuItem {MYSTORES,LOGOUT};
    protected DrawerLayout mDrawerLayout;
    protected LinearLayout mDrawerList;
    protected ActionBarDrawerToggle mDrawerToggle;
    protected CharSequence mDrawerTitle;
    protected CharSequence mTitle;
    protected String[] navMenuTitles;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private Method method;




    protected void populateMenu() {

        for (NavDrawerItem item : navDrawerItems) {
            LayoutInflater mInflater = (LayoutInflater) getLayoutInflater();
            View inflatedView = mInflater.inflate(R.layout.menu_list_item, null);
            TextView textView = (TextView) inflatedView.findViewById(R.id.title);
            textView.setText(item.getTitle());
            mDrawerList.addView(inflatedView);
        }
    }


    protected void createMenu() {

        mDrawerList = (LinearLayout) findViewById(R.id.list_slidermenu);
        navMenuTitles = getResources().getStringArray(R.array.menu_items);
        mTitle = mDrawerTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);

        navDrawerItems = new ArrayList<NavDrawerItem>();

        for(int i=0;i<navMenuTitles.length;i++){
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[i], i));
        }

        mDrawerList.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
        NavDrawerListAdapter adapter = new NavDrawerListAdapter(this, navDrawerItems);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.mipmap.ic_menu_white, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
            }
        };

        LayoutInflater mInflater = (LayoutInflater)
                this.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View profileImageView = mInflater.inflate(R.layout.profile_image,null);

        mDrawerList.addView(profileImageView);

        for(NavDrawerItem item: navDrawerItems){

            mInflater = (LayoutInflater)
                    this.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            View convertView = mInflater.inflate(R.layout.menu_list_item,null);
            final TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
            txtTitle.setText(item.getTitle());
            txtTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    displayView(txtTitle);
                }
            });
            mDrawerList.addView(convertView);
        }


        //populateMenu();

        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_menu_white);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void displayView(View view) {
        TextView view1 = (TextView) view;
        displayView(SliderMenuItem.valueOf(view1.getText().toString().toUpperCase()));
    }

    private void displayView(SliderMenuItem menu) {

        mDrawerLayout.closeDrawer(mDrawerList);
        switch (menu) {
            case MYSTORES:
                this.finish();
                CommonUtils.navigateTo(this, StoreActivity.class);
                break;
            case LOGOUT:
                logOut();
                break;
            default:
                break;
        }
    }


    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public abstract void logOut();


}
