package in.nogyo.nogyoapp.order;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import in.nogyo.nogyoapp.Constants;
import in.nogyo.nogyoapp.R;
import in.nogyo.nogyoapp.SKUresultList.SKUTabbed.TabbedSKUActivity;
import in.nogyo.nogyoapp.common.CommonUtils;
import in.nogyo.nogyoapp.models.Order;
import in.nogyo.nogyoapp.models.OrderItem;
import in.nogyo.nogyoapp.models.Store;
import in.nogyo.nogyoapp.models.User;
import in.nogyo.nogyoapp.rest.RestUtil;
import in.nogyo.nogyoapp.store.StoreActivity;
import lombok.Getter;
import lombok.Setter;

import static in.nogyo.nogyoapp.Constants.CALLER_ACTIVITY;
import static in.nogyo.nogyoapp.Constants.ORDER_SELECTED;


@Getter @Setter
public class OrderActivity extends AppCompatActivity {

    private Order order;
    private List<OrderItem> orderItemList;
    private GridView orderGridView;
    private LinearLayout layout;
    private OrderAdapter adapter;
    private Store selectedStore;
    private User sessionUser;
    private boolean fetchData = true;
    private boolean loadingMore = false;
    private ListView listView;
    private OrderItemListAdapter orderItemListAdapter;
    private OrderItemOnScrollListener orderItemOnScrollListener;
    private String language;
    private String caller;
    private Button editOrderButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try{
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_order);
            orderItemList = new ArrayList<>();
            editOrderButton = (Button)findViewById(R.id.order_edit_button);
            Bundle extras = getIntent().getExtras();
            if(extras != null)
                caller = (String)extras.get(CALLER_ACTIVITY);
            listView = (ListView)findViewById(R.id.order_item_list);
            layout = (LinearLayout)findViewById(R.id.order_data_layout);

            language = (String)getSharedPreferences(Constants.SESSION_DATA,Context.MODE_PRIVATE).getAll().get(Constants.LANGUAGE_SELECTED);
            try {
                selectedStore = RestUtil.mapper.readValue(
                        (String) getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).getAll().get(Constants.SELECTED_STORE), Store.class);
                sessionUser = RestUtil.mapper.readValue(
                        (String) getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).getAll().get(Constants.SESSION_USER), User.class);
            } catch (Exception e){
                selectedStore = null;
            }
            //orderGridView = (GridView)findViewById(R.id.order_grid_view);

            OrderDataPopulateTask task = new OrderDataPopulateTask(this,null);
            populateOrder();
            populateOrderData();
            task.execute((Void) null);

            if((sessionUser.isAdmin() || sessionUser.isSalesPerson()) &&(this.order.getStatus().equals("RECEIVED"))){
                editOrderButton.setVisibility(View.VISIBLE);
            }else{
                editOrderButton.setVisibility(View.GONE);
            }

            this.orderItemListAdapter = new OrderItemListAdapter(orderItemList,this);
            orderItemOnScrollListener = new OrderItemOnScrollListener(this);
            this.listView.setOnScrollListener(this.orderItemOnScrollListener);
            this.listView.setAdapter(this.orderItemListAdapter);
        }catch (Exception e){
            Log.e("ERROR","Unable to load Order View",e);
        }
    }


    public void populateOrder() {
        Map<String,String> sessionData = (Map<String,String>)getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).getAll();
        try {
            this.order = RestUtil.mapper.readValue(sessionData.get(ORDER_SELECTED), Order.class);
        } catch (IOException e) {
            Toast.makeText(getBaseContext(), R.string.problem_loading_order_try_again, Toast.LENGTH_SHORT).show();
            Log.e("ERROR", "Unable to Open the Order");
            this.onBackPressed();
        }
    }


    private void populateOrderData(){

        View view1  = getLayoutInflater().inflate(R.layout.order_row,null);
        TextView viewHolder = (TextView)view1.findViewById(R.id.order_id_value);
        viewHolder.setText(this.order.getId().toString());
        viewHolder = (TextView)view1.findViewById(R.id.order_status_value);
        viewHolder.setText(this.order.getStatus().toString());
        this.layout.addView(view1);
    }


    public void orderAgain(View view) {
        startActivity(CommonUtils.navigate(OrderActivity.this, StoreActivity.class));
    }


    public void editOrder(View view) {

        Intent intent = CommonUtils.navigate(OrderActivity.this, TabbedSKUActivity.class);
        this.order.setOrderItems(this.orderItemList);
        try {
            intent.putExtra(Constants.ORDER_SELECTED,RestUtil.mapper.writeValueAsString(this.order));
            startActivity(intent);
        } catch (IOException e) {
            return;
        }
    }


    @Override
    public void onBackPressed(){
        this.finish();
        if(Constants.ACTIVITY_ORDER_HISTORY.equals(caller)) {
            super.onBackPressed();
            return;
        }
        startActivity(CommonUtils.navigate(OrderActivity.this, StoreActivity.class));
        return;
    }
}
