package in.nogyo.nogyoapp.SKUresultList.SKUTabbed;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import in.nogyo.nogyoapp.models.SubCategory;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by srinandchallur on 13/04/16.
 */

@Getter @Setter
class SKUViewTabAdapter extends FragmentPagerAdapter {

    private final List<SubCategoryFragment> mFragmentList = new ArrayList<>();

    public SKUViewTabAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(SubCategoryFragment fragment) {
        mFragmentList.add(fragment);

    }

    @Override
    public CharSequence getPageTitle(int position) {
        String language = mFragmentList.get(position).getTabbedSKUActivity().getSeletedLanguage();
        return mFragmentList.get(position).getSubCategory().getLocalName(language);
    }
}
