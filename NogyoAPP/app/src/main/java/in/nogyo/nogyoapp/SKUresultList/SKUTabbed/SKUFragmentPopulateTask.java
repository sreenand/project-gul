package in.nogyo.nogyoapp.SKUresultList.SKUTabbed;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import in.nogyo.nogyoapp.Constants;
import in.nogyo.nogyoapp.R;
import in.nogyo.nogyoapp.SKUresultList.ImagePopulateTask;
import in.nogyo.nogyoapp.SKUresultList.SKUAddListener;
import in.nogyo.nogyoapp.SKUresultList.SKUUtil;
import in.nogyo.nogyoapp.models.Product;
import in.nogyo.nogyoapp.models.ProductPricing;
import in.nogyo.nogyoapp.models.SubCategory;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by srinandchallur on 13/04/16.
 */

@Getter @Setter @NoArgsConstructor
public class SKUFragmentPopulateTask extends AsyncTask<Void, Void, Boolean> {

    private SubCategoryFragment fragment;
    private List<ProductPricing> products;

    public SKUFragmentPopulateTask(SubCategoryFragment fragment){
        this.setFragment(fragment);
    }


    @Override
    protected Boolean doInBackground(Void... params) {

        SubCategory subCategory = fragment.getSubCategory();
        TabbedSKUActivity activity = fragment.getTabbedSKUActivity();
        List<ProductPricing> productsPopulated = activity.getPopulatedProducts().get(subCategory);

        if(productsPopulated == null || productsPopulated.isEmpty()){
            products = SKUUtil.getAllSKUPricingForSubCategory(fragment.getSubCategory(), fragment.getTabbedSKUActivity());
            Collections.sort(products, new Comparator() {
                @Override
                public int compare(Object sku1, Object sku2) {
                    //use instanceof to verify the references are indeed of the type in question
                    return ((ProductPricing) sku1).getProduct().getName()
                            .compareTo(((ProductPricing) sku2).getProduct().getName());
                }
            });
            activity.getPopulatedProducts().put(subCategory,products);
        } else{
            products = productsPopulated;
        }

        return true;
    }

    @Override
    protected void onPostExecute(final Boolean success) {

        if(success){
            try{
                for(ProductPricing sku: products ){
                    setQuantity(sku);
                    View view = fragment.getTabbedSKUActivity().getLayoutInflater().inflate(R.layout.grocery_list_row, null);
                    TextView name = (TextView)view.findViewById(R.id.groceryName);
                    EditText quantity = (EditText)view.findViewById(R.id.groceryQuantity);
                    Switch addButton = (Switch)view.findViewById(R.id.groceryAdd);
                    ImageView imageView = (ImageView)view.findViewById(R.id.skuImage);
                    if(sku.getPricePerUnit() != null) {
                        ((TextView) view.findViewById(R.id.pricePerUnit)).setText("\u20B9" + sku.getPricePerUnit().toString() +
                                "/" + sku.getProductUnit().getUnitMetric());
                        ((TextView) view.findViewById(R.id.actualpricePerUnit)).setText("\u20B9" + sku.getVariablePricing().get(0).discountedPrice+
                                "/" + sku.getProductUnit().getUnitMetric());
                        ((TextView) view.findViewById(R.id.kg_bucket_one)).setText(sku.getVariablePricing().get(0).min.intValue()+sku.getProductUnit().getUnitMetric() +" - " + sku.getVariablePricing().get(0).max.intValue()+
                                "" + sku.getProductUnit().getUnitMetric());

                        ((TextView) view.findViewById(R.id.pricePerUnit_bucket_two)).setText("\u20B9" + sku.getVariablePricing().get(1).discountedPrice+
                                "/" + sku.getProductUnit().getUnitMetric());
                        ((TextView) view.findViewById(R.id.kg_bucket_two)).setText("" + sku.getVariablePricing().get(1).min.intValue()+
                                sku.getProductUnit().getUnitMetric()+" - "+sku.getVariablePricing().get(1).max.intValue() + sku.getProductUnit().getUnitMetric());

                        ((TextView) view.findViewById(R.id.pricePerUnit_bucket_three)).setText("\u20B9" + sku.getVariablePricing().get(2).discountedPrice+
                                "/" + sku.getProductUnit().getUnitMetric());
                        ((TextView) view.findViewById(R.id.kg_bucket_three)).setText(">" + sku.getVariablePricing().get(2).min.intValue()+
                                "" + sku.getProductUnit().getUnitMetric());
                    }
                    String selectedLanguage = fragment.getTabbedSKUActivity().getSeletedLanguage();
                    if(selectedLanguage != null)
                        selectedLanguage = selectedLanguage.toLowerCase();
                    name.setText(sku.getProduct().getLocalName(selectedLanguage));
                    if(sku.getProduct().getQuantity() > 0) {
                        quantity.setText(String.valueOf(sku.getProduct().getQuantity()));
                        quantity.setNextFocusDownId(R.id.groceryQuantity);
                        quantity.setBackgroundResource(R.drawable.no_edittext_style);
                        quantity.setEnabled(false);
                        addButton.setChecked(true);
                    }
                    addButton.setOnClickListener(new SKUAddListener(sku, fragment.getTabbedSKUActivity().getSelectedItems(), quantity));
                    if (sku.getProduct().getImageUrl() != null && !sku.getProduct().getImageUrl().isEmpty()){
                        Picasso.with(fragment.getTabbedSKUActivity()).load(sku.getProduct().getImageUrl()).into(imageView);
                    }
                    fragment.getFragmentListLayout().addView(view);
                }

            }catch (Exception e){
                Log.e("Error in populating sku", e.getMessage(), e);
            }


        }
    }

    public void setQuantity(ProductPricing productPricing){

        if(fragment.getTabbedSKUActivity().getSelectedOrder() != null &&
                fragment.getTabbedSKUActivity().getProductIdToorderItemMap().containsKey(productPricing.getProduct().getId())){
            productPricing.getProduct().setQuantity(
                    fragment.getTabbedSKUActivity().getProductIdToorderItemMap().get(productPricing.getProduct().getId()).getQuantity());
            productPricing.getProduct().setSelected(true);
            this.getFragment().getTabbedSKUActivity().getSelectedItems().add(productPricing);
        }
    }

}

