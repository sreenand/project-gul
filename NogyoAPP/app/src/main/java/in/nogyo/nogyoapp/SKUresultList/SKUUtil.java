package in.nogyo.nogyoapp.SKUresultList;

import android.app.Activity;
import android.content.Context;

import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.nogyo.nogyoapp.Constants;
import in.nogyo.nogyoapp.models.Product;
import in.nogyo.nogyoapp.models.ProductPricing;
import in.nogyo.nogyoapp.models.SubCategory;
import in.nogyo.nogyoapp.rest.Response;
import in.nogyo.nogyoapp.rest.RestUtil;

/**
 * Created by srinand.pc on 23/02/16.
 */
public class SKUUtil {

    public static final Logger logger = LoggerFactory.getLogger(SKUUtil.class);

    public static List<Product> getAllSKUs(Activity activity){

        List<Product> products;

        products = (List<Product>)(activity.getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).
                getAll().get("SKUS"));

        if(products != null)
            return products;

        String sessionId = (String)(activity.getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).
                getAll().get(Constants.SESSION_ID));

        Response response = RestUtil.execute(Constants.backend_sku_url,
                null, null, "GET", 100, sessionId);

        try {
            products = RestUtil.mapper.readValue(response.getResponseBody(),
                    new TypeReference<List<Product>>(){});

            for(Product product : products){
                if(product.getLocalNames() != null){
                    product.setLocalNamesMap(RestUtil.mapper.readValue(product.getLocalNames(), HashMap.class));
                }
            }
        } catch (Exception e) {
            products = new ArrayList<>();
            logger.error("Error in getting store list from server",e);
        }
        return products;

    }

    public static List<SubCategory> getAllSubCategories(Long categoryId, Activity activity){

        List<SubCategory> subCategories;

        String sessionId = (String)(activity.getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).
                getAll().get(Constants.SESSION_ID));

        String backendUrl = Constants.backend_sku_url+"/category/"+categoryId+"/subcategories";
        Response response = RestUtil.execute(backendUrl,
                null, null, "GET", 100, sessionId);

        try {
            subCategories = RestUtil.mapper.readValue(response.getResponseBody(),
                    new TypeReference<List<SubCategory>>(){});

            for(SubCategory subCategory : subCategories){
                if(subCategory.getLocalNames() != null){
                    subCategory.setLocalNamesMap(RestUtil.mapper.readValue(subCategory.getLocalNames(), HashMap.class));
                }
            }
        } catch (Exception e) {
            subCategories = new ArrayList<>();
            logger.error("Error in getting store list from server",e);
        }
        return subCategories;

    }

    public static List<Product> getAllSKUForSubCategory(SubCategory subCategory, Activity activity){

        List<Product> products;

        String sessionId = (String)(activity.getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).
                getAll().get(Constants.SESSION_ID));

        Response response = RestUtil.execute(Constants.backend_sku_url+";jsessionid="+sessionId+"?sub_category_id="+subCategory.getId(),
                null, null, "GET", 100, null);

        try {
            products = RestUtil.mapper.readValue(response.getResponseBody(),
                    new TypeReference<List<Product>>(){});

            for(Product product : products){
                if(product.getLocalNames() != null){
                    product.setLocalNamesMap(RestUtil.mapper.readValue(product.getLocalNames(), HashMap.class));
                }
            }
        } catch (Exception e) {
            products = new ArrayList<>();
            logger.error("Error in getting store list from server",e);
        }
        return products;

    }

    public static List<ProductPricing> getAllSKUPricingForSubCategory(SubCategory subCategory, Activity activity){

        List<ProductPricing> productPricingList;

        String sessionId = (String)(activity.getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).
                getAll().get(Constants.SESSION_ID));

        Response response = RestUtil.execute(Constants.backend_sku_url+"/query;jsessionid="+
                        sessionId +"?sub_cat_id="+subCategory.getId()+"&city_id=1&enabled=true",
                null, null, "GET", 100, null);

        try {
            productPricingList = RestUtil.mapper.readValue(response.getResponseBody(),
                    new TypeReference<List<ProductPricing>>(){});

            for(ProductPricing productPricing : productPricingList){
                if(productPricing.getProduct().getLocalNames() != null){
                    productPricing.getProduct().setLocalNamesMap(RestUtil.mapper.readValue(productPricing.getProduct().getLocalNames(), HashMap.class));
                }
            }
        } catch (Exception e) {
            productPricingList = new ArrayList<>();
            logger.error("Error in getting store list from server",e);
        }
        return productPricingList;
    }
}
