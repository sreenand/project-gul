package in.nogyo.nogyoapp.SKUresultList.SKUTabbed;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import in.nogyo.nogyoapp.Constants;
import in.nogyo.nogyoapp.R;
import in.nogyo.nogyoapp.checkout.CheckoutActivity;
import in.nogyo.nogyoapp.common.CommonUtils;
import in.nogyo.nogyoapp.models.Order;
import in.nogyo.nogyoapp.models.OrderItem;
import in.nogyo.nogyoapp.models.Product;
import in.nogyo.nogyoapp.models.ProductPricing;
import in.nogyo.nogyoapp.models.SubCategory;
import in.nogyo.nogyoapp.rest.RestUtil;
import lombok.Getter;
import lombok.Setter;

import static in.nogyo.nogyoapp.Constants.CALLER_ACTIVITY;


@Getter @Setter
public class TabbedSKUActivity extends AppCompatActivity implements ObservableScrollViewCallbacks {


    private TabLayout tabLayout;
    private ViewPager viewPager;
    private String seletedLanguage;
    private Map<SubCategory,List<ProductPricing>> populatedProducts = new ConcurrentHashMap<>();
    private SKUViewTabAdapter adapter;
    private Set<ProductPricing> selectedItems;
    private Order selectedOrder;
    private Map<Long, OrderItem> productIdToorderItemMap;
    private List<SubCategory> subCategoryList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try {
            selectedItems = Collections.synchronizedSet(new HashSet<ProductPricing>());
            Bundle extras = getIntent().getExtras();
            if(extras != null) {
                String orderselected = (String) extras.get(Constants.ORDER_SELECTED);
                if(orderselected != null)
                   selectedOrder = RestUtil.mapper.readValue(orderselected, Order.class);
                   populateProductIdToOrderItemMap();
            }

            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_tabbed_sku);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            viewPager = (ViewPager)findViewById(R.id.containerViewPager);
            tabLayout = (TabLayout)findViewById(R.id.tabs);
            SubCategoryPopulateTask task = new SubCategoryPopulateTask(this);
            boolean result = task.execute((Void) null).get();
            subCategoryList = task.getSubCategoryList();
            seletedLanguage = (String)getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).getAll().get(Constants.LANGUAGE_SELECTED);

            adapter = new SKUViewTabAdapter(getSupportFragmentManager());
            for(SubCategory subCategory : subCategoryList){
                SubCategoryFragment fragment = new SubCategoryFragment();
                fragment.setSubCategory(subCategory);
                fragment.setTabbedSKUActivity(this);
                adapter.getMFragmentList().add(fragment);

            }
            viewPager.setAdapter(adapter);
            tabLayout.setupWithViewPager(viewPager);

        } catch (Exception e){
            Log.e("ERROR","Exception in tabbed sku activity",e);
        }


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //startActivity(CommonUtils.navigate(SKUListActivity.this,StoreActivity.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_skulist, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_checkout:
                List<ProductPricing> cart = new ArrayList<>();
                for(ProductPricing sku : selectedItems){
                    if(sku.getProduct().isSelected()){
                        cart.add(sku);
                    }
                }
                if(cart.size() == 0){
                    Toast.makeText(this, R.string.please_select_something, Toast.LENGTH_SHORT)
                            .show();
                    return false;
                }

//                if(this.selectedOrder != null && subCategoryList.size() != populatedProducts.size()){
//                    Toast.makeText(this, R.string.verify_all_items, Toast.LENGTH_LONG)
//                            .show();
//                    return false;
//                }
                SharedPreferences.Editor editor = getSharedPreferences(Constants.SESSION_DATA,Context.MODE_PRIVATE).edit();
                try {
                    editor.putString(Constants.SESSION_CART, RestUtil.mapper.writeValueAsString(cart));
                    editor.commit();
                    Intent intent = CommonUtils.navigate(TabbedSKUActivity.this, CheckoutActivity.class);
                    if(this.getSelectedOrder() != null){
                        intent.putExtra(Constants.ORDER_SELECTED,RestUtil.mapper.writeValueAsString(this.getSelectedOrder()));
                    }
                    startActivity(intent);
                } catch (IOException e){
                    Log.e("ERROR","Error in serializing cart",e);
                }

                break;
            default:
                break;
        }

        return true;
    }


    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {

    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

        ActionBar ab = getSupportActionBar();
        if (ab == null) {
            return;
        }
        if (scrollState == ScrollState.UP) {
            if (ab.isShowing()) {
                ab.hide();
            }
        } else if (scrollState == ScrollState.DOWN) {
            if (!ab.isShowing()) {
                ab.show();
            }
        }
    }


    public void populateProductIdToOrderItemMap(){
        List<OrderItem> orderItemList = this.selectedOrder.getOrderItems();
        this.productIdToorderItemMap = new HashMap<>();
        for( OrderItem orderItem : orderItemList){
            this.productIdToorderItemMap.put(orderItem.getProduct().getId(), orderItem);
        }
    }
}
