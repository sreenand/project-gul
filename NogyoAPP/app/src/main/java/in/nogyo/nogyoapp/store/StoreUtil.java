package in.nogyo.nogyoapp.store;

import android.app.Activity;
import android.content.Context;

import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.nogyo.nogyoapp.Constants;
import in.nogyo.nogyoapp.login.LoginActivity;
import in.nogyo.nogyoapp.models.Store;
import in.nogyo.nogyoapp.models.User;
import in.nogyo.nogyoapp.rest.Response;
import in.nogyo.nogyoapp.rest.RestUtil;

/**
 * Created by srinand.pc on 23/02/16.
 */
public class StoreUtil {

    private static final Logger logger = LoggerFactory.getLogger(StoreUtil.class);
    public static List<Store> getStores(String name, Activity activity){

        List<Store> storeList = null;
        String sessionId = (String)(activity.getSharedPreferences(Constants.SESSION_DATA,Context.MODE_PRIVATE).
                getAll().get(Constants.SESSION_ID));


        Response response = RestUtil.execute(Constants.backend_stores_url+"/name/"+ URLEncoder.encode(name),
                null,null,"GET",100,sessionId);

        try {
            storeList = RestUtil.mapper.readValue(response.getResponseBody(),
                    new TypeReference<List<Store>>(){});
        } catch (Exception e) {
            storeList = new ArrayList<>();
            logger.error("Error in getting store list from server",e);
        }
        return storeList;
    }

    public static List<Store> getMyStores(Activity activity){

        List<Store> storeList = null;
        String sessionId = (String)(activity.getSharedPreferences(Constants.SESSION_DATA,Context.MODE_PRIVATE).
                getAll().get(Constants.SESSION_ID));


        Response response = RestUtil.execute(Constants.backend_stores_url+"/mystores;jsessionid=" + sessionId+"?limit=200&offset=0",
                null,null,"GET",100,null);

        try {
            storeList = RestUtil.mapper.readValue(response.getResponseBody(),
                    new TypeReference<List<Store>>(){});
        } catch (Exception e) {
            storeList = new ArrayList<>();
            logger.error("Error in getting store list from server",e);
        }
        return storeList;
    }

    public static List<Store> getAllStores(Activity activity){

        List<Store> storeList = null;
        String sessionId = (String)(activity.getSharedPreferences(Constants.SESSION_DATA,Context.MODE_PRIVATE).
                getAll().get(Constants.SESSION_ID));


        Response response = RestUtil.execute(Constants.backend_all_stores_url+";jsessionid="+sessionId+"?limit=200&offset=0&city_id=1",
                null,null,"GET",100,null);

        try {
            storeList = RestUtil.mapper.readValue(response.getResponseBody(),
                    new TypeReference<List<Store>>(){});
        } catch (Exception e) {
            storeList = new ArrayList<>();
            logger.error("Error in getting store list from server",e);
        }
        return storeList;
    }


    public  static Store getStoreById(Long id, Activity activity){
        List<Store> storeList = null;
        String sessionId = (String)(activity.getSharedPreferences(Constants.SESSION_DATA,Context.MODE_PRIVATE).
                getAll().get(Constants.SESSION_ID));

        Response response = RestUtil.execute(Constants.backend_stores_url+";jsessionid="+sessionId + "/?id="+id.toString(),
                null,null,"GET",100,null);

        try {
            storeList = RestUtil.mapper.readValue(response.getResponseBody(),
                    new TypeReference<List<Store>>(){});
        } catch (Exception e) {
            storeList = new ArrayList<>();
            logger.error("Error in getting store list from server",e);
        }

        return storeList.get(0);
    }


    public  static User getStoreUser(Store store, Activity activity){
        User user = null;
        String sessionId = (String)(activity.getSharedPreferences(Constants.SESSION_DATA,Context.MODE_PRIVATE).
                getAll().get(Constants.SESSION_ID));

        Response response = RestUtil.execute(Constants.backend_user_url+"/findbyid/"+store.getUserId(),
                null,null,"GET",100,sessionId);

        try {
            user = RestUtil.mapper.readValue(response.getResponseBody(),
                    User.class);
        } catch (Exception e) {
            logger.error("Error in getting store list from server",e);
        }

        return user;
    }


    public static void collectCash(Store store, Activity activity, Double amount) {
        String sessionId = (String) (activity.getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).
                getAll().get(Constants.SESSION_ID));

        Map<String,Object> requestBody = new HashMap<>();
        requestBody.put("amount",amount);

        try {
            Response response = RestUtil.execute(Constants.backend_user_url + "/wallet/addmoney/store/" + store.getId(),
                    RestUtil.mapper.writeValueAsString(requestBody), null, "PUT", 100, sessionId);
            response.getResponseCode();
        } catch (Exception e) {
            logger.error("Error in getting store list from server", e);
        }
    }


}
