package in.nogyo.nogyoapp.checkout;

import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.nogyo.nogyoapp.Constants;
import in.nogyo.nogyoapp.models.Order;
import in.nogyo.nogyoapp.models.OrderItem;
import in.nogyo.nogyoapp.models.Product;
import in.nogyo.nogyoapp.models.ProductPricing;
import in.nogyo.nogyoapp.models.Store;
import in.nogyo.nogyoapp.models.User;
import in.nogyo.nogyoapp.rest.Response;
import in.nogyo.nogyoapp.rest.RestUtil;

/**
 * Created by srinand.pc on 25/02/16.
 */
public class CheckoutUtil {


    public static Order completeCheckout(List<ProductPricing> skuList, Store selectedStore, User sessionUser) throws IOException {

        Order order = new Order();
        order.setStatus("RECEIVED");
        order.setFulfillmentStatus("NOT_FULFILLED");
        order.setChannel(sessionUser.getType());
        order.setOrderDate(new Date());
        order.setUserId(sessionUser.getId());
        order.setStoreId(selectedStore.getId());
        List<OrderItem> orderItems = new ArrayList<>();

        Double amount = 0.0;
        Double discountedAmount;
        int bucketid;


        for(ProductPricing sku: skuList){
            switch (isInBucket(sku)){
                case 1:
                    bucketid = 1;
                    discountedAmount = sku.getVariablePricing().get(0).discountedPrice;
                    break;
                case 2:
                    bucketid = 2;
                    discountedAmount = sku.getVariablePricing().get(1).discountedPrice;
                    break;
                case 3:
                    bucketid = 3;
                    discountedAmount = sku.getVariablePricing().get(2).discountedPrice;
                    break;
                default:
                    bucketid = 0;
                    discountedAmount = sku.getPricePerUnit();
            }
            orderItems.add(new OrderItem(null,null,selectedStore.getId(),sku.getId(),order.getStatus().toString(),order.getFulfillmentStatus().toString(),
                    sku.getProductUnit().getId(), sku.getProduct().getQuantity(),discountedAmount,bucketid));
            amount += sku.getProduct().getQuantity() * discountedAmount;
        }
        order.setOrderItems(orderItems);
        order.setPricePromised(Math.ceil(amount));
        Response response = RestUtil.execute(Constants.backend_orders_url,
                RestUtil.mapper.writeValueAsString(order),null,"POST",100,sessionUser.getSessionId());

        try{
            order = RestUtil.mapper.readValue(response.getResponseBody(),Order.class);
        }catch (Exception e){
            Log.e("ERROR","Unable place the order Retry again");
            return null;
        }

        return order;
    }


    public static Order completeCheckout(List<ProductPricing> skuList, Order selectedOrder, User sessionUser) throws IOException {

        Map<Long, OrderItem> productIdToorderItemMap = new HashMap<>();

        populateProductIdToOrderItemMap(productIdToorderItemMap,selectedOrder);

        for(ProductPricing productPricing : skuList){
            OrderItem orderItem = productIdToorderItemMap.get(productPricing.getProductId());
            if(orderItem == null){
                orderItem = new OrderItem(null,selectedOrder.getId(), selectedOrder.getStoreId(),productPricing.getProductId(),selectedOrder.getStatus(),selectedOrder.getFulfillmentStatus(),
                        productPricing.getUnitId(),productPricing.getProduct().getQuantity(),productPricing.getPricePerUnit(),0);
                selectedOrder.getOrderItems().add(orderItem);
            } else {
                orderItem.setQuantity(productPricing.getProduct().getQuantity());
                productIdToorderItemMap.remove(productPricing.getProductId());
            }
        }

        for(OrderItem orderItem : productIdToorderItemMap.values()){
            orderItem.setQuantity(0);
        }

        Double amount = 0.0;
        for(OrderItem orderItem: selectedOrder.getOrderItems()){
            orderItem.setProduct(null);
            amount += orderItem.getPricePerUnit()*orderItem.getQuantity();
        }
        selectedOrder.setPricePromised(Math.ceil(amount));


        Response response = RestUtil.execute(Constants.backend_orders_url,
                RestUtil.mapper.writeValueAsString(selectedOrder),null,"PUT",100,sessionUser.getSessionId());

        try{
            selectedOrder = RestUtil.mapper.readValue(response.getResponseBody(),Order.class);
        }catch (Exception e){
            Log.e("ERROR","Unable update the order Retry again");
            return null;
        }
        return selectedOrder;
    }

    public static void populateProductIdToOrderItemMap(Map<Long, OrderItem> productIdToorderItemMap, Order selectedOrder ){
        List<OrderItem> orderItemList = selectedOrder.getOrderItems();
        for( OrderItem orderItem : orderItemList){
            productIdToorderItemMap.put(orderItem.getSkuCode(), orderItem);
        }
    }


    public static void populateProductIdToPricingMap(Map<Long, ProductPricing> productIdToorderItemMap, List<ProductPricing> productPricings){

        for( ProductPricing productPricing : productPricings){
            productIdToorderItemMap.put(productPricing.getProductId(), productPricing);
        }
    }

    private static int isInBucket(ProductPricing productPricing){
        if(productPricing.getProduct().getQuantity() >= productPricing.getVariablePricing().get(0).min && productPricing.getProduct().getQuantity() < productPricing.getVariablePricing().get(0).max) {
            return 1;
        }else if(productPricing.getProduct().getQuantity() >= productPricing.getVariablePricing().get(1).min && productPricing.getProduct().getQuantity() < productPricing.getVariablePricing().get(1).max){
            return 2;
        }else if(productPricing.getProduct().getQuantity() >= productPricing.getVariablePricing().get(2).min){
            return 3;
        }
        return 0;
    }
}
