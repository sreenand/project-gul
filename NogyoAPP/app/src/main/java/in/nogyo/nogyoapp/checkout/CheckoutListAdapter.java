package in.nogyo.nogyoapp.checkout;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import in.nogyo.nogyoapp.R;
import in.nogyo.nogyoapp.models.Product;
import in.nogyo.nogyoapp.models.ProductPricing;

/**
 * Created by srinand.pc on 25/02/16.
 */
public class CheckoutListAdapter extends BaseAdapter {

    private List<ProductPricing> products;
    private Activity activity;


    public CheckoutListAdapter(List<ProductPricing> products, Activity activity){
        this.products = products;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return products.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ProductPricing skuItem = products.get(position);
        View view = convertView;
        if (view == null) // no view to re-use, create new
            view = activity.getLayoutInflater().inflate(R.layout.checkout_row, null);
        TextView name = (TextView)view.findViewById(R.id.checkout_grocery_name);
        TextView quantity = (TextView)view.findViewById(R.id.checkout_grocery_quantity);
        name.setText(skuItem.getProduct().getName());
        quantity.setText(skuItem.getProduct().getQuantity()+" kgs");
        return view;
    }
}
