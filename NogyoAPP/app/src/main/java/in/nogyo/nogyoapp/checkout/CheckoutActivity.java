package in.nogyo.nogyoapp.checkout;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.codehaus.jackson.type.TypeReference;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.nogyo.nogyoapp.Constants;
import in.nogyo.nogyoapp.R;
import in.nogyo.nogyoapp.SKUresultList.SKUListActivity;
import in.nogyo.nogyoapp.common.CommonUtils;
import in.nogyo.nogyoapp.models.Order;
import in.nogyo.nogyoapp.models.Product;
import in.nogyo.nogyoapp.models.ProductPricing;
import in.nogyo.nogyoapp.models.Store;
import in.nogyo.nogyoapp.rest.RestUtil;
import in.nogyo.nogyoapp.store.StoreActivity;
import lombok.Getter;
import lombok.Setter;


@Getter @Setter
public class CheckoutActivity extends AppCompatActivity {


    private LinearLayout listView;
    private List<ProductPricing> products;
    private View mCheckoutProgressView;
    private Button button;
    private String language;
    private TextView totalCount;
    private Order selectedOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Map<String,String> preferences = (Map<String,String>)getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).getAll();
        try{

            Bundle extras = getIntent().getExtras();
            if(extras != null) {
                String orderselected = (String) extras.get(Constants.ORDER_SELECTED);
                if(orderselected != null)
                    selectedOrder = RestUtil.mapper.readValue(orderselected, Order.class);
            }
//            Store store = RestUtil.mapper.readValue(
//                    (String)getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).getAll().get(Constants.SELECTED_STORE), Store.class);
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_checkout);
            listView = (LinearLayout)findViewById(R.id.checkout_list);
            mCheckoutProgressView = findViewById(R.id.checkout_progress);
            button = (Button)findViewById(R.id.checkout_button);
            if(selectedOrder != null){
                button.setText("Update Order");
            }
            language = (String)getSharedPreferences(Constants.SESSION_DATA,Context.MODE_PRIVATE).getAll().get(Constants.LANGUAGE_SELECTED);
            totalCount = (TextView)(findViewById(R.id.totalCount));
            products = RestUtil.mapper.readValue(preferences.get(Constants.SESSION_CART),new TypeReference<List<ProductPricing>>(){});
            for(ProductPricing product : products){
                product.getProduct().setLocalNamesMap(RestUtil.mapper.readValue(product.getProduct().getLocalNames(), HashMap.class));
            }
            totalCount.setText("No. Of Items:" + products.size());
        }catch(Exception e){
            products = null;
        }

        if(products == null || products.isEmpty()){
            Toast.makeText(getBaseContext(), "Please select Something", Toast.LENGTH_SHORT).show();
            startActivity(CommonUtils.navigate(CheckoutActivity.this, SKUListActivity.class));
            return;
        }

        View iview = getLayoutInflater().inflate(R.layout.checkout_row, null);
        ((TextView)iview.findViewById(R.id.checkout_grocery_name)).setText("Product");
        ((TextView)iview.findViewById(R.id.checkout_grocery_quantity)).setText("Qty");
        ((TextView)iview.findViewById(R.id.checkout_grocery_ppu)).setText("Price/Unit");
        ((TextView)iview.findViewById(R.id.checkout_grocery_item_total)).setText("Item Total");
        listView.addView(iview);

        Double totalAmount = 0.0;
        Double discountedAmount;

        for(ProductPricing sku: products){
            View view = getLayoutInflater().inflate(R.layout.checkout_row, null);
            TextView view1 = (TextView)view.findViewById(R.id.checkout_grocery_name);
            TextView view2 = (TextView)view.findViewById(R.id.checkout_grocery_quantity);
            view1.setText(sku.getProduct().getLocalName(language));
            view2.setText(sku.getProduct().getQuantity() + " " + sku.getProductUnit().getUnitMetric());
            switch (isInBucket(sku)){
                case 1:
                    discountedAmount = sku.getVariablePricing().get(0).discountedPrice;
                    break;
                case 2:
                    discountedAmount = sku.getVariablePricing().get(1).discountedPrice;
                    break;
                case 3:
                    discountedAmount = sku.getVariablePricing().get(2).discountedPrice;
                    break;
                default:
                    discountedAmount = sku.getPricePerUnit();
            }
            ((TextView)view.findViewById(R.id.checkout_grocery_ppu)).setText("\u20B9" + discountedAmount + "/" + sku.getProductUnit().getUnitMetric());
            ((TextView)view.findViewById(R.id.checkout_grocery_item_total)).setText("\u20B9"  + (discountedAmount * sku.getProduct().getQuantity()));
            listView.addView(view);
            totalAmount += (discountedAmount*sku.getProduct().getQuantity());
        }

        if(totalAmount > 0){
            View gview = getLayoutInflater().inflate(R.layout.checkout_total,null);
            ((TextView)gview.findViewById(R.id.grand_total)).setText("₹" + totalAmount.toString());
            listView.addView(gview);
        }
    }


    private int isInBucket(ProductPricing productPricing){
        if(productPricing.getProduct().getQuantity() >= productPricing.getVariablePricing().get(0).min && productPricing.getProduct().getQuantity() < productPricing.getVariablePricing().get(0).max) {
            return 1;
        }else if(productPricing.getProduct().getQuantity() >= productPricing.getVariablePricing().get(1).min && productPricing.getProduct().getQuantity() < productPricing.getVariablePricing().get(1).max){
            return 2;
        }else if(productPricing.getProduct().getQuantity() >= productPricing.getVariablePricing().get(2).min){
            return 3;
        }
        return 0;
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu_checkout, menu);
//        return true;
//    }

//    @Override
//    public void onBackPressed(){
//        startActivity(CommonUtils.navigate(CheckoutActivity.this, SKUListActivity.class));
//        this.finish();
//    }


    public void confirmOrder(View view) {

        Calendar cal = Calendar.getInstance();
        try {
            Thread.sleep(100);
            button.setVisibility(View.GONE);
            mCheckoutProgressView.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            Toast.makeText(getBaseContext(), "Please Wait While we Confirm Your Order", Toast.LENGTH_SHORT).show();
            CheckoutTask checkoutTask = new CheckoutTask(this);
            checkoutTask.execute();
        } catch (InterruptedException e) {
            Toast.makeText(getBaseContext(), "Unable to place order Try after some time", Toast.LENGTH_SHORT).show();
            startActivity(CommonUtils.navigate(CheckoutActivity.this, StoreActivity.class));
        }

    }
}
