package in.nogyo.nogyoapp.SKUresultList.SKUTabbed;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;

import in.nogyo.nogyoapp.R;
import in.nogyo.nogyoapp.models.SubCategory;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by srinandchallur on 13/04/16.
 */

@Getter @Setter
public class SubCategoryFragment extends Fragment {

    private SubCategory subCategory;
    private View rootView;
    private LinearLayout fragmentListLayout;
    private TabbedSKUActivity tabbedSKUActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_tabbed_sku, container, false);
        this.rootView = rootView;
        fragmentListLayout = (LinearLayout)rootView.findViewById(R.id.sku_list);
        SKUFragmentPopulateTask task = new SKUFragmentPopulateTask(this);
        task.execute((Void) null);
        return rootView;
    }
}
