package in.nogyo.nogyoapp.order;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import in.nogyo.nogyoapp.R;
import in.nogyo.nogyoapp.models.Order;
import in.nogyo.nogyoapp.models.OrderItem;
import in.nogyo.nogyoapp.rest.RestUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by srinandchallur on 08/04/16.
 */

@Getter @Setter
public class OrderItemListAdapter extends BaseAdapter {

    private List<OrderItem> orderItemList;
    private OrderActivity orderActivity;


    public OrderItemListAdapter(List<OrderItem> orderItemList, OrderActivity activity){
        this.orderItemList = orderItemList;
        this.orderActivity = activity;
    }

    @Override
    public int getCount() {
        return orderItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return orderItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return orderItemList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        OrderItem item = orderItemList.get(position);
        View view = convertView;
        if (view == null) // no view to re-use, create new
            view = orderActivity.getLayoutInflater().inflate(R.layout.order_item_row, null);
        try {
            item.getProduct().setLocalNamesMap(RestUtil.mapper.readValue(item.getProduct().getLocalNames(), HashMap.class));
        } catch (IOException e) {
            Log.e("ERRPR","No local language present",e);
        }
        ((TextView)(view.findViewById(R.id.order_item_name))).setText(item.getProduct().getLocalName(orderActivity.getLanguage()));
        ((TextView)(view.findViewById(R.id.order_item_quantity))).setText(item.getQuantity()+"kgs");
        return view;
    }
}
