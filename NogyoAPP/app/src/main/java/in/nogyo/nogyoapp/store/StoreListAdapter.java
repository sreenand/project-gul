package in.nogyo.nogyoapp.store;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.nogyo.nogyoapp.models.Store;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import in.nogyo.nogyoapp.R;

/**
 * Created by srinand.pc on 23/02/16.
 */

@Getter @Setter @NoArgsConstructor
public class StoreListAdapter extends BaseAdapter {

    private List<Store> storeList;
    private Activity listActivity;
    private ArrayList<Store> arraylist;

    public StoreListAdapter(List<Store> storeList, Activity listActivity){
        this.storeList = storeList;
        this.listActivity = listActivity;
        this.arraylist = new ArrayList<>();
    }


    @Override
    public int getCount() {

        if(storeList == null) return 0;
        return storeList.size();
    }

    @Override
    public Object getItem(int position) {

        if(storeList == null) return null;
        return storeList.get(position);
    }

    @Override
    public long getItemId(int position) {

        if(storeList == null) return -1L;
        return storeList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Store item = storeList.get(position);
        View view = convertView;
        if (view == null) // no view to re-use, create new
            view = listActivity.getLayoutInflater().inflate(R.layout.store_list_row, null);
        TextView name = (TextView)view.findViewById(R.id.storeName);
        TextView ownerName = (TextView)view.findViewById(R.id.storeOwnerName);
        TextView address = (TextView)view.findViewById(R.id.storeAddress);
        name.setText(item.getName());
        ownerName.setText(item.getContactPerson());
        address.setText(item.getAddress());
        return view;
    }

    public void filter(String charText) {
        if(arraylist.size() == 0)
            arraylist.addAll(storeList);
        charText = charText.toLowerCase(Locale.getDefault());
        storeList.clear();
        if (charText.length() == 0) {
            storeList.addAll(arraylist);
        }
        else
        {
            for (Store wp : arraylist)
            {
                if (wp.name.toLowerCase(Locale.getDefault()).contains(charText))
                {
                    storeList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
