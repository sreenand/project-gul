package in.nogyo.nogyoapp.SKUresultList.SKUTabbed;

import android.os.AsyncTask;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import in.nogyo.nogyoapp.SKUresultList.SKUUtil;
import in.nogyo.nogyoapp.models.SubCategory;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by srinandchallur on 13/04/16.
 */

@Getter @Setter
public class SubCategoryPopulateTask extends AsyncTask<Void, Void, Boolean> {

    private List<SubCategory> subCategoryList;
    private TabbedSKUActivity tabbedSKUActivity;

    public SubCategoryPopulateTask(TabbedSKUActivity tabbedSKUActivity){
        this.tabbedSKUActivity = tabbedSKUActivity;
    }


    @Override
    protected Boolean doInBackground(Void... params) {
        try{
            subCategoryList = SKUUtil.getAllSubCategories(1L,tabbedSKUActivity);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
