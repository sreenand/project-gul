package in.nogyo.nogyoapp.checkout;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.io.IOException;
import java.util.Map;

import in.nogyo.nogyoapp.Constants;
import in.nogyo.nogyoapp.common.CommonUtils;
import in.nogyo.nogyoapp.models.Order;
import in.nogyo.nogyoapp.models.Store;
import in.nogyo.nogyoapp.models.User;
import in.nogyo.nogyoapp.order.OrderActivity;
import in.nogyo.nogyoapp.rest.RestUtil;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by srinand.pc on 25/02/16.
 */

@Getter @Setter
public class CheckoutTask extends AsyncTask<Void, Void, Order> {

    private CheckoutActivity activity;

    public  CheckoutTask (CheckoutActivity activity){
        this.activity = activity;
    }
    @Override
    protected Order doInBackground(Void... params) {

        Map<String,String> map = (Map<String,String>)activity.getSharedPreferences(Constants.SESSION_DATA,Context.MODE_PRIVATE).getAll();
        try{
            User sessionUser = RestUtil.mapper.readValue(map.get(Constants.SESSION_USER),User.class);
            Store store = RestUtil.mapper.readValue(map.get(Constants.SELECTED_STORE), Store.class);
            RestUtil.mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);

            if(activity.getSelectedOrder() != null){
                return CheckoutUtil.completeCheckout(activity.getProducts(),activity.getSelectedOrder(),sessionUser);
            } else{
                return CheckoutUtil.completeCheckout(activity.getProducts(),store,sessionUser);
            }

        } catch (Exception e){
            return null;
        }

    }

    @Override
    protected void onPostExecute(final Order order) {
        if(order == null){
            Toast.makeText(activity.getBaseContext(), "Unable to complete the transaction please try after some time", Toast.LENGTH_SHORT).show();
        } else {
            SharedPreferences.Editor editor = activity.getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).edit();
            try {
                try {
                    Order confirmedOrder = order;
                    if(confirmedOrder == null)
                        throw new RuntimeException("Unable to place the order");
                    editor.remove(Constants.SESSION_CART);
                    editor.commit();
                    Intent intent = CommonUtils.navigate(activity, OrderActivity.class);
                    intent.putExtra(Constants.CALLER_ACTIVITY,String.valueOf(Constants.ACTIVITY_CHECKOUT));
                    activity.startActivity(intent);
                } catch (Exception e) {
                    Log.e("ERROR","Unable to place the order",e);
                    Toast.makeText(activity.getBaseContext(), "", Toast.LENGTH_SHORT).show();
                }
                editor.putString(Constants.ORDER_SELECTED, RestUtil.mapper.writeValueAsString(order));
                editor.commit();
                activity.finish();
            } catch (IOException e) {
                Toast.makeText(activity.getBaseContext(), "Your Order has been Placed Contact EcoFarm for further details", Toast.LENGTH_SHORT).show();
                Log.e("ERROR","Error in serialzing Order");
            }
            activity.startActivity(CommonUtils.navigate(activity, OrderActivity.class));
        }
    }
}
