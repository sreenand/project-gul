package in.nogyo.nogyoapp.order;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.IOException;

import in.nogyo.nogyoapp.Constants;
import in.nogyo.nogyoapp.models.Store;
import in.nogyo.nogyoapp.rest.RestUtil;
import in.nogyo.nogyoapp.store.StoreUtil;
import lombok.Getter;
import lombok.Setter;
import in.nogyo.nogyoapp.R;

/**
 * Created by srinand.pc on 25/02/16.
 */

@Getter @Setter
public class OrderDataPopulateTask extends AsyncTask<Void, Void, Store>{

    private OrderActivity activity;
    private Store store;


    public OrderDataPopulateTask(OrderActivity activity, Store store){
        this.activity = activity;
        this.store = store;
    }


    @Override
    protected Store doInBackground(Void... params) {

        try {

            if(activity.getSelectedStore() == null)
                  return StoreUtil.getStoreById(activity.getOrder().getStoreId(),activity);

            return activity.getSelectedStore();
        } catch (Exception e) {
           return null;
        }

    }

    @Override
    public void onPostExecute(Store store){
        this.setStore(store);
        populateStoreData();
    }

    private void populateStoreData(){

        if(store == null)
            return;

        View storeView = activity.getLayoutInflater().inflate(R.layout.order_view_store_content, null);
        ((TextView)(storeView.findViewById(R.id.store_name_value))).setText(store.getName());
        ((TextView)(storeView.findViewById(R.id.store_owner_value))).setText(store.getContactPerson());
        ((TextView)(storeView.findViewById(R.id.store_address_value))).setText(store.getAddress());
        activity.getLayout().addView(storeView);
    }
}
