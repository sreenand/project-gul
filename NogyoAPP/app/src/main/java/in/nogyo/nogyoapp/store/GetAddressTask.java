package in.nogyo.nogyoapp.store;

import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;

import java.util.List;
import java.util.Locale;

/**
 * Created by srinandchallur on 07/06/16.
 */
public class GetAddressTask extends AsyncTask<String, Void, String> {

    private NewStoreActivity activity;
    private Address address;

    public GetAddressTask(NewStoreActivity activity) {
        super();
        this.activity = activity;
    }

    @Override
    protected String doInBackground(String... params) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(activity, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(Double.parseDouble(params[0]), Double.parseDouble(params[1]), 1);

            if(addresses != null && addresses.size() > 0){
                address = addresses.get(0);
            }

        } catch (Exception ex) {
            ex.printStackTrace();

        }

        return null;

    }

    /**
     * When the task finishes, onPostExecute() call back data to Activity UI and displays the address.
     * @param address
     */
    @Override
    protected void onPostExecute(String address) {

    }
}

