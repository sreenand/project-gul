package in.nogyo.nogyoapp.login;


import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import in.nogyo.nogyoapp.Constants;
import in.nogyo.nogyoapp.models.User;
import in.nogyo.nogyoapp.rest.Response;
import in.nogyo.nogyoapp.rest.RestUtil;

/**
 * Created by srinand.pc on 21/02/16.
 */
public class LoginUtils {

    public static final String session_user = "SESSION_USER";


    public static User login(String username, String password){

        Map<String,String> userLoginRequest = new HashMap<>();
        userLoginRequest.put("username",username);
        userLoginRequest.put("password",password);

        try {
            Response response = RestUtil.execute(Constants.backend_login_url,RestUtil.mapper.writeValueAsString(userLoginRequest),null,"post",1000,null);
            if(response.getResponseCode() == 200){
                User sessionUser = RestUtil.mapper.readValue(response.getResponseBody(),User.class);

                if(sessionUser.isSalesPerson() || sessionUser.isAdmin()) {
                    sessionUser.setSessionId(response.getCookies().get("JSESSIONID"));
                    return sessionUser;
                }
            } else {
                Log.e("LOGIN_ERROR","Response: "+RestUtil.mapper.writeValueAsString(response));
                return null;
            }

        } catch (IOException e) {
            Log.e("ERROR",e.getMessage(),e);
        }

        return null;
    }
}
