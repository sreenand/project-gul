package in.nogyo.nogyoapp.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by srinand.pc on 25/02/16.
 */

@Getter @Setter @NoArgsConstructor
public class OrderItem extends AbstractTimeStamp{


    public OrderItem(Long id,Long orderId,Long storeId,Long skuCode,String status, String ffstatus,  Long productUnitId,
                     int quantity, Double ppu,int bucketid){
        this.id = id;
        this.orderId = orderId;
        this.storeId = storeId;
        this.skuCode = skuCode;
        this.status = status;
        this.quantity = quantity;
        this.productUnitId = productUnitId;
        this.fulfillmentStatus = ffstatus;
        this.pricePerUnit = ppu;
        this.bucketid = bucketid;
    }

    private Long id;
    private Long orderId;
    private Long storeId;
    private Long skuCode;
    private String status;
    private int quantity;
    private Product product;
    private Long productUnitId;
    private ProductUnit productUnit;
    private Double quantityProcured;
    private Double discount;
    private String fulfillmentStatus;
    private String returnStatus;
    private Double priceOfferedPerUnit;
    private Double pricePerUnit;
    private int bucketid;
}
