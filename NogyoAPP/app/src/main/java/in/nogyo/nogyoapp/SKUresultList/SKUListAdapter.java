package in.nogyo.nogyoapp.SKUresultList;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import in.nogyo.nogyoapp.R;
import in.nogyo.nogyoapp.models.Product;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by srinand.pc on 23/02/16.
 */

@Getter @Setter
public class SKUListAdapter extends BaseAdapter {

    private List<Product> products;
    private Activity listActivity;

    public SKUListAdapter(List<Product> skus, Activity activity){
        this.products = skus;
        this.listActivity = activity;
    }


    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return products.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Product skuItem = products.get(position);
        View view = convertView;
        if (view == null) // no view to re-use, create  new
            view = listActivity.getLayoutInflater().inflate(R.layout.grocery_list_row, null);
        TextView name = (TextView)view.findViewById(R.id.groceryName);
        EditText quantity = (EditText)view.findViewById(R.id.groceryQuantity);
        Button addButton = (Button)view.findViewById(R.id.groceryAdd);
        ImageView imageView = (ImageView)view.findViewById(R.id.skuImage);
//        if(skuItem.getImageUrl() != null || skuItem.getImageUrl().isEmpty())
        Picasso.with(listActivity).load("http://54.187.14.2/Images/logo.png").into(imageView);
        addButton.setOnClickListener(new SKUAddListener(null,null,quantity));
        name.setText(skuItem.getName());
        return view;
    }
}
