package in.nogyo.nogyoapp.store;

import android.app.Activity;
import android.os.AsyncTask;
import android.view.View;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import in.nogyo.nogyoapp.models.Store;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by srinand.pc on 23/02/16.
 */
@Getter
@Setter
public class StorePopulateTask extends AsyncTask<Void, Void, Boolean> {

    private List<Store> stores;
    private String storeName;
    private StoreActivity activity;
    private StoreListAdapter storeListAdapter;

    public StorePopulateTask(List<Store> stores, String storeName, StoreActivity activity, StoreListAdapter listAdapter){
        this.stores = stores;
        this.storeName = storeName;
        this.activity = activity;
        this.storeListAdapter = listAdapter;
    }

    public StorePopulateTask(StorePopulateTask task){
        this.stores = task.getStores();
        this.storeName = task.getStoreName();
        this.activity = task.getActivity();
        this.storeListAdapter = task.getStoreListAdapter();
    }


    @Override
    protected Boolean doInBackground(Void... params) {
        if(storeName == null){
            this.activity.getMProgressView().setVisibility(View.VISIBLE);
            List<Store> storeList;
            Comparator<Store> ALPHABETICAL_ORDER = new Comparator<Store>() {
                public int compare(Store object1, Store object2) {
                    int res = String.CASE_INSENSITIVE_ORDER.compare(object1.name, object2.name);
                    return res;
                }
            };
            if(activity.getUser().isAdmin() || activity.getUser().isAdmin()) {
                stores.addAll(StoreUtil.getAllStores(activity));
            }else {
                stores.addAll(StoreUtil.getMyStores(activity));
            }
            Collections.sort(stores, ALPHABETICAL_ORDER);
        } else{
            stores.clear();
            stores.addAll(StoreUtil.getStores(storeName, activity));
        }
        return true;
    }

    @Override
    protected void onPostExecute(final Boolean success) {
        if(success){
            this.activity.getMProgressView().setVisibility(View.GONE);
            storeListAdapter.notifyDataSetChanged();
        }
    }
}
