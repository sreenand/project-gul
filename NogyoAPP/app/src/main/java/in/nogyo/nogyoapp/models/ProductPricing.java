package in.nogyo.nogyoapp.models;

import java.util.ArrayList;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by srinandchallur on 27/04/16.
 */

@Getter @Setter @NoArgsConstructor
public class ProductPricing {
    private Long id;
    private Long localityId;
    private Long cityId;
    private Long productId;
    private Product product;
    private Long unitId;
    private ProductUnit productUnit;
    private Double pricePerUnit;
    private Double minQuantity;
    private Double maxQuantity;
    private String quantityDescription;
    private String status;
    public ArrayList<VariablePricing> variablePricings;

    public ArrayList<VariablePricing> getVariablePricing() {
        return variablePricings;
    }

    public void setVariablePricing(ArrayList<VariablePricing> variablePricings) {
        this.variablePricings = variablePricings;
    }
}
