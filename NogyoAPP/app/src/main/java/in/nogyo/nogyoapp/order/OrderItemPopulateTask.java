package in.nogyo.nogyoapp.order;

import android.app.Activity;
import android.os.AsyncTask;
import android.view.View;

import java.util.List;

import in.nogyo.nogyoapp.models.OrderItem;
import in.nogyo.nogyoapp.models.Store;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by srinandchallur on 08/04/16.
 */

@Getter @Setter
public class OrderItemPopulateTask extends AsyncTask<Void, Void, Boolean> {


    private int limit;
    private int offset;
    private OrderActivity activity;
    private List<OrderItem> orderItemList;


    public OrderItemPopulateTask(int limit, int offset, OrderActivity activity, List<OrderItem> orderItemList){
        this.offset = offset;
        this.limit = limit;
        this.activity = activity;
        this.orderItemList = orderItemList;
    }


    @Override
    protected Boolean doInBackground(Void... params) {
        activity.setLoadingMore(true);
        orderItemList = OrderUtil.getOrderItemsForOrder(activity.getOrder().getId(),limit,offset,activity);
        activity.getOrderItemList().addAll(getOrderItemList());
        return true;
    }

    @Override
    protected void onPostExecute(final Boolean success) {
        activity.getOrderItemListAdapter().notifyDataSetChanged();
        activity.setLoadingMore(false);
        if(getOrderItemList().size() < 10){
            if(activity.getSessionUser().isAdmin() && activity.getOrder().getStatus().equals("RECEIVED"))
               activity.getEditOrderButton().setVisibility(View.VISIBLE);
            activity.setFetchData(false);
        }
    }
}
