package in.nogyo.nogyoapp.SKUresultList;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import in.nogyo.nogyoapp.Constants;
import in.nogyo.nogyoapp.R;
import in.nogyo.nogyoapp.models.Product;
import in.nogyo.nogyoapp.models.Store;
import in.nogyo.nogyoapp.rest.RestUtil;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by srinand.pc on 23/02/16.
 */

@Getter @Setter
public class SKUPopulateTask extends AsyncTask<Void, Void, Boolean> {

    private SKUListActivity activity;
    private List<ImageView> imagesList;
    private List<String> urls;
    private ImagePopulateTask imagePopulateTask;

    public SKUPopulateTask(SKUListActivity activity){
        this.activity = activity;
        this.imagesList = new ArrayList<>();
        this.urls = new ArrayList<>();
    }

    @Override
    protected Boolean doInBackground(Void... params) {

        try {
            activity.setStore(RestUtil.mapper.readValue(
                    (String)activity.getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).getAll().get(Constants.SELECTED_STORE), Store.class));

            List<Product> skuList = SKUUtil.getAllSKUs(activity);
            Collections.sort(skuList, new Comparator() {
                @Override
                public int compare(Object sku1, Object sku2) {
                    //use instanceof to verify the references are indeed of the type in question
                    return ((Product) sku1).getName()
                            .compareTo(((Product) sku2).getName());
                }
            });

            activity.getSkuList().addAll(skuList);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    @Override
    protected void onPostExecute(final Boolean success) {
        if(success){

            try{
                for(Product sku: activity.getSkuList()){
                    View view = activity.getLayoutInflater().inflate(R.layout.grocery_list_row, null);
                    TextView name = (TextView)view.findViewById(R.id.groceryName);
                    EditText quantity = (EditText)view.findViewById(R.id.groceryQuantity);
                    Button addButton = (Button)view.findViewById(R.id.groceryAdd);
                    ImageView imageView = (ImageView)view.findViewById(R.id.skuImage);
                    name.setText(sku.getLocalName(activity.getLanguage().toLowerCase()));
                    if(sku.getQuantity() > 0)
                        quantity.setText(sku.getQuantity());
                    addButton.setOnClickListener(new SKUAddListener(null,null,quantity));
                    if (sku.getImageUrl() != null && !sku.getImageUrl().isEmpty()){
                        Picasso.with(activity).load(sku.getImageUrl()).into(imageView);
                    }
                    activity.getListView().addView(view);
                }

                this.imagePopulateTask = new ImagePopulateTask(imagesList,urls,activity);
                this.activity.getmProgressView().setVisibility(View.GONE);
                this.activity.setPostExecuteComplete(true);

//                imagePopulateTask.execute((Void) null);

            }catch (Exception e){
                Log.e("Error in populating sku",e.getMessage(),e);
            }


        }
    }
}
