package in.nogyo.nogyoapp.store;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import in.nogyo.nogyoapp.Constants;
import in.nogyo.nogyoapp.R;
import in.nogyo.nogyoapp.common.CommonUtils;
import in.nogyo.nogyoapp.login.LoginActivity;
import in.nogyo.nogyoapp.menu.BaseActivityWithMenu;
import in.nogyo.nogyoapp.models.Store;
import in.nogyo.nogyoapp.models.User;
import in.nogyo.nogyoapp.mystore.MyStoreActivity;
import in.nogyo.nogyoapp.rest.RestUtil;
import lombok.Getter;
import lombok.Setter;


@Getter @Setter
public class StoreActivity extends BaseActivityWithMenu {


    private EditText storeEditText;
    private ListView listView;
    private StorePopulateTask storePopulateTask;
    private List<Store> storeList;
    private static final int TIME_INTERVAL = 2000; // # milliseconds, desired time passed between two back presses.
    private long mBackPressed;
    private View mProgressView;
    private User user;
    public StoreListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try{
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_store);
            user = RestUtil.mapper.readValue(
                    (String)getSharedPreferences(Constants.SESSION_DATA,Context.MODE_PRIVATE).getAll().get(Constants.SESSION_USER),User.class);
            createMenu();
            storeList = new ArrayList<Store>();
            mProgressView = findViewById(R.id.store_task_progress);
            mProgressView.setVisibility(View.VISIBLE);
            storeEditText = (EditText) findViewById(R.id.store_name);
            listView = (ListView)findViewById(R.id.store_list);
            adapter = new StoreListAdapter(storeList,this);
            ListView listView = (ListView) findViewById(R.id.store_list);
            listView.setAdapter(adapter);

            storePopulateTask = new StorePopulateTask(storeList,null,this,adapter);

            if(user.isAdmin() || user.isSalesPerson()) {
                storeEditText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        adapter.filter(s.toString());
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
            } else{
                storeEditText.setVisibility(View.GONE);
                ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) listView
                        .getLayoutParams();
                mlp.setMargins(5, 5, 5, 5);

            }
            storePopulateTask.execute((Void) null);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View v, int position,
                                        long id) {

                    Store store = (Store) parent.getAdapter().getItem(position);
                    try {
                        SharedPreferences.Editor editor = getSharedPreferences(Constants.SESSION_DATA, Context.MODE_PRIVATE).edit();
                        editor.putString(Constants.SELECTED_STORE, RestUtil.mapper.writeValueAsString(store));
                        editor.commit();
                        startActivity(CommonUtils.navigate(StoreActivity.this, MyStoreActivity.class));
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), R.string.unable_to_select_store, Toast.LENGTH_LONG).show();
                        Log.v("SCHEMA", "onItemClick Nuked!");
                    }
                }
            });


        } catch (Exception e){
            System.out.println(e.getMessage());
        }

    }

    @Override
    public void onBackPressed() {
        if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()){
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            return;
        } else {
            Toast.makeText(getBaseContext(), R.string.tap_back_to_exit, Toast.LENGTH_SHORT).show();
        }
        mBackPressed = System.currentTimeMillis();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        //inflater.inflate(R.menu.menu_store, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.string.mystores:
                ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(storeEditText.getWindowToken(), 0);
                if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {
                    mDrawerLayout.openDrawer(mDrawerList);
                }
                break;
            case R.string.action_logout:
                this.logOut();
                break;
            default:
                break;
        }

        return true;
    }

    @Override
    public void displayView(View view) {
        super.displayView(view);
    }

    @Override
    public void logOut(){
        CommonUtils.clearSessionData(this);
        storeEditText.setVisibility(View.GONE);
        listView.setVisibility(View.GONE);
        mProgressView.setVisibility(View.VISIBLE);
        try {
            Thread.sleep(1000);
            startActivity(CommonUtils.navigate(StoreActivity.this, LoginActivity.class));
        } catch (InterruptedException e) {
            Log.e("ERROR", "ERROR in thread wait");

        }
        CommonUtils.clearSessionData(this);
    }

}
