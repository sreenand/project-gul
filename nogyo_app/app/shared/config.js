var apiurl = "http://128.199.122.204:8080";

module.exports = {
    apiUrl: apiurl,
    loginUrl: apiurl + "/users/login",
    skuUrl: apiurl+"/sku",
    storeUrl: apiurl+"/stores",
    demandUrl: apiurl+ "/demand/add/bulk",
    orderUrl: apiurl+ "/orders",
    sessionId : null,
    invalidateToken : function(){
        delete this.sessionId;
        delete this.emailId;
        delete this.userName;
        delete this.userType;
    },
    session_url: function (url){
    return url + ";" + this.sessionId;},
    handleErrors: function(response) {
    if (!response.ok) {
        delete(this.sessionId);
        delete(this.userName);
        delete(this.emailId);
        delete(this.userType);
        console.log(JSON.stringify(response));
        throw Error(response.statusText);
    }
    return response;}
};




