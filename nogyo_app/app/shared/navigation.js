var config = require("./config");
var frameModule = require("ui/frame");
var dialogsModule = require("ui/dialogs");
var fetchModule = require("fetch");

module.exports = {
	goToLoginPage: function() {
		frameModule.topmost().navigate("views/login/login");
	},
	goToRegisterPage: function() {
		frameModule.topmost().navigate("views/register/register");
	},
	goToPasswordPage: function() {
		frameModule.topmost().navigate("views/password/password");
	},
    goToStorePage: function() {
        frameModule.topmost().navigate({
            moduleName: "views/store/store",
            clearHistory: true,
            backstackVisible: false
        });
    },
	goTolistDetailPage: function() {
		frameModule.topmost().navigate({
			moduleName: "views/listdetail/listdetail",
			clearHistory: true,
			animated: true,

		});
	},
	goToListPage: function() {
		frameModule.topmost().navigate({
			moduleName: "views/list/list",
			clearHistory: true,
            backstackVisible: false
		});
	},
	signOut: function() {
		frameModule.topmost().navigate({
			moduleName: "views/login/login",
			animated: false,
			clearHistory: true
		});
	},
	checkout: function() {

		frameModule.topmost().navigate({
			moduleName: "views/checkout/checkout",
			clearHistory: true,
			backstackVisible: false,
			animated: true
		});
	},

	goToOrderDetailsPage: function() {

		try{
			frameModule.topmost().navigate({
				moduleName: "views/order/order",
				clearHistory: true,
				backstackVisible: false,
				animated: true
			});
		}catch(error){
			console.log(error.message);
		}

	},

	startingPage: function() {
		return config.token ? "views/list/list" : "views/login/login";
	},

	customNavigate: function(navigationEntry){
		frameModule.topmost().navigate(navigationEntry);
	}
};
