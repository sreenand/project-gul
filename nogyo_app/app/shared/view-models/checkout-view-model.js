var config = require("../../shared/config");
var fetchModule = require("fetch");
var ObservableArray = require("data/observable-array").ObservableArray;

function CheckoutListViewModel(items){
    var viewModel = new ObservableArray(items);
    viewModel.load = function() {

        var cart = config.cart;
        Object.keys(cart).forEach(function(skuId) {
            var demand = cart[skuId];
            viewModel.push({
                name: demand.name,
                quantity: demand.quantity
            });

        });

    };

    viewModel.empty = function() {
        while (viewModel.length) {
            viewModel.pop();
        }
    };

    return viewModel;

}

module.exports = CheckoutListViewModel;