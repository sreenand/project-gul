var config = require("../../shared/config");
var fetchModule = require("fetch");
var ObservableArray = require("data/observable-array").ObservableArray;
var Observable = require("data/observable-array").Observable;


function StoreListViewModel(items){
    var viewModel = new ObservableArray(items);
    viewModel.load = function(input){

        var urlSubstring;
        if(!input || input.length == 0){
            urlSubstring = "/all";
        } else if(input.length > 0  && input.length < 3 ){
            return;
        } else{
            urlSubstring = "/name/" + encodeURIComponent(input)
        }
        return fetchModule.fetch(config.session_url(config.storeUrl + urlSubstring),
            {headers: {"Content-Type": "application/json"}}).then(config.handleErrors)
            .then(function(response) {
                return response.json();
            }).then(function(data) {
                viewModel.empty();
                data.forEach(function(store) {
                    viewModel.push({
                        name: store.name,
                        id: store.id,
                        address: store.address,
                    });
                });
            });
    };

    viewModel.empty = function() {
        while (viewModel.length) {
            viewModel.pop();
        }
    };

    return viewModel;
}

//function StoreListViewModel(items){
//    var viewModel = new ObservableArray(items);
//    viewModel.load = function(){
//        return fetchModule.fetch(config.session_url(config.storeUrl + "/all"),
//            {headers: {"Content-Type": "application/json"}}).then(config.handleErrors)
//            .then(function(response) {
//                return response.json();
//            }).then(function(data) {
//                viewModel.empty();
//                data.forEach(function(store) {
//                    viewModel.push({
//                        name: store.name,
//                        id: store.id,
//                        address: store.address,
//                    });
//                });
//            });
//    };
//
//    viewModel.empty = function() {
//        while (viewModel.length) {
//            viewModel.pop();
//        }
//    };
//
//    return viewModel;
//}

module.exports = StoreListViewModel;