var config = require("../../shared/config");
var fetchModule = require("fetch");
var ObservableArray = require("data/observable-array").ObservableArray;

function indexOf(item) {
    var match = -1;
    this.forEach(function(loopItem, index) {
        if (loopItem.id === item.id) {
            match = index;
        }
    });
    return match;
}

function GroceryListViewModel(items) {
    var viewModel = new ObservableArray(items);
    viewModel.load = function() {
        if(config.cart == null){
            config.cart = {};
        }
        return fetchModule.fetch(config.session_url(config.skuUrl), {
            headers: {
                "Content-Type": "application/json"
            }
        }).then(config.handleErrors)
            .then(function(response) {
                return response.json();
            }).then(function(data) {
                data.forEach(function(grocery) {

                    var quantity = "";
                    var item_in_cart = config.cart[grocery.id];
                    if(item_in_cart){
                        quantity = item_in_cart.quantity;
                    }
                    viewModel.push({
                        name: grocery.name,
                        id: grocery.id,
                        quantity:quantity
                    });
                });
            });
    };

    viewModel.reload = function(items) {

    }

    viewModel.empty = function() {
        while (viewModel.length) {
            viewModel.pop();
        }
    };

    return viewModel;
}


module.exports = GroceryListViewModel;
