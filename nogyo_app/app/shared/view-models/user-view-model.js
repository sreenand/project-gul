var config = require("../../shared/config");
var fetchModule = require("fetch");
var Observable = require("data/observable").Observable;
var navigate = require ("../../shared/navigation");

function User(info) {
    info = info || {};

    // You can add properties to observables on creation
    var viewModel = new Observable({
        email: info.email || "",
        password: info.password || ""
    });


    viewModel.login = function() {

        if (config.sessionId){
            navigate.goToStorePage();
        }
        return fetchModule.fetch(config.loginUrl, {
            method: "POST",
            body: JSON.stringify({
                username: viewModel.get("email"),
                password: viewModel.get("password")
            }),
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(handleErrors)
            .then(function(response) {
                var cookies = response.headers.get('set-cookie');
                if(cookies != null){
                    config.sessionId = (cookies.split(";")[0]).toString().split("=")[1];
                    var data = response.json();
                    config.userName = data.name;
                    config.userType = data.type;
                    config.emailId = data.emailId;
                }
            });
    };

    viewModel.register = function() {
        return fetchModule.fetch(config.apiUrl + "Users", {
            method: "POST",
            body: JSON.stringify({
                Username: viewModel.get("email"),
                Email: viewModel.get("email"),
                Password: viewModel.get("password")
            }),
            headers: {
                "Content-Type": "application/json"
            }
        }).then(handleErrors);
    };

    return viewModel;
}

function handleErrors(response) {
    if (!response.ok) {
        delete(config.sessionId);
        delete(config.userName);
        delete(config.emailId);
        delete(config.userType);
        console.log(JSON.stringify(response));
        throw Error(response.statusText);
    }
    return response;
}

module.exports = User;