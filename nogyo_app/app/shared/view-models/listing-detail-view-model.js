var config = require("../../shared/config");
var fetchModule = require("fetch");
var Observable = require("data/observable").Observable;
var navigate = require ("../../shared/navigation");


function ListingDetailViewModel(item){
    var viewModel = new Observable(item);
    viewModel.load = function() {
        viewModel.name = config.selectedItem.name;
        viewModel.id = config.selectedItem.id;
        viewModel.quantity = config.selectedItem.quantity;
        delete config.selectedItem;
    };

    viewModel.empty = function() {
        viewModel = {};
    };

    return viewModel;

}

module.exports = ListingDetailViewModel;