var config = require("../../shared/config");
var fetchModule = require("fetch");
var Observable = require("data/observable").Observable;
var navigate = require ("../../shared/navigation");

function OrderViewModel(order){
    var viewModel  = new Observable(order);
    viewModel.load = function() {
        viewModel['order_id'] = config.confirmed_order.id;
        viewModel['order_status'] = config.confirmed_order.status;
    };

    return viewModel;
}

module.exports = OrderViewModel;