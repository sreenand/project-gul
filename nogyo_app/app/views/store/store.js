var Observable = require("data/observable").Observable;
var ObservableArray = require("data/observable-array").ObservableArray;
var viewModule = require("ui/core/view");
var StoreListViewModel = require("../../shared/view-models/store-list-view-model");
var navigation = require ("../../shared/navigation");
var config = require("../../shared/config");
var dialogsModule = require("ui/dialogs");
var page;



var storeList = new StoreListViewModel([]);
var pageData = new Observable({
    storeList: storeList,
    storeName: ""
});


exports.loaded = function(args) {
    page = args.object;
    page.bindingContext = pageData;
    storeList.load();
    var labl = viewModule.getViewById(page, "selected_store");
    if(!labl.text){
        labl.cssClass = "invisible";
    }else{
        labl.cssClass = "visible";
    }
};

pageData.on(Observable.propertyChangeEvent, function (propertyChangeData) {
    try {
        if (propertyChangeData.propertyName == "storeName") {
            storeList.load(propertyChangeData.value);
        }
    } catch(error){
        console.log(error);
    }
});


exports.list = function() {
    if(!config.storeId) {
        dialogsModule.alert("Please Select a Store");
    } else{
        navigation.goToListPage();
    }
};
exports.logout = navigation.signOut;
exports.select_store = function(args){

    try{
        var button = args.object;
        var item = button.bindingContext;
        var labl = viewModule.getViewById(page, "selected_store");
        labl.text = "selected Store: " + item.name;
        labl.cssClass = "visible";
        config.storeId = item.id;
        dialogsModule.alert("Store Set Successfully").then(function() {
            navigation.goToListPage();
        });

    } catch(error){
        console.log(error.message);
    }

};