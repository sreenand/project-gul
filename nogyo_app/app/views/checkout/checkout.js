var Observable = require("data/observable").Observable;
var ObservableArray = require("data/observable-array").ObservableArray;
var viewModule = require("ui/core/view");
var CheckoutListViewModel = require("../../shared/view-models/checkout-view-model");
var navigation = require ("../../shared/navigation");
var config = require("../../shared/config");
var fetchModule = require("fetch");
var dialogsModule = require("ui/dialogs");
var frameModule = require("ui/frame");
var page;


var checkoutList = new CheckoutListViewModel([]);

var pageData = new Observable({
    checkoutList: checkoutList
});

exports.loaded = function(args) {
    page = args.object;
    page.bindingContext = pageData;
    checkoutList.empty();
    checkoutList.load();
};

exports.confirm_order = function(){

    try{
        var payload=[];
        if (!config.storeId){
            dialogsModule.alert("Store Is not Set").then(function() {
                navigation.goToStorePage();
            });
        }
        Object.keys(config.cart).forEach(function(key) {
            var item = config.cart[key];
            payload.push({ skuCode: item.skuCode, storeId:config.storeId,quantity:item.quantity })
        });
        if(payload.length == 0 ){
            dialogsModule.alert("Please Select Something").then(function() {
                navigation.goToStorePage();
            });
            return;
        }

        var orderPayload = {orderItems: payload, status: "RECEIVED"};

        return fetchModule.fetch(config.session_url(config.orderUrl), {
            method: "POST",
            body: JSON.stringify(orderPayload),
            headers: {
                "Content-Type": "application/json"
            }
        }).then(config.handleErrors)
            .then(function(response) {
                delete config.cart;
                delete config.confirmed_order;
                return response.json();
            }).then(function(data){
                config.confirmed_order = data;
                delete config.storeId;
                dialogsModule.alert("Order Created Successfully").then(function() {
                    navigation.goToOrderDetailsPage();
                });
            });
    } catch(err){
        console.log(err);
    }

};

exports.listpage = navigation.goToListPage;
exports.logout = navigation.signOut;

