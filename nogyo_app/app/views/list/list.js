var Observable = require("data/observable").Observable;
var ObservableArray = require("data/observable-array").ObservableArray;
var viewModule = require("ui/core/view");
var GroceryListViewModel = require("../../shared/view-models/grocery-list-view-model");
var navigation = require ("../../shared/navigation");
var config = require("../../shared/config");
var dialogsModule = require("ui/dialogs");
var listViewModule = require("ui/list-view");

var page;


var groceryList = new GroceryListViewModel([]);
var pageData = new Observable({
    groceryList: groceryList
});

exports.loaded = function(args) {

    try {
        page = args.object;
        page.bindingContext = pageData;
        if(!config.storeId){
            dialogsModule.alert("Please Select a Store").then(function() {
                navigation.goToStorePage();
            });
        }
        groceryList.empty();
        groceryList.load();
    } catch(Error){
        console.log("error in loading list.xml: " + Error.message)
    }
};


exports.add_to_cart = function(args){

    var button = args.object;
    var item = button.bindingContext;

    if(item.quantity <= 0){
        dialogsModule.alert({
            message: "Add a quantity for " + item.name,
            okButtonText: "OK"
        });
        return;
    }
    if(button.text == "Remove"){
        delete config.cart[item.id];
        button.text = "Add";
        return;
    }
    var demand = {skuCode:item.id, storeId:config.storeId, vegetableFruitFlag:true, quantity:item.quantity, name: item.name};
    config.cart[item.id] = demand;
    button.text = "Remove";

};



pageData.on(Observable.propertyChangeEvent, function (propertyChangeData) {
    try {
        if (propertyChangeData.propertyName.match("quantity_")) {
            console.log("quantity field updated");
        }
    } catch(error){
        console.log(error);
    }
});

exports.got_to_list_detail = function (args) {

    var button = args.object;
    var item = button.bindingContext;
    config.selectedItem = item;
    navigation.goTolistDetailPage();

};

//exports.got_to_list_detail = function (args) {
//
//    var button = args.object;
//    var item = button.bindingContext;
//    config.selectedItem = item;
//    page.showModal("views/listdetail/listdetail",{},function closecallback(){
//        item.quantity = config.selectedItem.quantity;
//    },true);
//};

exports.increase_quantity = function(args){

    var button = args.object;
    var item = button.bindingContext;
    item.quantity = item.quantity + 1;
};

exports.decrease_quantity = function(args) {

    var button = args.object;
    var item = button.bindingContext;

    if (item.quantity) {
       item.quantity = item.quantity - 1;
    }

    console.log(JSON.stringify(item));

};

exports.list = navigation.goToListPage;
exports.checkout = function (){
    if(config.storeId && config.cart != null && Object.keys(config.cart).length > 0 ){
        navigation.checkout();
    } else{
        dialogsModule.alert({
            message: "Please Select Something",
            okButtonText: "OK"
        });
    }
};
exports.store = function(){
    delete config.storeId;
    navigation.goToStorePage();
};

