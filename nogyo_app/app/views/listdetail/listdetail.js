var Observable = require("data/observable").Observable;
var ObservableArray = require("data/observable-array").ObservableArray;
var viewModule = require("ui/core/view");
var ListingDetailViewModel = require("../../shared/view-models/listing-detail-view-model");
var navigation = require ("../../shared/navigation");
var config = require("../../shared/config");
var dialogsModule = require("ui/dialogs");

var page;

var listDetailModel = new ListingDetailViewModel(
    {name: config.selectedItem.name,
    id: config.selectedItem.id,
    quantity: config.selectedItem.quantity});

exports.loaded = function(args) {
    var page = args.object;
    page.bindingContext = listDetailModel;
    listDetailModel.load();
};

exports.add_to_cart = function(){
    var item = listDetailModel
    if(!item.quantity){
        dialogsModule.alert({
            message: "Add a quantity for " + item.name,
            okButtonText: "OK"
        });
        return;
    }

    var demand = {skuCode:item.id, storeId:config.storeId,quantity:item.quantity, name: item.name};
    config.cart[item.id] = demand;

    dialogsModule.confirm({
        title: "Added to cart Successfully !!!",
        message: "Do you wish to Checkout ?",
        okButtonText: "Yes",
        cancelButtonText: "No Choose More",
    }).then(function (result) {

        delete config.selectedItem;
        if(result){
            navigation.checkout();
        } else{
            navigation.goToListPage();
        }
    });
};


exports.remove_from_cart = function(){
    delete config.selectedItem;
    delete config.cart[listDetailModel.id];
    navigation.goToListPage();
};

exports.list = function(){
    delete config.selectedItem;
    navigation.goToListPage();
}
exports.checkout = navigation.checkout;

