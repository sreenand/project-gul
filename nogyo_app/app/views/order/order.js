var Observable = require("data/observable").Observable;
var ObservableArray = require("data/observable-array").ObservableArray;
var viewModule = require("ui/core/view");
var OrderViewModel = require("../../shared/view-models/order-view-model");
var navigation = require ("../../shared/navigation");
var config = require("../../shared/config");
var dialogsModule = require("ui/dialogs");


var page;
var order = new OrderViewModel({
     order_id: config.confirmed_order.id,
     order_status: config.confirmed_order.status
    });


exports.loaded = function(args) {
    var page = args.object;
    page.bindingContext = order;
    order.load();
};

exports.store = navigation.goToStorePage;
exports.logout = navigation.signOut;