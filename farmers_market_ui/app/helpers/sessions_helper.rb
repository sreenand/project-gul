module SessionsHelper


  def login_user(login_request)
    response = RestClient.post @backend_url + "/users/login", login_request.to_json, :content_type => :json
    cookies = response.headers[:set_cookie][0]
    user = JSON.parse(response)
    print user.to_json
    if(!(user['type']== "ADMIN"))
      redirect_to '/login'
      return
    end
    session_data = {}
    split_cookies = cookies.to_s.split(";")

    print split_cookies.to_json

    split_cookies.each do |split_cookie|
      if(split_cookie.include? "SESSIONID" )
        session_data[:jsessionId] = split_cookie.split("=")[1]
        break;
      end
    end
    current_user = SessionUser.new
    current_user.username = login_request["username"]
    current_user.session_data = session_data
    session[:user] = current_user
    redirect_to "/stores"
  end

  def get_current_user
     return session[:user]
  end

end