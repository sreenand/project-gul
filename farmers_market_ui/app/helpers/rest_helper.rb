module RestHelper

  def session_url(given_url)
    return given_url + ";jsessionid=#{session[:user].session_data[:jsessionId]}"
  end

  def validate(response)
    if response.code == 401
      render :template => 'errors/unauthorised'
    end
  end

end