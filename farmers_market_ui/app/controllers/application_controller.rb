class ApplicationController < ActionController::Base
  include ApplicationHelper
  before_filter :set_config

  def index

    if(session[:user].nil?)
      render :template => 'default/index'
    else
      render :template => 'demand/demand_add_view'
    end
  end
end