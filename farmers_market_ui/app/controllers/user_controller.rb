class UserController < ApplicationController
  include RestHelper


  def view_users
    render :template => 'user/user_view'
  end

  def get_user
    render :json =>  JSON.parse(RestClient.get session_url(@backend_url + "/users/find/" + params[:emailId]))
  end

  def update_user
    payload = params["user"]
    render :json =>  JSON.parse(RestClient.put session_url(@backend_url + "/users/update/" + payload[:emailId]),
                                               payload.to_json, :content_type => 'application/json')
  end

  def create_user
    payload = params["user"]
    RestClient.post session_url(@backend_url + "/users/register"), payload.to_json, :content_type => 'application/json'
    render :json => {}
  end

end