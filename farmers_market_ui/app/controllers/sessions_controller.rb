require 'restclient'
require 'json'


class SessionsController < ApplicationController
  include SessionsHelper
  before_filter :set_config

  def new
  end

  def create
    login_user({"username" => params["username"],"password" => params["password"]})
  end

  def destroy
    session[:user] = nil
    render :json => {}
  end
end