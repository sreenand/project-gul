class StoreController < ApplicationController
  include RestHelper
  before_filter :set_config

  def find_store
    name = params[:name]
    id = params[:id]
    locality = params[:locality]
    total_url = (@backend_url + "/stores?")
    total_url = total_url + "&id=#{id}" if !id.nil?
    total_url = total_url + "&name=#{name}" if !name.nil?
    render :json => (RestClient.get session_url(total_url))
  end

  def add_store
    store = params[:store]
    total_url = (@backend_url + "/stores")
    response = RestClient.post session_url(total_url),store.to_json, :content_type => 'application/json'
    render :json => response
  end

  def update_store
    store = params[:store]
    total_url = (@backend_url + "/stores/" + store['id'] )
    response = RestClient.put session_url(total_url),store.to_json, :content_type => 'application/json'
    render :json => response
  end

  def get_all_skus
    total_url = (@backend_url + "/sku")
    @allSKUs = JSON.parse(RestClient.get session_url(total_url))
    render :template => 'SKU/sku_view'
  end

  def create_sku
    total_url = (@backend_url + "/sku")
    payload = params["skupayload"]
    response_body = JSON.parse(RestClient.post session_url(total_url),payload.to_json, :content_type => :json)
    render :json => response_body
  end

end