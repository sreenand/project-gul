require 'restclient'
require 'json'

class DemandController < ApplicationController
  before_filter :set_config

  def demand_view
    response_body = RestClient.get @backend_url + "/demand/store/#{params[:storeId]}" + ";jsessionid=#{session[:user].session_data[:jsessionId]}"
    @Demands = JSON.parse(response_body)
    render :template => 'demand/demand_view'
  end


  def demand_add_action
    response_body = RestClient.get @backend_url + "/demand/store/#{params[:storeId]}" + ";jsessionid=#{session[:user].session_data[:jsessionId]}"
    @Demands = JSON.parse(response_body)
  end

  def demand_add_view
    render :template => 'demand/demand_add_view'
  end

  def wastage_add_view
    render :template => 'wastage/wastage_add_view'
  end

  def wastage_view
    response_body = RestClient.get @backend_url + "/wastage/store/#{params[:storeId]}" + ";jsessionid=#{session[:user].session_data[:jsessionId]}"
    @Demands = JSON.parse(response_body)
  end

  def add_demand
    payload = params["demand"]
    response_body = RestClient.post @backend_url + "/demand/add" + ";jsessionid=#{session[:user].session_data[:jsessionId]}", payload.to_json, :content_type => :json
    render :json => {}
  end

  def add_wastage
    payload = params["_json"]
    response_body = RestClient.post @backend_url + "/wastage/add" + ";jsessionid=#{session[:user].session_data[:jsessionId]}", payload.to_json, :content_type => :json
    render :json => {}
  end
end