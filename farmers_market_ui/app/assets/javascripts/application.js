// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require jquery/jquery.js
//= require bootstrap/bootstrap
//= require bootstrap/bootstrap-dialog.min.js
//= require bootstrap/bootstrap-popover.js
//= require bootstrap/bootstrap-tooltip.js
//= require_tree .



$(function() {

    $('#login-form-link').click(function(e) {
        $("#login-form").delay(100).fadeIn(100);
        $("#register-form").fadeOut(100);
        $('#register-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });
    $('#register-form-link').click(function(e) {
        $("#register-form").delay(100).fadeIn(100);
        $("#login-form").fadeOut(100);
        $('#login-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });

});


function add_demand (){
    var fruit_vegetable = (($("#fruit_vegetable").val()).toUpperCase() == "YES") ? 1 : 0;
    var payload = {};
    payload["storeId"] = $("#storeId").val();
    payload["skuCode"] = $("#skucode").val();
    payload["quantity"] = $("#quantity").val();
    payload["vegetableFruitFlag"] = fruit_vegetable;


    jQuery.ajax({
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'url': window.location.origin + "/demand/add",
        'Content-Type': 'application/json',
        'data': JSON.stringify(payload),
        'type':'POST',
        'dataType': 'json',
        'success': function(response){
            alert("Your demand has been accepted");

        },
        'error':function (){
            alert("Sorry something went wrong terribly");
        }
    });
}


function add_sku(){

    var payload = {};
    payload["name"] = $("#sku_name").val();
    payload["category"] = $("#sku_category").val();
    payload["description"] = $("#sku_description").val();

    if(!payload['name']) {
        alert("Please add a name for the SKU");
        return false;
    }

    skupayload = {}
    skupayload['skupayload'] = payload;

    jQuery.ajax({
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'url': window.location.origin + "/sku/add",
        'Content-Type': 'application/json',
        'data': JSON.stringify(skupayload),
        'type':'POST',
        'dataType': 'json',
        'success': function(response){
            alert("SKU has been created");
        },
        'error':function (){
            alert("Sorry something went wrong terribly");
        }
    });
}


function add_wastage (){
    var fruit_vegetable = (($("#fruit_vegetable").val()).toUpperCase() == "YES") ? 1 : 0;
    var payload = {};

    payload["storeId"] = $("#storeId").val();
    payload["skucode"] = $("#skucode").val();
    payload["quantity"] = $("#quantity").val();
    payload["fruit_vegetable"] = fruit_vegetable;

    jQuery.ajax({
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'url': window.location.origin + "/wastage/add",
        'data': JSON.stringify(payload),
        'type':'post',
        'dataType': 'json',
        'success': function(response){
            alert("Wastage has been noted");

        },
        'error':function (){
            alert("Sorry something went wrong terribly");
        }
    });
}
