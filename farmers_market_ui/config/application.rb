require File.expand_path('../boot', __FILE__)

require 'rails/all'

if defined?(Bundler)
  Bundler.require(*Rails.groups(:assets => %w(development test)))
  # Bundler.require(:default, :assets, Rails.env)
end

module FarmersMarketUi
  class Application < Rails::Application
    config.encoding = "utf-8"
    config.filter_parameters += [:password]
    config.assets.enabled = true
    config.assets.version = '3.0'
    config.paths['log'] = "/var/log/farmers_market_ui/#{Rails.env}.log"
  end
end
