package com.challur.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

/**
 * Created by hariprasanthac on 07/08/16.
 */
@Entity @Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Vendor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "vendor_id")
    private Long vendor_id;

    private Long sourcing_mkt_id;

    private String vendor_name;

    private String owner_name;

    private String contact_number;

    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name = "sourcing_mkt_id",referencedColumnName = "sourcing_mkt_id", insertable = false, updatable = false)
    @Fetch(value = FetchMode.JOIN)
    private SourcingPoint sourcingPoint;
}
