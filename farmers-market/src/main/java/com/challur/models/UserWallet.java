package com.challur.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by srinandchallur on 08/05/16.
 */

@Entity @Getter @Setter
public class UserWallet extends AbstractTimeStamp{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long userId;

    private Double currentBalance;

    private Double upperLimit;

    private Double lowerLimit;
}
