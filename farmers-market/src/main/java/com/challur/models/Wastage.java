package com.challur.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by srinand.pc on 15/12/15.
 */
@Entity @Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class Wastage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long storeId;
    private Long skuCode;
    private Boolean vegetableFruitFlag;
    private int qunatity;
}
