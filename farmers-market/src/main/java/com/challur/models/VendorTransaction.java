package com.challur.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

/**
 * Created by hariprasanthac on 11/08/16.
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "vendor_transaction")
public class VendorTransaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    private Long po_id;
    private Long vendor_id;
    private String payment_mode;
    private String payment_status;
    private Double total_amount;
    private Double amout_paid;
    private Double pending_amount;

    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name = "vendor_id",referencedColumnName = "vendor_id", insertable = false, updatable = false)
    @Fetch(value = FetchMode.JOIN)
    private Vendor vendor;

    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name = "po_id",referencedColumnName = "po_id", insertable = false, updatable = false)
    @Fetch(value = FetchMode.JOIN)
    private PurchaseOrder purchaseOrder;

    public Long getPo_id() {
        return po_id;
    }

    public Long getVendor_id() {
        return vendor_id;
    }

    public String getPayment_mode() {
        return payment_mode;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public Double getTotal_amout() {
        return total_amount;
    }

    public Double getAmout_paid() {
        return amout_paid;
    }

    public Double getPending_amount() {
        return pending_amount;
    }
}
