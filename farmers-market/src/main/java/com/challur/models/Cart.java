package com.challur.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

/**
 * Created by hariprasanthac on 10/08/16.
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "purchase_cart")
public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    private Long sku_code;
    private Integer order_qty;
    private Long sourcing_mkt_id;
    private Long vendor_id;
    @ManyToOne(fetch = FetchType.EAGER, cascade= CascadeType.ALL)
    @JoinColumn(name = "sourcing_mkt_id",referencedColumnName = "sourcing_mkt_id", insertable = false, updatable = false)
    @Fetch(value = FetchMode.JOIN)
    private SourcingPoint sourcingPoint;

    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name = "vendor_id",referencedColumnName = "vendor_id", insertable = false, updatable = false)
    @Fetch(value = FetchMode.JOIN)
    private Vendor vendor;

    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name = "sku_code",referencedColumnName = "id", insertable = false, updatable = false)
    @Fetch(value = FetchMode.JOIN)
    private Product product;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSku_code() {
        return sku_code;
    }

    public void setSku_code(Long sku_code) {
        this.sku_code = sku_code;
    }

    public Integer getOrder_qty() {
        return order_qty;
    }

    public void setOrder_qty(Integer order_qty) {
        this.order_qty = order_qty;
    }

    public Long getSourcing_mkt_id() {
        return sourcing_mkt_id;
    }

    public void setSourcing_mkt_id(Long sourcing_mkt_id) {
        this.sourcing_mkt_id = sourcing_mkt_id;
    }

    public Long getVendor_id() {
        return vendor_id;
    }

    public void setVendor_id(Long vendor_id) {
        this.vendor_id = vendor_id;
    }
}
