package com.challur.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;

/**
 * Created by srinand.pc on 22/12/15.
 */

@Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class UserLoginResponse {

    private Long id;
    private String name;
    private String emailId;
    private String type;
    private String phoneNo;
    private String address;
    private UserWallet userWallet;

}
