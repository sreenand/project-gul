package com.challur.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by srinand.pc on 04/02/16.
 */

@Getter @Setter @Entity @AllArgsConstructor @NoArgsConstructor
public class Order extends AbstractTimeStamp {


    public enum OrderStatus {RECEIVED, PROCESSING, PACKED, DISPATCHED, DELIVERED, CANCELLED}

    public enum Channel {STORE, DIRECT, OTHER}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId", referencedColumnName = "id",updatable=false,insertable = false)
    private User user;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "orderId")
    @Fetch(value = FetchMode.SELECT)
    @Cascade({CascadeType.SAVE_UPDATE})
    private List<OrderItem> orderItems;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "orderId")
    @Fetch(value = FetchMode.JOIN)
    @Cascade({CascadeType.SAVE_UPDATE})
    private List<OrderPayment> orderPayment;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "storeId", referencedColumnName = "id", updatable=false,insertable = false)
    private Store store;


    private String fulfillmentStatus;

    private String returnStatus;

    private Double pricePromised;

    private Double priceCalculated;

    private Double discount;

    private Long userId;

    @NotNull
    private String status;

    private Date orderDate;

    private Long storeId;

    private String channel;

    public OrderPayment getOrderPayment(){

        if(orderPayment != null && orderPayment.size() > 0)
          return orderPayment.get(0);

        return null;
    }

    public void setOrderPayment(OrderPayment orderPayment){
        if(this.orderPayment == null){
            this.orderPayment = new ArrayList<OrderPayment>();
        } else if(this.orderPayment.size() > 0){
            this.orderPayment.remove(0);
        }
        this.orderPayment.add(orderPayment);
    }


}
