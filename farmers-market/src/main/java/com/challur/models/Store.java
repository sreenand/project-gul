package com.challur.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by srinand.pc on 15/12/15.
 */

@Getter @Setter @AllArgsConstructor @Entity @NoArgsConstructor
public class Store extends AbstractTimeStamp {


    public enum StoreType {SMALL, MEDIUM, LARGE}

    public enum StoreStatus {ENABLED, DISABLED}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    private String name;
    @NotNull
    private String contactPerson;

    private Long userId;

    private Long salesUserId;
    @NotNull
    private String type;
    @NotNull
    private Long localityId;

    @OneToOne(mappedBy = "", fetch = FetchType.EAGER)
    @JoinColumn(name = "localityId", referencedColumnName = "id", updatable=false,insertable = false)
    private Locality locality;

    @NotNull
    private String phoneNo;
    @NotNull
    private String address;

    @NotNull
    private String status;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId", referencedColumnName = "userId", updatable=false,insertable = false)
    private UserWallet userWallet;

}
