package com.challur.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;

/**
 * Created by srinandchallur on 09/05/16.
 */

@Entity @Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class TransactionItem extends AbstractTimeStamp {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonIgnore
    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "transactionId", referencedColumnName = "id")
    @Cascade(value={org.hibernate.annotations.CascadeType.ALL})
    private Transaction transaction;

    private String externalId;

    private Double amount;

    private String reconcilationStatus;

    private String reconcilationTxnItemId;

}
