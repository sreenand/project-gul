package com.challur.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * Created by srinandchallur on 19/04/16.
 */

@Entity @Getter @Setter @NoArgsConstructor
public class ProductUnit extends AbstractTimeStamp{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String unitName;

    @NotNull
    private Double unitQuantity;

    @NotNull
    private String unitMetric;

    @NotNull
    private String unitType;

    private String unitDescription;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public Double getUnitQuantity() {
        return unitQuantity;
    }

    public void setUnitQuantity(Double unitQuantity) {
        this.unitQuantity = unitQuantity;
    }

    public String getUnitMetric() {
        return unitMetric;
    }

    public void setUnitMetric(String unitMetric) {
        this.unitMetric = unitMetric;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public String getUnitDescription() {
        return unitDescription;
    }

    public void setUnitDescription(String unitDescription) {
        this.unitDescription = unitDescription;
    }
}
