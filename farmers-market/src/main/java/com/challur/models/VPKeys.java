package com.challur.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Created by hariprasanthac on 19/07/16.
 */
@Embeddable @Getter @Setter
public class VPKeys implements Serializable {
    private Long productId;
    private int bucketId;
}
