package com.challur.models;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by srinand.pc on 13/03/16.
 */

@Entity @Getter @Setter @AllArgsConstructor @NoArgsConstructor @Table(name="cities")
public class City extends AbstractTimeStamp {


    private static String [] states = {"tamil nadu","andhra pradesh","karnataka","kerala","maharastra", "telangana"};

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String state;
    private Boolean serviceable;
    private String extraAttributes;

    public static String[] getAllStates(){
        return states;
    }
}
