package com.challur.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by srinand.pc on 13/03/16.
 */

@Entity @Getter @Setter @AllArgsConstructor @NoArgsConstructor @Table(name="localities")
public class Locality {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @JoinColumn(name ="cityId", referencedColumnName = "id" ,insertable = false,updatable = false)
    @OneToOne(fetch = FetchType.EAGER)
    private City city;

    private Long cityId;

    private boolean serviceable;

    private String pincode;

    private Double lattitudeSouth;
    private Double lattitudeNorth;
    private Double longitudeEast;
    private Double longitudeWest;
}
