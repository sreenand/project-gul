package com.challur.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.dropwizard.validation.ValidationMethod;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by srinand.pc on 15/12/15.
 */

@Entity @Getter @Setter @AllArgsConstructor
public class OrderItem extends AbstractTimeStamp {

    public enum Status {PROCURED, NOT_PROCURED,PARTIALLY_PROCURED, RECEIVED}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JsonIgnore
    @JoinColumn(name = "orderId", referencedColumnName = "id", insertable = false, updatable = false)
    private Order order;

    private Long orderId;

    @NotNull
    private Long storeId;
    @NotNull
    private Long skuCode;

    private Double pricePerUnit;

    private Long productUnitId;

    private Double priceOfferedPerUnit;

    @OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name = "productUnitId", referencedColumnName = "id", insertable = false, updatable = false)
    private ProductUnit productUnit;

    private Double quantityProcured;

    private Double discount;

    private String fulfillmentStatus;

    private String returnStatus;

    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name = "skuCode",referencedColumnName = "id", insertable = false, updatable = false)
    private Product product;

    private String status;

    private int quantity;

    private int bucketid;
    public OrderItem(){
        this.setStatus(Status.RECEIVED.toString());
    }

}
