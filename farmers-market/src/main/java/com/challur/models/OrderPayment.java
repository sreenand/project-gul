package com.challur.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.List;

/**
 * Created by srinand.pc on 13/03/16.
 */

@Entity @Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class OrderPayment extends AbstractTimeStamp {


    public enum OrderPaymentStatus {PENDING, RECEIVED, REFUNDED, PARTIALLY_RECEIVED, PARTIALLY_REFUNDED}
    public enum OrderPaymentMode {COD, NET_BANKING, CREDIT_CARD, WALLET}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JsonIgnore
    @JoinColumn(name = "orderId", referencedColumnName = "id")
    @Cascade(value={org.hibernate.annotations.CascadeType.ALL})
    private Order order;


    @ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    @JsonIgnore
    @JoinColumn(name = "transactionId", referencedColumnName = "externalId")
    @Where(clause = "transaction_type = 'WALLET_DEBIT'")
    private Transaction transaction;

    private String notes;

    private String status;



}
