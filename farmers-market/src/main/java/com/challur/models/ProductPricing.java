package com.challur.models;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by srinand.pc on 13/03/16.
 */

@Entity @Getter @Setter @AllArgsConstructor @NoArgsConstructor
@Table(name = "product_pricing")
public class ProductPricing {

    public enum Status {ENABLED, DISABLED};

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long localityId;

    @NotNull
    private Long cityId;

    @NotNull
    private Long productId;

    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name = "productId",referencedColumnName = "id", insertable = false, updatable = false)
    @Fetch(value = FetchMode.JOIN)
    private Product product;

    private Long unitId;

    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name = "unitId",referencedColumnName = "id", insertable = false, updatable = false)
    @Fetch(value = FetchMode.JOIN)
    private ProductUnit productUnit;

    private Double pricePerUnit;

    private Double minQuantity;

    private Double maxQuantity;

    private String quantityDescription;

    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLocalityId() {
        return localityId;
    }

    public void setLocalityId(Long localityId) {
        this.localityId = localityId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Long getUnitId() {
        return unitId;
    }

    public void setUnitId(Long unitId) {
        this.unitId = unitId;
    }

    public ProductUnit getProductUnit() {
        return productUnit;
    }

    public void setProductUnit(ProductUnit productUnit) {
        this.productUnit = productUnit;
    }

    public Double getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(Double pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public Double getMinQuantity() {
        return minQuantity;
    }

    public void setMinQuantity(Double minQuantity) {
        this.minQuantity = minQuantity;
    }

    public Double getMaxQuantity() {
        return maxQuantity;
    }

    public void setMaxQuantity(Double maxQuantity) {
        this.maxQuantity = maxQuantity;
    }

    public String getQuantityDescription() {
        return quantityDescription;
    }

    public void setQuantityDescription(String quantityDescription) {
        this.quantityDescription = quantityDescription;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<VariablePricing> getVariablePricings() {
        return variablePricings;
    }

    public void setVariablePricings(List<VariablePricing> variablePricings) {
        this.variablePricings = variablePricings;
    }

    @OneToMany(mappedBy = "productPricingId", cascade=CascadeType.ALL)
    private List<VariablePricing> variablePricings;



    @Override
    public String toString() {
        return "ProductPricing{" +
                "id=" + id +
                ", localityId=" + localityId +
                ", cityId=" + cityId +
                ", productId=" + productId +
                ", product=" + product +
                ", unitId=" + unitId +
                ", productUnit=" + productUnit +
                ", pricePerUnit=" + pricePerUnit +
                ", minQuantity=" + minQuantity +
                ", maxQuantity=" + maxQuantity +
                ", quantityDescription='" + quantityDescription + '\'' +
                ", status='" + status + '\'' +
                ", variablePricings=" + variablePricings +
                '}';
    }
}
