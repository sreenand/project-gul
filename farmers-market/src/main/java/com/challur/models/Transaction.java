package com.challur.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import java.util.Date;
import java.util.List;

/**
 * Created by srinandchallur on 08/05/16.
 */

@Entity @Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class Transaction extends AbstractTimeStamp {


    public static String INR = "INR";
    public enum PAYMENT_MODE{CASH, CHEQUE, STORE_CREDIT}

    public enum TRANSACTION_TYPE {MONEY_DEBIT, MONEY_CREDIT, WALLET_DEBIT, WALLET_CREDIT, MONEY_RECON}

    public enum RECON_STATUS {PENDING, COMPLETE, PARTIAL}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "transaction")
    @Fetch(value = FetchMode.JOIN)
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<TransactionItem> transactionItems;

    private String externalId;

    private Long donor;

    private Long recepient;

    private Double amount;

    private Double amountReconciled;

    private String reconcilationStatus;

    private String currency;

    private String type;

    private String paymentMode;

    private Date paymentDate;
}
