package com.challur.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Strings;
import io.dropwizard.validation.ValidationMethod;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;
import org.omg.PortableInterceptor.INACTIVE;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by srinand.pc on 15/04/16.
 */

@Entity @Getter @Setter @AllArgsConstructor @NoArgsConstructor
public class User extends AbstractTimeStamp{


    public enum UserStatus {INACTIVE, ACTIVE, DELETED}
    public enum UserType {SALES, ADMIN, STORE}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    @Column(name = "type")
    private String type;
    private String emailId;


    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name="userId")
    @Fetch(value = FetchMode.JOIN)
    @Cascade({CascadeType.SAVE_UPDATE})
    private List<UserWallet> userWallet;

    private String phoneNo;
    private String password;
    private String address;
    @Column(name = "status")
    private String status;


    @JsonIgnore
    public boolean isActive(){
        return this.status.equals(UserStatus.ACTIVE.toString());
    }


    @JsonIgnore
    @ValidationMethod(message="UserName or emailId or PhoneNo cannot be Empty")
    public boolean isValid() {
        return !(Strings.isNullOrEmpty(this.getName()) || Strings.isNullOrEmpty(this.getPhoneNo()));
    }

    @JsonIgnore
    public boolean isSalesPerson(){
        return this.type.equals(UserType.SALES.toString());
    }

    @JsonIgnore
    public boolean isAdmin(){
        return this.type.equals(UserType.ADMIN.toString());
    }

    @JsonIgnore
    public boolean isStore(){
        return this.type.equals(UserType.STORE.toString());
    }


    public UserWallet getUserWallet(){
        return this.userWallet.get(0);
    }

    public void setUserWallet(UserWallet wallet){
        userWallet = new ArrayList<UserWallet>();
        userWallet.add(wallet);
    }

    public Long getId() {
        return id;
    }
}
