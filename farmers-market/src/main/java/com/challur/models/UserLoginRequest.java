package com.challur.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Strings;
import io.dropwizard.validation.ValidationMethod;
import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Created by srinand.pc on 20/12/15.
 */

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class UserLoginRequest {


    private String username;
    private String password;

    @JsonIgnore
    @ValidationMethod(message="UserName or Password cannot be Empty")
    public boolean isValid() {
        return !(Strings.isNullOrEmpty(this.getUsername()) || Strings.isNullOrEmpty(this.getUsername()));
    }
}
