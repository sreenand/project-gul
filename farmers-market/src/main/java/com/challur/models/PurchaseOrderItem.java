package com.challur.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

/**
 * Created by hariprasanthac on 07/08/16.
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseOrderItem extends AbstractTimeStamp {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    private Long po_id;
    private Long sku_code;
    private Integer order_qty;
    private Integer procured_qty;
    private Double unit_price;
    private Long sourcing_mkt_id;
    private Long vendor_id;
    private Double total_amount;

    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name = "sourcing_mkt_id",referencedColumnName = "sourcing_mkt_id", insertable = false, updatable = false)
    @Fetch(value = FetchMode.JOIN)
    private SourcingPoint sourcingPoint;

    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name = "vendor_id",referencedColumnName = "vendor_id", insertable = false, updatable = false)
    @Fetch(value = FetchMode.JOIN)
    private Vendor vendor;

    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name = "sku_code",referencedColumnName = "id", insertable = false, updatable = false)
    @Fetch(value = FetchMode.JOIN)
    private Product product;

    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name = "po_id",referencedColumnName = "po_id", insertable = false, updatable = false)
    @Fetch(value = FetchMode.JOIN)
    private PurchaseOrder purchaseOrder;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPo_id() {
        return po_id;
    }

    public void setPo_id(Long po_id) {
        this.po_id = po_id;
    }

    public Long getSku_code() {
        return sku_code;
    }

    public void setSku_code(Long sku_code) {
        this.sku_code = sku_code;
    }

    public Integer getOrder_qty() {
        return order_qty;
    }

    public void setOrder_qty(Integer order_qty) {
        this.order_qty = order_qty;
    }

    public Integer getProcured_qty() {
        return procured_qty;
    }

    public void setProcured_qty(Integer procured_qty) {
        this.procured_qty = procured_qty;
    }

    public Double getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(Double unit_price) {
        this.unit_price = unit_price;
    }

    public Long getSourcing_mkt_id() {
        return sourcing_mkt_id;
    }

    public void setSourcing_mkt_id(Long sourcing_mkt_id) {
        this.sourcing_mkt_id = sourcing_mkt_id;
    }

    public Long getVendor_id() {
        return vendor_id;
    }

    public void setVendor_id(Long vendor_id) {
        this.vendor_id = vendor_id;
    }

    public Double getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(Double total_amount) {
        this.total_amount = total_amount;
    }

    public SourcingPoint getSourcingPoint() {
        return sourcingPoint;
    }

    public void setSourcingPoint(SourcingPoint sourcingPoint) {
        this.sourcingPoint = sourcingPoint;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
