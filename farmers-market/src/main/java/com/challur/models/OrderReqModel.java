package com.challur.models;

/**
 * Created by hariprasanthac on 09/08/16.
 */
public class OrderReqModel {
    public Long id;
    public Double total_amount;
    public String status;

    public OrderReqModel(Long id, Double total_amount, String status) {
        this.id = id;
        this.total_amount = total_amount;
        this.status = status;
    }
}
