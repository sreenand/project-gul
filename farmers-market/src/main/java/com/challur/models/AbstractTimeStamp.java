package com.challur.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Setter;
import lombok.Getter;

import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by srinand.pc on 15/12/15.
 */
@MappedSuperclass @Setter @Getter
public class AbstractTimeStamp implements Serializable {

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSSXXX", timezone="IST") //ISO8601 format
    protected Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSSXXX", timezone="IST") //ISO8601 format
    @Version
    protected Date updatedAt;  //Optimistic locking


    public AbstractTimeStamp()  {
        createdAt = new Date();
    }
}
