package com.challur.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by hariprasanthac on 19/07/16.
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "variable_pricing")
public class VariablePricing implements Serializable {
    @Id
    @NotNull
    @Column(name = "product_pricing_id")
    private Long productPricingId;

    @Id
    @NotNull
    @Column(name = "bucket_id")
    private int bucketId;

    private Double min;

    @Column(name = "max")
    private Double max;
    @Column(name = "discounted_price")
    private Double discountedPrice;

    @ManyToOne
    @JoinColumn(name = "product_pricing_id", referencedColumnName = "id")
    @Fetch(value = FetchMode.JOIN)
    private ProductPricing productPricing;

    @Override
    public String toString() {
        return "VariablePricing{" +
                "productPricingId=" + productPricingId +
                ", bucketId=" + bucketId +
                ", min=" + min +
                ", max=" + max +
                ", discountedPrice=" + discountedPrice +
                ", productPricing=" + productPricing +
                '}';
    }

    public Long getProductPricingId() {
        return productPricingId;
    }

    public void setProductPricingId(Long productPricingId) {
        this.productPricingId = productPricingId;
    }

    public int getBucketId() {
        return bucketId;
    }

    public void setBucketId(int bucketId) {
        this.bucketId = bucketId;
    }

    public Double getMin() {
        return min;
    }

    public void setMin(Double min) {
        this.min = min;
    }

    public Double getMax() {
        return max;
    }

    public void setMax(Double max) {
        this.max = max;
    }

    public Double getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(Double discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public ProductPricing getProductPricing() {
        return productPricing;
    }

    public void setProductPricing(ProductPricing productPricing) {
        this.productPricing = productPricing;
    }
}

//@Embeddable
//class VariablePricingId {
//
//    @Column(name = "product_id")
//    private int productId;
//    @Column(name = "bucket_id")
//    private int bucketId;
//}
