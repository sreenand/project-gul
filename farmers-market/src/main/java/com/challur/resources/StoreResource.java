package com.challur.resources;

import com.challur.annotations.SessionUser;
import com.challur.exceptions.AppError;
import com.challur.exceptions.FarmersMarketServiceException;
import com.challur.models.Store;
import com.challur.models.User;
import com.challur.services.orders.OrderService;
import com.challur.services.StoreService;
import com.google.inject.Inject;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.IntParam;
import io.dropwizard.jersey.params.LongParam;


import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URLDecoder;

/**
 * Created by srinand.pc on 22/12/15.
 */

@Path("/stores")

@Produces(MediaType.APPLICATION_JSON)
public class StoreResource {


    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(StoreResource.class);

    private StoreService storeService;
    private OrderService orderService;

    @Inject
    public StoreResource(StoreService storeService, OrderService orderService){
        this.storeService = storeService;
        this.orderService = orderService;
    }

    @POST @Consumes(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response addStore(Store store, @QueryParam("create_user")boolean createUser, @SessionUser User user){
        return Response.status(Response.Status.CREATED).entity(storeService.addStore(store,createUser)).build();
    }

    @PUT @Consumes(MediaType.APPLICATION_JSON) @UnitOfWork @Path("/{id}")
    public Response updateStore(Store store, @SessionUser User user){
        return Response.status(Response.Status.ACCEPTED).entity(storeService.updateStore(store)).build();
    }

//    @GET @UnitOfWork
//    public Response getStores(@QueryParam("id") Long id, @QueryParam("name") String name,@QueryParam("locality") String locality,
//                              @QueryParam("name") String pincode,@QueryParam("name") String type,@QueryParam("city") String city,
//                              @QueryParam("state") String state, @SessionUser User user){
//
//        if(name != null) {
//            name = URLDecoder.decode(name);
//        }
//        return Response.status(Response.Status.OK).entity(storeService.getStores(id,name,locality,pincode,type,city,state)).build();
//
//    }

    @GET @Path("/all") @UnitOfWork
    public Response getAllStores(@QueryParam("limit") IntParam limit, @QueryParam("offset") IntParam offset,
                                 @QueryParam("all") boolean all, @QueryParam("city_id")Long cityId, @SessionUser User user){
        if(user.isStore())
            throw new FarmersMarketServiceException(401, AppError.NOT_AUTHORIZED,"Not Authorized");
        return Response.status(Response.Status.OK).entity(storeService.getAllStores(limit.get(),offset.get(),all,cityId)).build();

    }

    @GET @Path("/name/{name}") @UnitOfWork
    public Response getStores(@PathParam("name")String name, @SessionUser User user){
        name = URLDecoder.decode(name);
        return Response.status(Response.Status.OK).entity(storeService.getStoresByname(name)).build();
    }

//    @GET @Path("/locality/{locality}") @UnitOfWork
//    public Response getStoresByLocality(@PathParam("locality")String locality, @SessionUser User user){
//        return Response.status(Response.Status.OK).entity(storeService.getStores(null, null,"%"+locality+"%",null,null,null,null)).build();
//    }


    @GET @Path("/{storeId}/orders") @UnitOfWork
    public Response getOrdersForStore(@PathParam("storeId")LongParam storeId, @QueryParam("limit") IntParam limit,
                                      @QueryParam("offset") IntParam offset,  @SessionUser User user){

        boolean getResults = false;
        Response response = null;
        if(user.isAdmin() || user.isSalesPerson()){
            getResults = true;
        } else if(user.isStore()){
            Store storeSelected = storeService.getStoreById(storeId.get());
            if(storeSelected.getUserId().equals(user.getId())){
                getResults = true;
            }
        }

        if(getResults){
            response = Response.status(Response.Status.OK).entity(orderService.getOrders(storeId.get(),limit.get(),offset.get())).build();
        } else{
            response = Response.status(Response.Status.UNAUTHORIZED).build();
        }

        return response;
    }

    @GET @UnitOfWork @Path("/mystores")
    public Response getStores(@SessionUser User user, @QueryParam("limit")IntParam limit, @QueryParam("offset") IntParam offset){

        int lim = 20,oset = 0;

        if(limit != null)
            lim = limit.get();
        if(offset != null)
            oset = offset.get();

        return Response.status(Response.Status.OK).entity(storeService.getStoresForUser(user,lim,oset)).build();
    }

}
