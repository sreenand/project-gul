package com.challur.resources;

import com.challur.annotations.SessionUser;
import com.challur.exceptions.FarmersMarketException;
import com.challur.models.*;
import com.challur.services.ProductService;
import com.google.inject.Inject;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.LongParam;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by srinand.pc on 31/12/15.
 */

@Path("/sku")
@Consumes(MediaType.APPLICATION_JSON)
public class ProductResource {


    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ProductResource.class);

    private ProductService productService;

    @Inject
    public ProductResource(ProductService productService){
        this.productService = productService;
    }

    @GET @Produces(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response getAllProducts(@QueryParam("sub_category_id")LongParam subCatId, @SessionUser User user){

        try{
            Long subcatid = 0L;

            if(subcatid != null)
                subcatid = subCatId.get();
            return Response.status(Response.Status.OK).entity(productService.getAllSkus(subcatid)).build();
        }catch (Exception e){
            logger.error("Error in getting products",e);
            throw new FarmersMarketException(e.getMessage(),e);
        }
    }

    @GET @Path("/name/{name}") @UnitOfWork
    public Response getProducts(@PathParam("name")String name, @SessionUser User user){
        name = URLDecoder.decode(name);
        List<Product> products = productService.getByName(name);
        return Response.status(Response.Status.OK).entity(products.subList(0,15)).build();
    }

    @POST @Produces(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response createProduct(@Valid Product product, @SessionUser User user){
        try{
            return Response.status(Response.Status.CREATED).entity(productService.create(product)).build();
        }catch (Exception e){
            logger.error("Error in creating product",e);
            throw new FarmersMarketException(e.getMessage(),e);
        }

    }

//    @GET @Path("/query") @UnitOfWork
//    public Response getProducts(@PathParam("names")String names, @PathParam("ids")String ids, @SessionUser User user){
//        try{
//
//            String[] namesArray;
//            String[] idsArray;
//            if(names != null)
//              namesArray = names.split(",");
//            if(ids != null)
//                idsArray = ids.split(",");
//            return Response.status(Response.Status.OK).entity(productService.getAllSkus()).build();
//        }catch (Exception e){
//            logger.error("Error in creating product",e);
//            throw new FarmersMarketException(e.getMessage(),e);
//        }
//
//    }

    @GET @Path("/query") @UnitOfWork @Produces(MediaType.APPLICATION_JSON)
    public Response getProductsForCity(@QueryParam("city_id")LongParam cityId ,
                                           @QueryParam("locality_id") LongParam localityId,
                                            @QueryParam("sub_cat_id") LongParam subCatId,
                                            @QueryParam("enabled") boolean enabled
                                           ){

        Long localityid = null;
        if(localityId != null){
            localityid = localityId.get();
        }
        try{
            List<ProductPricing> productpricings = productService.getProductsWithPricing(cityId.get(),subCatId.get(),enabled);
            List<ProductPricing> mix = new ArrayList<ProductPricing>();
            mix.addAll(productpricings);
            return Response.status(Response.Status.OK).entity(mix).build();
        }catch (Exception e){
            logger.error("Error in getting product",e);
            throw new FarmersMarketException(e.getMessage(),e);
        }
    }

    @GET @Path("/variableprice") @UnitOfWork @Produces(MediaType.APPLICATION_JSON)
    public Response getVariablePrice(@QueryParam("product_id")LongParam productId){

        try{
            List<VariablePricing> variablepricings = productService.getProductsVariablePricing(productId.get());
            return Response.status(Response.Status.OK).entity(variablepricings).build();
        }catch (Exception e){
            logger.error("Error in getting product",e);
            throw new FarmersMarketException(e.getMessage(),e);
        }
    }

    @PUT  @Path("/pricing") @UnitOfWork @Produces(MediaType.APPLICATION_JSON)
    public Response updatePricing(@SessionUser User user, @Valid ProductPricing productPricing){

        try{
            productService.updatePricing(productPricing);
            return Response.status(Response.Status.OK).build();
        }catch (Exception e){
            logger.error("Error in getting product",e);
            throw new FarmersMarketException(e.getMessage(),e);
        }
    }


    @GET @Path("/category/{categoryId}/subcategories") @UnitOfWork @Produces(MediaType.APPLICATION_JSON)
    public Response getAllSubCategores(@PathParam("categoryId")LongParam catId, @SessionUser User user){

        try{
            List<SubCategory> subCategories  = productService.getAllSubCategories(catId.get());
            return Response.status(Response.Status.OK).entity(subCategories).build();
        }catch (Exception e){
            logger.error("Error in getting product",e);
            throw new FarmersMarketException(e.getMessage(),e);
        }
    }


}
