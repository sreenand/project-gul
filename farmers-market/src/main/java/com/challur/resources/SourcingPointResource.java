package com.challur.resources;

import com.challur.exceptions.FarmersMarketException;
import com.challur.services.SourcingPointService;
import com.google.inject.Inject;
import io.dropwizard.hibernate.UnitOfWork;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by hariprasanthac on 10/08/16.
 */
@Path("/markets")
@Produces(MediaType.APPLICATION_JSON)
public class SourcingPointResource {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(SourcingPointResource.class);
    private SourcingPointService soucingPointService;

    @Inject
    public SourcingPointResource(SourcingPointService soucingPointService){
        this.soucingPointService = soucingPointService;
    }

    @GET
    @Path("/getall") @Consumes(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response getAllMarkets(){
        try{
            return Response.status(Response.Status.OK).entity(soucingPointService.getAllMarkets()).build();
        }catch (Exception e){
            e.printStackTrace();
            throw new FarmersMarketException(e.getMessage(), e);
        }
    }
}
