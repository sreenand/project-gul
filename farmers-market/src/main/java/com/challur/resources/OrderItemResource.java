package com.challur.resources;

import com.challur.annotations.SessionUser;
import com.challur.exceptions.FarmersMarketException;
import com.challur.models.Order;
import com.challur.models.OrderItem;
import com.challur.models.User;
import com.challur.services.orders.OrderItemService;
import com.google.inject.Inject;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.LongParam;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by srinand.pc on 17/12/15.
 */

@Path("/order_items")
@Produces(MediaType.APPLICATION_JSON)
public class OrderItemResource {


    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(OrderItemResource.class);

    private OrderItemService orderItemService;


    @Inject
    public OrderItemResource(OrderItemService orderItemService){
        this.orderItemService = orderItemService;
    }


//    @POST @UnitOfWork @Path("/add")@Consumes(MediaType.APPLICATION_JSON)
//    public Response addDemand(@Valid OrderItem orderItem, @SessionUser User user) {
//        orderItemService.addDemand(orderItem);
//        return Response.status(Response.Status.CREATED).build();
//    }
//
//    @POST @UnitOfWork @Path("/add/bulk")@Consumes(MediaType.APPLICATION_JSON)
//    public Response addDemands(@Valid List<OrderItem> orderItem, @SessionUser User user) {
//        orderItemService.addDemands(orderItem);
//        return Response.status(Response.Status.CREATED).build();
//    }

    @GET @UnitOfWork(readOnly = true) @Path("/store/{storeID}")
    public Response getAllDemands(@PathParam("storeID")LongParam storeId, @SessionUser User user) {

        try{
            return Response.status(Response.Status.OK).entity(orderItemService.getOrderItems(storeId.get(),0,1000)).build();
        }catch (Exception e){
            logger.error("Error in getting demands",e);
            throw new FarmersMarketException("Error in getting order Items",e);

        }
    }

    @PUT @Path("/{order_item_id}/status/{status}") @Consumes(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response updateOrderItem(@PathParam("order_item_id") Long orderItemId
            ,@PathParam("status") String status, @SessionUser User user){


        try{
            OrderItem oItem = orderItemService.getOrderItem(orderItemId);
            oItem.setStatus(OrderItem.Status.valueOf(status).toString());
            Order order = oItem.getOrder();
            return Response.status(Response.Status.OK).entity(orderItemService.updateOrderItem(oItem)).build();

        }catch (Exception e){
            logger.error("Error in getting demands",e);
            throw new FarmersMarketException(e.getMessage(), e);
        }
    }

}
