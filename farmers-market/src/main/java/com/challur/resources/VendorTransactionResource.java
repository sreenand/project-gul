package com.challur.resources;

import com.challur.exceptions.FarmersMarketException;
import com.challur.models.VendorTransaction;
import com.challur.services.VendorTransactionService;
import com.google.inject.Inject;
import io.dropwizard.hibernate.UnitOfWork;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by hariprasanthac on 12/08/16.
 */
@Path("/vendor_transaction")
@Produces(MediaType.APPLICATION_JSON)
public class VendorTransactionResource {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(VendorTransactionResource.class);
    private VendorTransactionService vendorTransactionService;

    @Inject
    public VendorTransactionResource(VendorTransactionService vendorTransactionService){
        this.vendorTransactionService = vendorTransactionService;
    }

    @GET
    @Path("/getall") @Consumes(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response getAllVendors(){
        try{
            return Response.status(Response.Status.OK).entity(vendorTransactionService.getAllVendorTransactions()).build();
        }catch (Exception e){
            throw new FarmersMarketException(e.getMessage(), e);
        }
    }

    @GET
    @Path("/{vendor_id}") @Consumes(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response getVendorTransactions(@PathParam("vendor_id") String vendor_id){
        try{
            return Response.status(Response.Status.OK).entity(vendorTransactionService.getVendorTransaction(Long.parseLong(vendor_id))).build();
        }catch (Exception e){
            throw new FarmersMarketException(e.getMessage(), e);
        }
    }

    @GET
    @Path("/{order_id}/{vendor_id}") @Consumes(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response getVendorTransaction(@PathParam("order_id") String order_id, @PathParam("vendor_id") String vendor_id){
        try{
            return Response.status(Response.Status.OK).entity(vendorTransactionService.getVendorTransactions(Long.parseLong(order_id), Long.parseLong(vendor_id))).build();
        }catch (Exception e){
            throw new FarmersMarketException(e.getMessage(), e);
        }
    }

    @POST
    @Path("/add") @Consumes(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response insertVendorTransaction(@Valid VendorTransaction vendorTransaction){
        try{
            vendorTransactionService.add(vendorTransaction);
            return Response.status(Response.Status.OK).build();
        }catch (Exception e){
            e.printStackTrace();
            throw new FarmersMarketException(e.getMessage(), e);
        }
    }
}
