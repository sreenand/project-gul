package com.challur.resources;

import com.challur.exceptions.FarmersMarketException;
import com.challur.models.PurchaseOrder;
import com.challur.models.Vendor;
import com.challur.services.VendorService;
import com.google.inject.Inject;
import io.dropwizard.hibernate.UnitOfWork;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by hariprasanthac on 10/08/16.
 */
@Path("/vendors")
@Produces(MediaType.APPLICATION_JSON)
public class VendorResource {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(VendorResource.class);
    private VendorService vendorService;

    @Inject
    public VendorResource(VendorService vendorService){
        this.vendorService = vendorService;
    }

    @GET
    @Path("/getall") @Consumes(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response getAllVendors(@QueryParam("marketSelected") String marketSelected){
        try{
            return Response.status(Response.Status.OK).entity(vendorService.getAllVendors(marketSelected)).build();
        }catch (Exception e){
            throw new FarmersMarketException(e.getMessage(), e);
        }
    }

    @GET
    @Path("/") @Consumes(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response getVendors(){
        try{
            return Response.status(Response.Status.OK).entity(vendorService.getVendors()).build();
        }catch (Exception e){
            throw new FarmersMarketException(e.getMessage(), e);
        }
    }

    @POST
    @Path("/add") @Consumes(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response addVendor(@Valid Vendor vendor){
        try{
            return Response.status(Response.Status.OK).entity(vendorService.addVendor(vendor)).build();
        }catch (Exception e){
            throw  new FarmersMarketException(e.getMessage(), e);
        }
    }
}
