package com.challur.resources;

import com.challur.annotations.SessionUser;
import com.challur.exceptions.AppError;
import com.challur.exceptions.FarmersMarketException;
import com.challur.exceptions.FarmersMarketServiceException;
import com.challur.models.Order;
import com.challur.models.OrderItem;
import com.challur.models.User;
import com.challur.services.orders.OrderItemService;
import com.challur.services.orders.OrderService;
import com.google.inject.Inject;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.IntParam;
import io.dropwizard.jersey.params.LongParam;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;


/**
 * Created by srinand.pc on 08/02/16.
 */

@Path("/orders")
@Produces(MediaType.APPLICATION_JSON)
public class OrderResource {

    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(OrderResource.class);


    private OrderService orderService;
    private OrderItemService orderItemService;

    @Inject
    public OrderResource(OrderService orderService,OrderItemService orderItemService){
        this.orderService = orderService;
        this.orderItemService = orderItemService;
    }


    @GET @Path("/{order_id}") @UnitOfWork
    public Response getOrder(@PathParam("order_id") Long orderId,@SessionUser User user){

        try{
            Order order = orderService.findOrder(orderId);
            if(order == null){
                throw new FarmersMarketException("Unable to find the order");
            }
            for(OrderItem orderItem: order.getOrderItems()){
                orderItem.getProduct();
            }
            return Response.status(Response.Status.OK).entity(order).build();
        }catch (Exception e){
            logger.error("Error in Retrieving Order" ,e);
            throw  new FarmersMarketServiceException(404, AppError.NOT_FOUND,"Unable to find the order");
        }

    }

    @POST @Consumes(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response createOrder(@Valid Order order, @SessionUser User user){
        try {
            order.setUser(user);
            order = orderService.createOrder(order);
            order.getOrderPayment().getTransaction();
            return Response.status(Response.Status.CREATED).entity(order).build();
        } catch (Exception e){
            logger.error("Error in Creating Order" ,e);
            throw new FarmersMarketException(e.getMessage(), e);
        }

    }


    @PUT @Consumes(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response updateOrder(@Valid Order order, @SessionUser User user){

        try {

            Order persistentOrder = orderService.findOrder(order.getId());
            persistentOrder.getOrderItems();
            orderService.applyUpdate(persistentOrder,order);
            orderService.updateOrder(persistentOrder);
            return Response.status(Response.Status.OK).entity(persistentOrder).build();
        } catch (Exception e){
            logger.error("Error in Creating Order" ,e);
            throw new FarmersMarketException(e.getMessage(), e);
        }

    }

    @PUT @Path("/{order_ids}/status/{status}") @Consumes(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response updateOrderStatus(@PathParam("order_ids") String orderIds, @PathParam("status") String status,
                                      @SessionUser User user){

        try{

            List<Long>orderIdList = new ArrayList<Long>();
            List<String> orderIdStringList = Arrays.asList(orderIds.split(","));
            for(String id : orderIdStringList){
                orderIdList.add(Long.parseLong(id));
            }
            orderService.applyOrderStatusUpdate(orderIdList,status);
            return Response.status(Response.Status.OK).entity(orderIdList).build();
        }catch (Exception e){
            logger.error("Error in updating orders: "+ orderIds);
            throw new FarmersMarketException(e.getMessage(), e);
        }

    }


    @GET @Path("/{order_ids}/proc_list") @Consumes(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response getProcList(@PathParam("order_ids") String orderIds, @PathParam("status") String status, @SessionUser User user){

        try{

            List<Long>orderIdList = new ArrayList<Long>();
            List<String> orderIdStringList = Arrays.asList(orderIds.split(","));
            for(String id : orderIdStringList){
                orderIdList.add(Long.parseLong(id));
            }
            return Response.status(Response.Status.OK).entity(orderService.getProcList(orderIdList)).build();
        }catch (Exception e){
            logger.error("Error in updating orders: "+ orderIds);
            throw new FarmersMarketException(e.getMessage(), e);
        }

    }


    @GET @Path("/{order_ids}/freq_list") @Consumes(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response getFrequencyList(@PathParam("order_ids") String orderIds, @PathParam("status") String status, @SessionUser User user){

        try{

            List<Long>orderIdList = new ArrayList<Long>();
            List<String> orderIdStringList = Arrays.asList(orderIds.split(","));
            for(String id : orderIdStringList){
                orderIdList.add(Long.parseLong(id));
            }
            return Response.status(Response.Status.OK).entity(orderService.getFreqList(orderIdList)).build();
        }catch (Exception e){
            logger.error("Error in updating orders: "+ orderIds);
            throw new FarmersMarketException(e.getMessage(), e);
        }

    }

    @GET @Path("/{orderId}/orderitems") @UnitOfWork
    public Response getOrderItems(@SessionUser User user, @PathParam("orderId") LongParam orderId,
                                  @QueryParam("limit")IntParam limit, @QueryParam("offset") IntParam offset, @QueryParam("populate_price") boolean pricing){

        int lim = 40,oset = 0;

        if(limit != null)
            lim = limit.get();
        if(offset != null)
            oset = offset.get();


        try{
            if(!pricing) {
                return Response.status(Response.Status.OK).entity(orderItemService.getOrderItems(orderId.get(), lim, oset)).build();
            } else{
                Order order = orderService.findOrder(orderId.get());
                return Response.status(Response.Status.OK).entity(orderItemService.populatePrices(order)).build();
            }
        }catch (Exception e){
            logger.error("Error in updating order "+ orderId);
            throw new FarmersMarketException(e.getMessage(), e);
        }

    }

    @GET @PathParam("/") @UnitOfWork
    public Response getOrders(@SessionUser User user, @QueryParam("limit")IntParam limit, @QueryParam("offset") IntParam offset,
                              @QueryParam("city_id")LongParam cityId, @QueryParam("status")String status,
                              @QueryParam("fulfill_status") String fulfillStatus, @QueryParam("from_date")String fromDate,
                              @QueryParam("to_date")String toDate){

        int lim = 20,oset = 0;

        if(limit != null)
            lim = limit.get();
        if(offset != null)
            oset = offset.get();

        Map<String, Object> result = new HashMap<String, Object>();

        try{

            int count = orderService.getOrdersCount(cityId.get(),status,fulfillStatus,fromDate, toDate);
            result.put("count",count);
            if(count >0){
                List<Order> orderList = orderService.getOrders(cityId.get(),status,fulfillStatus,fromDate, toDate,lim,oset);
                result.put("orders",orderList);
            }
            return Response.status(Response.Status.OK).entity(result).build();


        }catch (Exception e){
            logger.error("Error in fetching orders");
            throw new FarmersMarketException(e.getMessage(), e);
        }

    }

}
