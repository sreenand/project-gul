package com.challur.resources;

import com.challur.annotations.SessionUser;
import com.challur.exceptions.FarmersMarketException;
import com.challur.models.Cart;
import com.challur.models.DummyObject;
import com.challur.models.User;
import com.challur.services.CartService;
import com.google.inject.Inject;
import io.dropwizard.hibernate.UnitOfWork;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by hariprasanthac on 10/08/16.
 */
@Path("/purchasecart")
@Produces(MediaType.APPLICATION_JSON)
public class CartResource {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(CartResource.class);
    private CartService cartService;

    @Inject
    public CartResource(CartService cartService){
        this.cartService = cartService;
    }

    @GET
    @Path("/getall") @Consumes(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response getAllCart(){
        try{
            return Response.status(Response.Status.OK).entity(cartService.getAllCarts()).build();
        }catch (Exception e){
            throw new FarmersMarketException(e.getMessage(), e);
        }
    }

    @POST
    @Path("/add") @Consumes(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response insertCart(@Valid Cart cart){
        try{
            return Response.status(Response.Status.OK).entity(cartService.addToCart(cart)).build();
        }catch (Exception e){
            throw new FarmersMarketException(e.getMessage(), e);
        }
    }

    @POST
    @Path("/remove") @Consumes(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response removeCart(@PathParam("id") String id, @Valid Cart cart, @SessionUser User user){
        try{
            return Response.status(Response.Status.OK).entity(cartService.removeFromCart(cart)).build();
        }catch (Exception e){
            throw new FarmersMarketException(e.getMessage(), e);
        }
    }
}
