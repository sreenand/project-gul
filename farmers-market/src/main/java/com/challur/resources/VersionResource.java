package com.challur.resources;

import com.challur.annotations.SessionUser;
import com.challur.configuration.VersionInformation;
import com.challur.models.Store;
import com.challur.models.User;
import com.google.inject.Inject;
import io.dropwizard.hibernate.UnitOfWork;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by srinandchallur on 25/05/16.
 */

@Path("/version")
@Produces(MediaType.APPLICATION_JSON)
public class VersionResource {



    private VersionInformation versionInformation;

    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(VersionResource.class);


    @Inject
    public VersionResource (VersionInformation versionInformation){
        this.versionInformation = versionInformation;
    }



    @GET @UnitOfWork
    public Response addStore(){
//        return Response.status(Response.Status.OK).entity(versionInformation).build();
        return Response.status(Response.Status.OK).build();
    }


}
