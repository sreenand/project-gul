package com.challur.resources;

import com.challur.annotations.SessionUser;
import com.challur.exceptions.FarmersMarketException;
import com.challur.models.*;
import com.challur.services.orders.PurchaseOrderService;
import com.google.inject.Inject;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.IntParam;
import io.dropwizard.jersey.params.LongParam;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hariprasanthac on 06/08/16.
 */
@Path("/purchase_orders")
@Produces(MediaType.APPLICATION_JSON)
public class PurchaseOrderResource {

    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PurchaseOrderResource.class);
    private PurchaseOrderService purchaseOrderService;

    @Inject
    public PurchaseOrderResource(PurchaseOrderService purchaseOrderService){
        this.purchaseOrderService = purchaseOrderService;
    }

    @GET
    @Path("/") @Consumes(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response getAllOpenPurchaseOrders(){
        try{
            return Response.status(Response.Status.OK).entity(purchaseOrderService.getOpenPurchaseOrders()).build();
        }catch (Exception e){
            throw new FarmersMarketException(e.getMessage(), e);
        }
    }

    @POST
    @Path("/createorder") @Consumes(MediaType.APPLICATION_JSON) @UnitOfWork
    public Response createOrder(@Valid List<Cart> cartList, @SessionUser User user){
        try {
            PurchaseOrder order = new PurchaseOrder();
            order.setUser_id(user.getId());
            order.setStatus("PLACED");
            purchaseOrderService.placeOrder(order,cartList);
            return Response.status(Response.Status.CREATED).build();
        } catch (Exception e){
            logger.error("Error in Creating Order" ,e);
            throw new FarmersMarketException(e.getMessage(), e);
        }

    }

    @GET @Path("/getAll") @UnitOfWork
    public Response getOrders(@SessionUser User user, @QueryParam("limit")IntParam limit, @QueryParam("offset") IntParam offset,
                              @QueryParam("city_id")LongParam cityId, @QueryParam("status")String status, @QueryParam("from_date")String fromDate,
                              @QueryParam("to_date")String toDate){

        int lim = 20,oset = 0;

        if(limit != null)
            lim = limit.get();
        if(offset != null)
            oset = offset.get();

        Map<String, Object> result = new HashMap<String, Object>();

        try{

            int count = purchaseOrderService.getpOrdersCount(status,fromDate, toDate);
            result.put("count",count);
            if(count >0){
                List<PurchaseOrder> orderList = purchaseOrderService.getpOrders(status,fromDate, toDate,lim,oset);
                result.put("orders",orderList);
            }
            return Response.status(Response.Status.OK).entity(result).build();


        }catch (Exception e){
            logger.error("Error in fetching orders");
            throw new FarmersMarketException(e.getMessage(), e);
        }

    }

    @PUT
    @Path("/{orderId}/update/{status}")
    @Consumes(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Response updateOrder(@SessionUser User user, @PathParam("orderId") String orderId, @PathParam("status") String status, @Valid PurchaseOrder purchaseOrder) {
        try {
            return Response.status(Response.Status.OK).entity(purchaseOrderService.updateStatus(orderId, status)).build();
        } catch (Exception e) {
            e.printStackTrace();
            throw new FarmersMarketException(e.getMessage(), e);
        }
    }

    @PUT
    @Path("/{order_id_list_string}/status/{status_to_be_updated}")
    @Consumes(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Response updateOrderStatus(@SessionUser User user, @PathParam("order_id_list_string") String order_id_list_string, @PathParam("status_to_be_updated") String status_to_be_updated) {
        try {
            return Response.status(Response.Status.OK).entity(purchaseOrderService.updateStatus(order_id_list_string, status_to_be_updated)).build();
        } catch (Exception e) {
            e.printStackTrace();
            throw new FarmersMarketException(e.getMessage(), e);
        }
    }
}
