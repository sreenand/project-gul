package com.challur.resources;

import com.challur.annotations.SessionUser;
import com.challur.exceptions.FarmersMarketException;
import com.challur.models.DummyObject;
import com.challur.models.PurchaseOrderItem;
import com.challur.models.User;
import com.challur.services.PurchaseOrderItemService;
import com.google.inject.Inject;
import io.dropwizard.hibernate.UnitOfWork;
import org.hibernate.persister.entity.Loadable;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;

/**
 * Created by hariprasanthac on 07/08/16.
 */

@Path("/purchase_order_item")
@Produces(MediaType.APPLICATION_JSON)
public class PurchaseOrderItemResource {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PurchaseOrderResource.class);
    private PurchaseOrderItemService purchaseOrderItemService;

    @Inject
    public PurchaseOrderItemResource(PurchaseOrderItemService purchaseOrderItemService) {
        this.purchaseOrderItemService = purchaseOrderItemService;
    }

    @GET
    @Path("/{orderId}/markets")
    @Consumes(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Response getMarkets(@PathParam("orderId") String orderId) {
        try {
            return Response.status(Response.Status.OK).entity(purchaseOrderItemService.getOrderItems(Long.parseLong(orderId))).build();
        } catch (Exception e) {
            e.printStackTrace();
            throw new FarmersMarketException(e.getMessage(), e);
        }
    }

    @GET
    @Path("/{orderId}/{marketId}/vendors")
    @Consumes(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Response getVendorOfMarket(@PathParam("orderId") String orderId, @PathParam("marketId") String marketId) {
        try {
            return Response.status(Response.Status.OK).entity(purchaseOrderItemService.getVendorOfMarket(Long.parseLong(orderId), Long.parseLong(marketId))).build();
        } catch (Exception e) {
            e.printStackTrace();
            throw new FarmersMarketException(e.getMessage(), e);
        }
    }

    @GET
    @Path("/{orderId}/{marketId}/{vendorId}/orderItems")
    @Consumes(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Response getOrderItems(@PathParam("orderId") String orderId, @PathParam("marketId") String marketId, @PathParam("vendorId") String vendorId) {
        try {
            return Response.status(Response.Status.OK).entity(purchaseOrderItemService.getAllOrderItems(Long.parseLong(orderId), Long.parseLong(marketId), Long.parseLong(vendorId))).build();
        } catch (Exception e) {
            e.printStackTrace();
            throw new FarmersMarketException(e.getMessage(), e);
        }
    }

    @PUT
    @Path("/{orderItemId}/{procQty}/{unitPrice}/updateOrderItem")
    @Consumes(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Response updateOrderItem(@PathParam("orderItemId") String orderItemId, @PathParam("procQty") String procQty, @PathParam("unitPrice") String unitPrice, @Valid DummyObject dummy) {
        try {
            return Response.status(Response.Status.OK).entity(purchaseOrderItemService.updateOrderItem(Long.parseLong(orderItemId), Integer.parseInt(procQty), Double.parseDouble(unitPrice))).build();
        } catch (Exception e) {
            e.printStackTrace();
            throw new FarmersMarketException(e.getMessage(), e);
        }
    }

    @GET
    @Path("/{orderId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Response getOrderDetails(@PathParam("orderId") String orderId) {
        try {
            return Response.status(Response.Status.OK).entity(purchaseOrderItemService.getPOrderItems(Long.parseLong(orderId))).build();
        } catch (Exception e) {
            e.printStackTrace();
            throw new FarmersMarketException(e.getMessage(), e);
        }
    }

    @PUT
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Response updateOrder(@SessionUser User user, @Valid ArrayList<PurchaseOrderItem> purchaseOrderItems) {
        try {
            return Response.status(Response.Status.OK).entity(purchaseOrderItemService.updateOrderItems(purchaseOrderItems)).build();
        } catch (Exception e) {
            e.printStackTrace();
            throw new FarmersMarketException(e.getMessage(), e);
        }
    }
}
