package com.challur.resources;

import com.challur.annotations.SessionUser;
import com.challur.exceptions.AppError;
import com.challur.exceptions.FarmersMarketServiceException;
import com.challur.models.Store;
import com.challur.models.User;
import com.challur.models.UserLoginRequest;
import com.challur.models.UserLoginResponse;
import com.challur.services.StoreService;
import com.challur.services.UserService;
import com.google.inject.Inject;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.IntParam;
import io.dropwizard.jersey.params.LongParam;
import org.hibernate.usertype.UserType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;

/**
 * Created by srinand.pc on 20/12/15.
 */

@Path("/users")

@Produces(MediaType.APPLICATION_JSON)
public class UserResource {

    private UserService userService;
    private StoreService storeService;
    private static final Logger logger = LoggerFactory.getLogger(UserResource.class);


    @Inject
    public UserResource(UserService userService, StoreService storeService){
        this.userService = userService;
        this.storeService = storeService;
    }


    @GET @UnitOfWork @Path("/tick")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(@SessionUser User user, @Context HttpServletRequest request){
        if(user != null){
            User persistentUser = userService.getUser(user.getId());
            if(user != null){
                request.getSession().setAttribute("user", persistentUser);
                return Response.status(Response.Status.OK).entity(new UserLoginResponse(persistentUser.getId(),persistentUser.getName(), persistentUser.getEmailId(), persistentUser.getType()
                        ,persistentUser.getPhoneNo(),persistentUser.getAddress(),persistentUser.getUserWallet())).build();
            }
        }
        return Response.status(Response.Status.UNAUTHORIZED).
                entity(new UserLoginResponse()).build();
    }


    @POST @UnitOfWork @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(@Valid UserLoginRequest loginRequest, @Context HttpServletRequest request){
        User user = userService.getUser(loginRequest.getUsername(), loginRequest.getPassword());
        if(user != null){
            request.getSession().setAttribute("user", user);
            return Response.status(Response.Status.OK).entity(new UserLoginResponse(user.getId(),user.getName(), user.getEmailId(), user.getType()
                    ,user.getPhoneNo(),user.getAddress(),user.getUserWallet())).build();
        }
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).
                entity(new UserLoginResponse()).build();
    }

    @POST @UnitOfWork @Path("/register")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createUser(User user){

        try{
            user.setType(User.UserType.SALES.toString());
            user.setStatus(User.UserStatus.INACTIVE.toString());
            userService.createUser(user);
            if(user != null){
                return Response.status(Response.Status.CREATED).build();
            }

        } catch (Exception e){
            logger.error("Unable to Create the User {}",user.getEmailId(), e);
        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }


    @PUT @UnitOfWork @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateUser(@Valid User user, @SessionUser User sessionUser){

        try{

            if(!sessionUser.isAdmin()){
                return Response.status(Response.Status.FORBIDDEN).build();
            }

            User persistentUser = userService.getUser(user.getId());
            userService.updateUser(user,persistentUser);

            return Response.status(Response.Status.OK).build();

        } catch (Exception e){
            logger.error("Unable to Create the User {}",user.getEmailId(), e);
        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }

    @POST @UnitOfWork @Path("/logout")
    public Response logOut(User user,  @Context HttpServletRequest request){

        try{
            request.getSession().removeAttribute("user");
        } catch (Exception e){
            logger.error("User {} Already Logged out",user.getEmailId(), e);
        }
        return Response.status(Response.Status.OK).build();
    }


    @GET @UnitOfWork @Path("/find/{mobile_no}")
    public Response findUser(@Context HttpServletRequest request, @SessionUser User user, @PathParam("mobile_no") String mobile){

        if(!user.getType().equals(User.UserType.ADMIN.toString())){
            throw new WebApplicationException(Response.Status.UNAUTHORIZED);
        }
        return Response.status(Response.Status.OK).entity(userService.getUser(mobile)).build();
    }


    @GET @UnitOfWork @Path("/findbyid/{id}")
    public Response findUser(@Context HttpServletRequest request, @SessionUser User user, @PathParam("id") Long userId){

//        if(user.isAdmin() || user.isSalesPerson()){
//            throw new WebApplicationException(Response.Status.UNAUTHORIZED);
//        }
        return Response.status(Response.Status.OK).entity(userService.getUser(userId)).build();
    }

    @GET @UnitOfWork @Path("/get_sales_users")
    public Response findUser(@SessionUser User user){
        return Response.status(Response.Status.OK).entity(userService.getSalesUser()).build();
    }

    @PUT @UnitOfWork @Path("/update/{email}") @Consumes(MediaType.APPLICATION_JSON)
    public Response updateUser(@Context HttpServletRequest request, @SessionUser User user,
                               @PathParam("email") String emailId, @Valid User updatableUser){

        if(!user.getType().equals(User.UserType.ADMIN.toString())){
            throw new WebApplicationException(Response.Status.UNAUTHORIZED);
        }
        User persistentUser = userService.getUser(emailId);
        userService.updateUser(updatableUser, persistentUser);
        return Response.status(Response.Status.OK).entity(persistentUser).build();

    }


    @PUT @UnitOfWork @Path("/password/reset/{id}") @Consumes(MediaType.APPLICATION_JSON)
    public Response resetPassword(@Context HttpServletRequest request, @SessionUser User user,
                                  @PathParam("id") LongParam id, HashMap parameters) {
        String password = (String)parameters.get("password");
        userService.resetPassword(user,id.get(),password);

        if(user.getId().equals(id.get())){
            try{
                request.getSession().removeAttribute("user");
            } catch (Exception e){
                logger.error("User {} Already Logged out",user.getEmailId(), e);
            }
        }
        return Response.status(Response.Status.OK).build();
    }


    @PUT @UnitOfWork @Path("/wallet/addmoney/store/{store_id}") @Consumes(MediaType.APPLICATION_JSON)
    public Response addMoneyToStore(@Context HttpServletRequest request, @SessionUser User user,
                                  @PathParam("store_id") LongParam storeId, HashMap parameters) {


        logger.info("Add money request from {}", user.getId());
        if(!user.isAdmin() && !user.isSalesPerson()){
            return Response.status(Response.Status.FORBIDDEN).build();
        }


        try{
            Store store = storeService.getStoreById(storeId.get());
            User donor = userService.getUser(user.getId());

            if(donor.getUserWallet().getCurrentBalance() <= donor.getUserWallet().getLowerLimit()){
                logger.info("Wallet Balance Too low to recharge for the donor {} ", user.getPhoneNo());
                return Response.status(Response.Status.FORBIDDEN).build();
            }
            User storeUser = userService.getUser(store.getUserId());
            Double amountTobeAdded = Double.parseDouble((String)parameters.get("amount").toString());
            logger.info("Add money request from {} to {}", user.getName(), storeUser.getName());
            if(user.getUserWallet().getCurrentBalance() + amountTobeAdded < user.getUserWallet().getLowerLimit()){
                return Response.status(Response.Status.NOT_ACCEPTABLE).build();
            }
            userService.collectCashAndAddMoney(donor,storeUser,amountTobeAdded,store.getId());
            logger.info("Added money successfully to user {}, user Id: ", storeUser.getName(), storeUser.getId());
        } catch (Exception e){
            logger.error("Unable to add money to wallet", e);
            throw new FarmersMarketServiceException(500, AppError.INTERNAL_ERROR,e.getMessage());
        }

        return Response.status(Response.Status.OK).build();
    }

    @PUT @UnitOfWork @Path("/{user_id}/wallet/remit") @Consumes(MediaType.APPLICATION_JSON)
    public Response addRemitMoneyToSystem(@Context HttpServletRequest request,
                                          @SessionUser User user,@PathParam("user_id") LongParam userId,
                                          @QueryParam("city_id") LongParam cityId) {

        logger.info("recon request from {}, user Id: {} received", user.getName(), user.getId());

        if(!user.isAdmin()){
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        try{
            User donor = userService.getUser(userId.get());
            userService.reconcileCash(donor,cityId.get());

            logger.info("recon request from {}, user Id: {} completed", user.getName(), user.getId());
        } catch (Exception e){
            logger.error("Unable to add money to wallet", e);
            throw new FarmersMarketServiceException(500, AppError.INTERNAL_ERROR,e.getMessage());
        }

        return Response.status(Response.Status.OK).build();
    }

}
