package com.challur.configuration;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by srinand.pc on 15/12/15.
 */
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class FarmersMarketConfiguration extends Configuration {


    @Valid @NotNull @JsonProperty("database")
    private DataSourceFactory database = new DataSourceFactory();

    private SystemUsersConfiguration sysUserConfig;

    private VersionInformation version;

    public DataSourceFactory getDataSourceFactory() {
        return database;
    }






}
