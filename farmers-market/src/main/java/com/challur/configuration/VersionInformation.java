package com.challur.configuration;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by srinandchallur on 25/05/16.
 */

@Getter
@Setter
@NoArgsConstructor
public class VersionInformation {

    private int currentVersion;

    private List<Integer> supportedVersions;

}
