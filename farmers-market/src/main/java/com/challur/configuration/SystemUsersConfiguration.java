package com.challur.configuration;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

/**
 * Created by srinandchallur on 11/05/16.
 */

@Getter @Setter @NoArgsConstructor
public class SystemUsersConfiguration {

    private Map<Long, Long> systemUsers;

}
