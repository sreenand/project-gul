package com.challur;

import com.challur.configuration.FarmersMarketConfiguration;
import com.challur.repository.*;
import com.challur.repository.interfaces.*;
import com.challur.repository.interfaces.UserRepository;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import io.dropwizard.hibernate.HibernateBundle;


/**
 * Created by srinand.pc on 15/12/15.
 */
public class HibernateModule extends AbstractModule {

    private HibernateBundle<FarmersMarketConfiguration> hibernateBundle;


    public HibernateModule(HibernateBundle<FarmersMarketConfiguration> hibernate) {
        this.hibernateBundle = hibernate;
    }

    @Override
    protected void configure() {

    }

    @Provides
    public OrderItemRepository provideDemandRepo(){
        return new OrderItemDAO(hibernateBundle.getSessionFactory());
    }

    @Provides
    public WastageRepository provideWastageRepo(){
        return new WastageDAO(hibernateBundle.getSessionFactory());
    }

    @Provides
    public UserRepository provideUserRepo(){
        return new UserDAO(hibernateBundle.getSessionFactory());
    }

    @Provides
    public StoreRepository provideStoreRepo(){
        return new StoreDAO(hibernateBundle.getSessionFactory());
    }

    @Provides
    public ProductRepository provideSkuRepo(){
        return new ProductDAO(hibernateBundle.getSessionFactory());
    }

    @Provides
    public OrderRepository provideOrderRepo(){
        return new OrderDAO(hibernateBundle.getSessionFactory());
    }

    @Provides
    public ProductPricingRepo providePricingRepo(){
        return new ProductPricingDAO(hibernateBundle.getSessionFactory());
    }

    @Provides
    public VariablePricingRepository variablePricingRepo(){
        return new VariablePricingDAO(hibernateBundle.getSessionFactory());
    }

    @Provides
    public LocalityRepository provideLocalityRepo(){
        return new LocalityDAO(hibernateBundle.getSessionFactory());
    }

    @Provides
    public CategoryRepository provideCategoryRepo(){
        return new CategoryDAO(hibernateBundle.getSessionFactory());
    }

    @Provides
    public SubCategoryRepository provideSubCategoryRepo(){
        return new SubCategoryDAO(hibernateBundle.getSessionFactory());
    }

    @Provides
    public TransactionRepository provideTransactionRepo(){
        return new TransactionDAO(hibernateBundle.getSessionFactory());
    }

    @Provides
    public PurchaseOrderRepository providePurchaseOrderRepo(){
        return new PurchaseOrderDAO(hibernateBundle.getSessionFactory());
    }

    @Provides
    public PurchaseOrderItemRepository providePurchaseOrderItemRepo(){
        return new PurchaseOrderItemDAO(hibernateBundle.getSessionFactory());
    }

    @Provides
    public SourcingPointRepository sourcingPointRepository(){
        return new SourcingPointDAO(hibernateBundle.getSessionFactory());
    }

    @Provides
    public VendorRepository vendorRepository(){
        return new VendorDAO(hibernateBundle.getSessionFactory());
    }

    @Provides
    public CartRepository cartRepository(){
        return new CartDAO(hibernateBundle.getSessionFactory());
    }

    @Provides
    public VendorTransactionRepository vendorTransactionRepository(){
        return new VendorTransactionDAO(hibernateBundle.getSessionFactory());
    }
}
