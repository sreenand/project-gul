package com.challur;

import com.challur.configuration.FarmersMarketConfiguration;
import com.challur.configuration.SystemUsersConfiguration;
import com.challur.configuration.VersionInformation;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import io.dropwizard.jetty.ConnectorFactory;
import io.dropwizard.jetty.HttpConnectorFactory;
import io.dropwizard.server.DefaultServerFactory;

import java.util.Map;

/**
 * Created by srinand.pc on 15/12/15.
 */
public class FarmersMarketModule extends AbstractModule {


    @Override
    protected void configure() {

    }


    @Provides @Singleton
    public ObjectMapper provideMapper() {
        return new ObjectMapper();
    }


    @Provides
    public HttpConnectorFactory provideServerConfig(FarmersMarketConfiguration configuration)  {
        HttpConnectorFactory httpConnector = null;
        DefaultServerFactory serverFactory = (DefaultServerFactory) configuration.getServerFactory();
        for (ConnectorFactory connector : serverFactory.getApplicationConnectors()) {
            if (connector.getClass().isAssignableFrom(HttpConnectorFactory.class)) {
                httpConnector = (HttpConnectorFactory) connector;
            }
        }
        return httpConnector;
    }

    @Provides
    public SystemUsersConfiguration getSystemUsersConfiguration(FarmersMarketConfiguration configuration){
        return configuration.getSysUserConfig();
    }


    @Provides
    public VersionInformation getVersionInformation(FarmersMarketConfiguration configuration){
        return configuration.getVersion();
    }
}
