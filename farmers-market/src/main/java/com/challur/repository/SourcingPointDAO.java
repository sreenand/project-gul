package com.challur.repository;

import com.challur.models.SourcingPoint;
import com.challur.repository.interfaces.SourcingPointRepository;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;

import java.util.List;
import java.util.Map;

/**
 * Created by hariprasanthac on 10/08/16.
 */
public class SourcingPointDAO extends AbstractDAO<SourcingPoint> implements SourcingPointRepository{

    public SourcingPointDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public SourcingPoint create(SourcingPoint sourcingPoint) {
        currentSession().persist(sourcingPoint);
        return sourcingPoint;
    }

    public SourcingPoint update(SourcingPoint sourcingPoint) {
        currentSession().persist(sourcingPoint);
        return sourcingPoint;
    }

    public void update(String query, Map<String, Object> parameters) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(SourcingPoint.class);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){
            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(),(List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        sqlQuery.executeUpdate();
    }

    public SourcingPoint find(long vendorId) {
        return uniqueResult(currentSession().createCriteria(SourcingPoint.class).
                add(Restrictions.eq("sourcing_mkt_id", vendorId)));
    }

    public List<SourcingPoint> find(String query) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(SourcingPoint.class);
        return sqlQuery.list();
    }

    public List<SourcingPoint> find(String query, Map<String, Object> parameters) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(SourcingPoint.class);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){

            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(),(List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return sqlQuery.list();
    }

    public int count(List<SimpleExpression> givenCriteria) {
        return 0;
    }

    public int count(String query, Map<String, Object> parameters) {
        return 0;
    }

    public List<SourcingPoint> find(String query, Map<String, Object> parameters, int limit, int offset) {
        return null;
    }
}
