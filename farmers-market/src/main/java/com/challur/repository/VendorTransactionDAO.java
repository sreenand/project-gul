package com.challur.repository;

import com.challur.models.VendorTransaction;
import com.challur.repository.interfaces.VendorTransactionRepository;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;

import java.util.List;
import java.util.Map;

/**
 * Created by hariprasanthac on 11/08/16.
 */
public class VendorTransactionDAO  extends AbstractDAO<VendorTransaction> implements VendorTransactionRepository {

    public VendorTransactionDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public VendorTransaction create(VendorTransaction vendorTransaction) {
        currentSession().persist(vendorTransaction);
        return vendorTransaction;
    }

    public void create(String query, Map<String, Object> parameters) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(VendorTransaction.class);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){
            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(),(List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        sqlQuery.executeUpdate();
    }

    public VendorTransaction update(VendorTransaction vendorTransaction) {
        currentSession().persist(vendorTransaction);
        return vendorTransaction;
    }

    public void update(String query, Map<String, Object> parameters) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(VendorTransaction.class);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){
            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(),(List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        sqlQuery.executeUpdate();
    }

    public VendorTransaction find(long vendorId) {
        return uniqueResult(currentSession().createCriteria(VendorTransaction.class).
                add(Restrictions.eq("id", vendorId)));
    }

    public List<VendorTransaction> find(String query) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(VendorTransaction.class);
        return sqlQuery.list();
    }

    public List<VendorTransaction> find(String query, Map<String, Object> parameters) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(VendorTransaction.class);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){

            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(),(List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return sqlQuery.list();
    }

    public VendorTransaction find(long vendorId, long orderId) {
        return uniqueResult(currentSession().createCriteria(VendorTransaction.class).
                add(Restrictions.eq("vendor_id", vendorId)).add(Restrictions.eq("po_id",orderId)));
    }

    public int count(List<SimpleExpression> givenCriteria) {
        return 0;
    }

    public int count(String query, Map<String, Object> parameters) {
        return 0;
    }

    public List<VendorTransaction> find(String query, Map<String, Object> parameters, int limit, int offset) {
        return null;
    }
}
