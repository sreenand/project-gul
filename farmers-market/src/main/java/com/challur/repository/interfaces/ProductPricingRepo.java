package com.challur.repository.interfaces;

import com.challur.models.ProductPricing;
import org.hibernate.criterion.SimpleExpression;

import java.util.List;
import java.util.Map;

/**
 * Created by srinandchallur on 23/03/16.
 */
public interface ProductPricingRepo {

    public ProductPricing create(ProductPricing productPricing);

    public ProductPricing update(ProductPricing productPricing);

    public List<ProductPricing> find(List<SimpleExpression> criteria);

    public List<ProductPricing> find(String sqlQuery, Map<String, Object> parameters, int limit, int offset);

    public List<ProductPricing> fetchVaribalePricing(String sqlQuery, Map<String, Object> parameters, int limit, int offset);

    public void delete(ProductPricing productPricing);
}
