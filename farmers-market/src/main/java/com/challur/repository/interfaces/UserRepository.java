package com.challur.repository.interfaces;

import com.challur.models.User;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by srinandchallur on 23/03/16.
 */
public interface UserRepository {

    public void create(User user);

    public void update(User user);

    public void delete(User user);

    public User getUser(String userName, String password);

    public User getUser(String userName);

    public User getUser(Long userId);

    public List<User> find(String sqlQuery);
}
