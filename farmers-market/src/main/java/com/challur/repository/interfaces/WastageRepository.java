package com.challur.repository.interfaces;

import com.challur.models.Wastage;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by srinandchallur on 23/03/16.
 */
public interface WastageRepository {

    public void create(Wastage wastage);

    public List<Wastage> getWastage(Long storeId);

    public void update(Wastage wastage);

    public Wastage delete(int id);
}
