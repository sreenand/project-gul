package com.challur.repository.interfaces;

import com.challur.models.Order;
import com.challur.models.OrderPayment;
import org.hibernate.criterion.SimpleExpression;

import java.util.List;
import java.util.Map;

/**
 * Created by srinandchallur on 23/03/16.
 */
public interface OrderRepository {

    public Order create(Order order);

    public Order update(Order order);

    public void update(String query,Map<String,Object> parameters);

    public Order find(long orderId);

    public List<Order> find(List<SimpleExpression> givenCriteria, int limit, int offset);

    public List<Order> find(String query,Map<String,Object> parameters);

    public List<OrderPayment> findOrderPayments(String query, Map<String,Object> parameters);

    public int count(List<SimpleExpression> givenCriteria);

    public int count(String query, Map<String,Object> parameters);

    public List<Order> find(String query, Map<String,Object> parameters, int limit, int offset);
}
