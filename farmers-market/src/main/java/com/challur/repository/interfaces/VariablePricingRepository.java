package com.challur.repository.interfaces;

import com.challur.models.VariablePricing;
import org.hibernate.criterion.SimpleExpression;

import java.util.List;
import java.util.Map;

/**
 * Created by hariprasanthac on 19/07/16.
 */
public interface VariablePricingRepository {
    public VariablePricing create(VariablePricing variablePricing);

    public VariablePricing update(VariablePricing variablePricing);

    public List<VariablePricing> find(List<SimpleExpression> criteria);

    public List<VariablePricing> find(String sqlQuery, Map<String,Object> parameters);

    public void delete(VariablePricing variablePricing);
}
