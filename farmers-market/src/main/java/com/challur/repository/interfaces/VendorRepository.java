package com.challur.repository.interfaces;

import com.challur.models.Vendor;
import org.hibernate.criterion.SimpleExpression;

import java.util.List;
import java.util.Map;

/**
 * Created by hariprasanthac on 10/08/16.
 */
public interface VendorRepository {
    public Vendor create(Vendor purchaseOrder);

    public Vendor update(Vendor purchaseOrder);

    public void update(String query, Map<String, Object> parameters);

    public Vendor find(long vendorId);

    public List<Vendor> find(String query);

    public List<Vendor> find(String query, Map<String, Object> parameters);

    public int count(List<SimpleExpression> givenCriteria);

    public int count(String query, Map<String, Object> parameters);

    public List<Vendor> find(String query, Map<String, Object> parameters, int limit, int offset);
}
