package com.challur.repository.interfaces;

import com.challur.models.PurchaseOrder;
import org.hibernate.criterion.SimpleExpression;

import java.util.List;
import java.util.Map;

/**
 * Created by hariprasanthac on 06/08/16.
 */
public interface PurchaseOrderRepository {
    public PurchaseOrder create(PurchaseOrder purchaseOrder);

    public PurchaseOrder update(PurchaseOrder purchaseOrder);

    public void update(String query, Map<String, Object> parameters);

    public PurchaseOrder find(long purchaseOrderId);

    public List<PurchaseOrder> find(String query);

    public List<PurchaseOrder> find(String query, Map<String, Object> parameters);

    public int count(List<SimpleExpression> givenCriteria);

    public int count(String query, Map<String, Object> parameters);

    public List<PurchaseOrder> find(String query, Map<String, Object> parameters, int limit, int offset);
}
