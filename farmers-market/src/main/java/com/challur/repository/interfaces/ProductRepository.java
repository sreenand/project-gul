package com.challur.repository.interfaces;

import com.challur.models.Product;
import org.hibernate.criterion.SimpleExpression;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by srinandchallur on 23/03/16.
 */
public interface ProductRepository {

    public Product create(Product stockKeepingUnit);

    public List<Product> getProducts(String sql, Map<String,Object> ParameterList);

    public void update(Product stockKeepingUnit);

    public Product delete(int id);

    public List<Product> getProducts(List<Long> idList);
}
