package com.challur.repository.interfaces;

import com.challur.models.Cart;
import org.hibernate.criterion.SimpleExpression;

import java.util.List;
import java.util.Map;

/**
 * Created by hariprasanthac on 10/08/16.
 */
public interface CartRepository {
    public Cart create(Cart cart);

    public Cart create(String query, Map<String, Object> parameters);

    public Cart update(Cart cart);

    public void update(String query, Map<String, Object> parameters);

    public Cart find(long purchaseOrderId);

    public List<Cart> find(String query);

    public List<Cart> find(String query, Map<String, Object> parameters);

    public Cart delete(Cart cart);

    public void delete(String query);

    public void delete(String query, Map<String, Object> parameters);

    public int count(List<SimpleExpression> givenCriteria);

    public int count(String query, Map<String, Object> parameters);

    public List<Cart> find(String query, Map<String, Object> parameters, int limit, int offset);
}
