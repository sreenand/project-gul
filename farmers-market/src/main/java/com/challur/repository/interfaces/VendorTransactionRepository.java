package com.challur.repository.interfaces;

import com.challur.models.VendorTransaction;
import org.hibernate.criterion.SimpleExpression;

import java.util.List;
import java.util.Map;

/**
 * Created by hariprasanthac on 11/08/16.
 */
public interface VendorTransactionRepository {
    public VendorTransaction create(VendorTransaction vendorTransaction);

    public void create(String query, Map<String, Object> parameters);

    public VendorTransaction update(VendorTransaction vendorTransaction);

    public void update(String query, Map<String, Object> parameters);

    public VendorTransaction find(long vendorId);

    public VendorTransaction find(long vendorId, long orderId);

    public List<VendorTransaction> find(String query);

    public List<VendorTransaction> find(String query, Map<String, Object> parameters);

    public int count(List<SimpleExpression> givenCriteria);

    public int count(String query, Map<String, Object> parameters);

    public List<VendorTransaction> find(String query, Map<String, Object> parameters, int limit, int offset);
}
