package com.challur.repository.interfaces;

import com.challur.models.Category;
import org.hibernate.criterion.SimpleExpression;

import java.util.List;

/**
 * Created by srinandchallur on 12/04/16.
 */
public interface CategoryRepository {

    public Category create(Category category);

    public Category update(Category category);

    public List<Category> find(List<SimpleExpression> criteria);

}
