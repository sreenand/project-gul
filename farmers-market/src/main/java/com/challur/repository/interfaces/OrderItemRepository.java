package com.challur.repository.interfaces;

import com.challur.models.OrderItem;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.Map;

/**
 * Created by srinandchallur on 23/03/16.
 */
public interface OrderItemRepository {


    public void create(OrderItem orderItem);

    public void create(List<OrderItem> orderItems);

    public List<OrderItem> getOrderItems(Long orderId, int limit, int offset);

    public List<OrderItem> getOrderItems(String sqlQuery, Map<String,Object> parameters);

    public OrderItem getOrderItem(Long orderId);

    public OrderItem update(OrderItem orderItem);

    public OrderItem delete(int id);
}
