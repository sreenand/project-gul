package com.challur.repository.interfaces;

import com.challur.models.Transaction;
import com.challur.models.TransactionItem;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by srinandchallur on 09/05/16.
 */
public interface TransactionRepository {

    public Transaction create(Transaction transaction);

    public List<Transaction> find(String sqlQuery, Map<String,Object> parameters);

    public List<TransactionItem> findTransactionItems(String sqlQuery, Map<String,Object> parameters);

    public Transaction update(Transaction transaction);

}
