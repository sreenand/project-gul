package com.challur.repository.interfaces;

import com.challur.models.SourcingPoint;
import org.hibernate.criterion.SimpleExpression;

import java.util.List;
import java.util.Map;

/**
 * Created by hariprasanthac on 10/08/16.
 */
public interface SourcingPointRepository {
    public SourcingPoint create(SourcingPoint purchaseOrder);

    public SourcingPoint update(SourcingPoint purchaseOrder);

    public void update(String query, Map<String, Object> parameters);

    public SourcingPoint find(long vendorId);

    public List<SourcingPoint> find(String query);

    public List<SourcingPoint> find(String query, Map<String, Object> parameters);

    public int count(List<SimpleExpression> givenCriteria);

    public int count(String query, Map<String, Object> parameters);

    public List<SourcingPoint> find(String query, Map<String, Object> parameters, int limit, int offset);
}
