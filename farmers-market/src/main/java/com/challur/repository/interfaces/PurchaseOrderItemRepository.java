package com.challur.repository.interfaces;

import com.challur.models.PurchaseOrder;
import com.challur.models.PurchaseOrderItem;
import org.hibernate.criterion.SimpleExpression;

import java.util.List;
import java.util.Map;

/**
 * Created by hariprasanthac on 07/08/16.
 */
public interface PurchaseOrderItemRepository {
    public PurchaseOrderItem create(PurchaseOrderItem purchaseOrderItem);

    public PurchaseOrderItem update(PurchaseOrderItem purchaseOrderItem);

    public void update(String query, Map<String, Object> parameters);

    public PurchaseOrderItem find(long purchaseOrderId);

    public List<PurchaseOrderItem> find(String query);

    public List<PurchaseOrderItem> find(String query, Map<String, Object> parameters);

    public int count(List<SimpleExpression> givenCriteria);

    public int count(String query, Map<String, Object> parameters);

    public List<PurchaseOrderItem> find(String query, Map<String, Object> parameters, int limit, int offset);
}
