package com.challur.repository.interfaces;

import com.challur.models.SubCategory;
import org.hibernate.criterion.SimpleExpression;

import java.util.List;

/**
 * Created by srinandchallur on 12/04/16.
 */
public interface SubCategoryRepository {


    public SubCategory create(SubCategory subCategory);

    public SubCategory update(SubCategory subCategory);

    public List<SubCategory> find(List<SimpleExpression> criteria);
}
