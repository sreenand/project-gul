package com.challur.repository.interfaces;

import com.challur.models.Store;
import com.google.common.base.Strings;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.hibernate.type.StringType;

import java.util.List;
import java.util.Map;

/**
 * Created by srinandchallur on 23/03/16.
 */
public interface StoreRepository {

    public Store create(Store store);

    public List<Store> getStores(String sqlQuery, Map<String,Object> parameters);

    public Store update(Store store);

    public Store delete(int id);

    public List<Store> getStores(List<SimpleExpression> cirteria, int limit, int offset);
}
