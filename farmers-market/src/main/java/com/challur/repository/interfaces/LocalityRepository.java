package com.challur.repository.interfaces;

import com.challur.models.Locality;
import org.hibernate.criterion.SimpleExpression;

import java.util.List;

/**
 * Created by srinandchallur on 23/03/16.
 */
public interface LocalityRepository {


    public Locality create(Locality locality);

    public Locality update(Locality locality);

    public List<Locality> find(List<SimpleExpression> criteria);

    public void delete(Locality locality);
}
