package com.challur.repository;

import com.challur.models.Order;
import com.challur.models.SubCategory;
import com.challur.repository.interfaces.SubCategoryRepository;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.SimpleExpression;

import java.util.List;

/**
 * Created by srinandchallur on 12/04/16.
 */
public class SubCategoryDAO extends AbstractDAO<SubCategory> implements SubCategoryRepository {
    /**
     * Creates a new DAO with a given session provider.
     *
     * @param sessionFactory a session provider
     */
    public SubCategoryDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public SubCategory create(SubCategory subCategory) {
        currentSession().persist(subCategory);
        return subCategory;
    }

    public SubCategory update(SubCategory subCategory) {
        currentSession().persist(subCategory);
        return subCategory;
    }

    public List<SubCategory> find(List<SimpleExpression> givenCriteria) {
        Criteria criteria = currentSession().createCriteria(SubCategory.class);
        for(SimpleExpression expression : givenCriteria){
            criteria.add(expression);
        }
        return criteria.list();
    }
}
