package com.challur.repository;

import com.challur.models.PurchaseOrder;
import com.challur.models.PurchaseOrderItem;
import com.challur.repository.interfaces.PurchaseOrderItemRepository;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.SimpleExpression;

import java.util.List;
import java.util.Map;

/**
 * Created by hariprasanthac on 07/08/16.
 */
public class PurchaseOrderItemDAO extends AbstractDAO<PurchaseOrder> implements PurchaseOrderItemRepository {

    public PurchaseOrderItemDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public PurchaseOrderItem create(PurchaseOrderItem purchaseOrderItem) {
        currentSession().persist(purchaseOrderItem);
        return purchaseOrderItem;
    }

    public PurchaseOrderItem update(PurchaseOrderItem purchaseOrderItem) {
        currentSession().persist(purchaseOrderItem);
        return purchaseOrderItem;
    }

    public void update(String query, Map<String, Object> parameters) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(PurchaseOrderItem.class);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){
            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(),(List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        sqlQuery.executeUpdate();
    }

    public PurchaseOrderItem find(long purchaseOrderId) {
        return null;
    }

    public List<PurchaseOrderItem> find(String query) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(PurchaseOrderItem.class);
        return sqlQuery.list();
    }

    public List<PurchaseOrderItem> find(String query, Map<String, Object> parameters) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(PurchaseOrderItem.class);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){

            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(),(List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return sqlQuery.list();
    }

    public int count(List<SimpleExpression> givenCriteria) {
        return 0;
    }

    public int count(String query, Map<String, Object> parameters) {
        return 0;
    }

    public List<PurchaseOrderItem> find(String query, Map<String, Object> parameters, int limit, int offset) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(PurchaseOrderItem.class);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){

            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(), (List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        sqlQuery.setFirstResult(offset);
        if(limit > 0){
            sqlQuery.setMaxResults(limit);
        }
        return sqlQuery.list();
    }
}
