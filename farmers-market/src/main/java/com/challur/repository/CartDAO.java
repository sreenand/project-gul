package com.challur.repository;

import com.challur.models.Cart;
import com.challur.repository.interfaces.CartRepository;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.SimpleExpression;

import java.util.List;
import java.util.Map;

/**
 * Created by hariprasanthac on 10/08/16.
 */
public class CartDAO extends AbstractDAO<Cart> implements CartRepository {
    public CartDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Cart create(Cart cart) {
        currentSession().persist(cart);
        return cart;
    }

    public Cart create(String query, Map<String, Object> parameters) {
        return null;
    }

    public Cart update(Cart cart) {
        currentSession().persist(cart);
        return cart;
    }

    public void update(String query, Map<String, Object> parameters) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(Cart.class);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){
            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(),(List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        sqlQuery.executeUpdate();
    }

    public Cart find(long cartId) {
        return null;
    }

    public List<Cart> find(String query) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(Cart.class);
        return sqlQuery.list();
    }

    public List<Cart> find(String query, Map<String, Object> parameters) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(Cart.class);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){

            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(),(List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return sqlQuery.list();
    }

    public Cart delete(Cart cart) {
        currentSession().delete(cart);
        return cart;
    }

    public void delete(String query) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.executeUpdate();
    }

    public void delete(String query, Map<String, Object> parameters) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(Cart.class);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){

            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(),(List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        sqlQuery.executeUpdate();
    }

    public int count(List<SimpleExpression> givenCriteria) {
        return 0;
    }

    public int count(String query, Map<String, Object> parameters) {
        return 0;
    }

    public List<Cart> find(String query, Map<String, Object> parameters, int limit, int offset) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(Cart.class);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){

            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(), (List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        sqlQuery.setFirstResult(offset);
        if(limit > 0){
            sqlQuery.setMaxResults(limit);
        }
        return sqlQuery.list();
    }
}
