package com.challur.repository;

import com.challur.models.Order;
import com.challur.models.Product;
import com.challur.repository.interfaces.ProductRepository;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.SimpleExpression;

import java.util.List;
import java.util.Map;

/**
 * Created by srinandchallur on 23/03/16.
 */
public class ProductDAO extends AbstractDAO<Product> implements ProductRepository {
    /**
     * Creates a new DAO with a given session provider.
     *
     * @param sessionFactory a session provider
     */
    public ProductDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Product create(Product stockKeepingUnit) {
        currentSession().persist(stockKeepingUnit);
        return stockKeepingUnit;
    }


    public void create(List<Product> products) {

        for(Product sku : products){
            currentSession().persist(sku);
        }

    }

    public List<Product>  getProducts(List<Long> idList) {

        Query query = currentSession().createQuery("FROM Product item WHERE id IN (:items)");
        query.setParameterList("items", idList);
        return query.list();
    }

    public List<Product> getProducts(String sql, Map<String,Object> parameterList) {
        SQLQuery query = currentSession().createSQLQuery(sql);
        query.addEntity(Product.class);
        for(Map.Entry<String, Object> entry : parameterList.entrySet()){
            query.setParameter(entry.getKey(),entry.getValue());
        }
        return query.list();
    }

    public void update(Product stockKeepingUnit) {

    }

    public Product delete(int id) {
        return null;
    }
}

