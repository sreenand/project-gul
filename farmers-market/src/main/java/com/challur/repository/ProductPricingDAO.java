package com.challur.repository;

import com.challur.models.*;
import com.challur.repository.interfaces.ProductPricingRepo;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.SimpleExpression;
import org.hibernate.type.descriptor.sql.LongNVarcharTypeDescriptor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by srinandchallur on 23/03/16.
 */
public class ProductPricingDAO extends AbstractDAO<ProductPricing> implements ProductPricingRepo {

    public ProductPricingDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public ProductPricing create(ProductPricing productPricing) {
        persist(productPricing);
        return productPricing;
    }

    public ProductPricing update(ProductPricing productPricing) {
        persist(productPricing);
        return productPricing;
    }

    public List<ProductPricing> find(List<SimpleExpression> givenCriteria) {
        Criteria criteria = currentSession().createCriteria(ProductPricing.class);
        for (SimpleExpression expression : givenCriteria) {
            criteria.add(expression);
        }
        return criteria.list();
    }

    public List<ProductPricing> find(String sqlQuery, Map<String, Object> parameters, int limit, int offset) {
        SQLQuery query = currentSession().createSQLQuery(sqlQuery);
        query.addEntity(ProductPricing.class);

        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            if (entry.getValue() instanceof List) {
                query.setParameterList(entry.getKey(), (List) entry.getValue());
            } else {
                query.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return query.list();
    }

    public List<ProductPricing> fetchVaribalePricing(String sqlQuery, Map<String, Object> parameters, int limit, int offset) {
        HashMap<Long, ProductPricing> productPricingMap = new HashMap<Long, ProductPricing>();

        SQLQuery query = currentSession().createSQLQuery(sqlQuery);
        try {

            for (Map.Entry<String, Object> entry : parameters.entrySet()) {
                if (entry.getValue() instanceof List) {
                    query.setParameterList(entry.getKey(), (List) entry.getValue());
                } else {
                    query.setParameter(entry.getKey(), entry.getValue());
                }
            }


            List<Object[]> result = query.list();
            for (Object[] row : result) {
                ProductPricing productPricing = new ProductPricing();
                long id = Long.valueOf(row[0].toString());
                if (productPricingMap.containsKey(id)) {
                    productPricing = productPricingMap.get(id);
                    productPricing.getVariablePricings().add(parseVariablePricing(row));
                    continue;
                }
                productPricing.setId(id);
                productPricing.setProductId(Long.valueOf(row[1].toString()));
                productPricing.setLocalityId(row[2] != null ? Long.valueOf(row[2].toString()):null);
                productPricing.setCityId(Long.valueOf(row[3].toString()));
                productPricing.setUnitId(Long.valueOf(row[4].toString()));
                productPricing.setPricePerUnit(Double.valueOf(row[5].toString()));
                productPricing.setMinQuantity(Double.valueOf(row[6].toString()));
                productPricing.setMaxQuantity(Double.valueOf(row[7].toString()));
                productPricing.setQuantityDescription(row[8].toString());
                productPricing.setStatus(row[9].toString());
                productPricing.setVariablePricings(new ArrayList<VariablePricing>());
                productPricing.getVariablePricings().add(parseVariablePricing(row));
                productPricing.setProduct(parseProduct(row));
                productPricing.setProductUnit(parseProductUnit(row));
                productPricingMap.put(id,productPricing);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return new ArrayList<ProductPricing>(productPricingMap.values());
    }

    private VariablePricing parseVariablePricing(Object[] row){
        VariablePricing varPricing = new VariablePricing();
        varPricing.setProductPricingId(Long.valueOf(row[10].toString()));
        varPricing.setBucketId(Integer.valueOf(row[11].toString()));
        varPricing.setMin(Double.valueOf(row[12].toString()));
        varPricing.setMax(Double.valueOf(row[13].toString()));
        varPricing.setDiscountedPrice(Double.valueOf(row[14].toString()));
        return varPricing;

    }

    private Product parseProduct(Object[] row){
        Product product = new Product();
        product.setId(Long.valueOf(row[15].toString()));
        product.setName(row[16].toString());
        product.setSubCategoryId(Long.valueOf(row[17].toString()));
        product.setImageUrl(row[18] != null ? row[18].toString():null);
        product.setDescription(row[19].toString());
        product.setLocalNames(row[20].toString());
        product.setSubCategory(parseSubCategory(row));
        return product;
    }

    private ProductUnit parseProductUnit(Object[] row) {
        ProductUnit productUnit = new ProductUnit();
        productUnit.setId(Long.valueOf(row[21].toString()));
        productUnit.setUnitName(row[22].toString());
        productUnit.setUnitQuantity(Double.valueOf(row[23].toString()));
        productUnit.setUnitType(row[24].toString());
        productUnit.setUnitMetric(row[25].toString());
        productUnit.setUnitDescription(row[26].toString());
        return productUnit;
    }
    private SubCategory parseSubCategory(Object[] row) {
        SubCategory subCategory = new SubCategory();
        subCategory.setId(Long.valueOf(row[27].toString()));
        subCategory.setName(row[28].toString());
        subCategory.setCategoryId(Long.valueOf(row[29].toString()));
        subCategory.setLocalNames(row[30].toString());
        subCategory.setCategory(parseCategory(row));
        return subCategory;
    }

    private Category parseCategory(Object[] row) {
        Category category = new Category();
        category.setId(Long.valueOf(row[31].toString()));
        category.setName(row[32].toString());
        category.setLocalNames(row[33].toString());
        return category;
    }

    public void delete(ProductPricing productPricing) {
        productPricing.setStatus(ProductPricing.Status.DISABLED.toString());
        persist(productPricing);
    }
}
