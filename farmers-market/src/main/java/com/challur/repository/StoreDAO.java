package com.challur.repository;

import com.challur.models.Order;
import com.challur.models.Store;
import com.challur.repository.interfaces.StoreRepository;
import com.google.common.base.Strings;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.hibernate.type.StringType;

import java.util.List;
import java.util.Map;

/**
 * Created by srinand.pc on 22/12/15.
 */
public class StoreDAO extends AbstractDAO<Store> implements StoreRepository {
    /**
     * Creates a new DAO with a given session provider.
     *
     * @param sessionFactory a session provider
     */
    public StoreDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Store create(Store store) {
        currentSession().persist(store);
        return store;
    }

    public List<Store> getStores(String query, Map<String,Object> parameters) {

        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(Store.class);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){

            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(),(List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return sqlQuery.list();
    }

    public Store update(Store store) {
        currentSession().persist(store);
        return store;
    }

    public Store delete(int id) {
        return null;
    }

    public List<Store> getStores(List<SimpleExpression> cirteria, int limit, int offset) {
        Criteria criteria = currentSession().createCriteria(Store.class);
        for(SimpleExpression expression : cirteria){
            criteria = criteria.add(expression);
        }

        criteria = criteria.setFirstResult(offset);
        if(limit > 0){
            criteria = criteria.setMaxResults(limit);
        }
        criteria.add(Restrictions.eq("status", Store.StoreStatus.ENABLED.toString()));
        return criteria.list();
    }
}
