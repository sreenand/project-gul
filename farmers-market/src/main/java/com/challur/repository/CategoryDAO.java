package com.challur.repository;

import com.challur.models.Category;
import com.challur.models.Order;
import com.challur.repository.interfaces.CategoryRepository;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.SimpleExpression;

import java.util.List;

/**
 * Created by srinandchallur on 12/04/16.
 */
public class CategoryDAO extends AbstractDAO<Category> implements CategoryRepository {
    /**
     * Creates a new DAO with a given session provider.
     *
     * @param sessionFactory a session provider
     */
    public CategoryDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Category create(Category category) {
        currentSession().persist(category);
        return category;
    }

    public Category update(Category category) {
        currentSession().persist(category);
        return category;
    }

    public List<Category> find(List<SimpleExpression> givenCriteria) {
        Criteria criteria = currentSession().createCriteria(Category.class);
        for(SimpleExpression expression : givenCriteria){
            criteria.add(expression);
        }
        return criteria.list();
    }
}
