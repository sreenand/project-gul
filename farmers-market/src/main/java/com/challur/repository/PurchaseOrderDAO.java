package com.challur.repository;

import com.challur.models.PurchaseOrder;
import com.challur.repository.interfaces.PurchaseOrderRepository;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

/**
 * Created by hariprasanthac on 06/08/16.
 */
public class PurchaseOrderDAO extends AbstractDAO<PurchaseOrder> implements PurchaseOrderRepository {

    /**
     * Creates a new DAO with a given session provider.
     *
     * @param sessionFactory a session provider
     */
    public PurchaseOrderDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public PurchaseOrder create(PurchaseOrder purchaseOrder) {
        currentSession().persist(purchaseOrder);
        return purchaseOrder;
    }

    public PurchaseOrder update(PurchaseOrder purchaseOrder) {
        currentSession().persist(purchaseOrder);
        return purchaseOrder;
    }

    public void update(String query, Map<String, Object> parameters) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(PurchaseOrder.class);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){
            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(),(List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        sqlQuery.executeUpdate();
    }

    public PurchaseOrder find(long purchaseOrderId) {
        return uniqueResult(currentSession().createCriteria(PurchaseOrder.class).
                add(Restrictions.eq("po_id", purchaseOrderId)));
    }

    public List<PurchaseOrder> find(String query) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(PurchaseOrder.class);
        return sqlQuery.list();
    }

    public List<PurchaseOrder> find(String query, Map<String, Object> parameters) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(PurchaseOrder.class);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){

            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(),(List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return sqlQuery.list();
    }

    public int count(List<SimpleExpression> givenCriteria) {
        return 0;
    }

    public int count(String query, Map<String, Object> parameters) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){
            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(), (List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return ((BigInteger) sqlQuery.uniqueResult()).intValue();
    }

    public List<PurchaseOrder> find(String query, Map<String, Object> parameters, int limit, int offset) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(PurchaseOrder.class);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){

            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(), (List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        sqlQuery.setFirstResult(offset);
        if(limit > 0){
            sqlQuery.setMaxResults(limit);
        }
        return sqlQuery.list();
    }
}
