package com.challur.repository;

import com.challur.models.ProductPricing;
import com.challur.models.VariablePricing;
import com.challur.repository.interfaces.VariablePricingRepository;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.SimpleExpression;

import java.util.List;
import java.util.Map;

/**
 * Created by hariprasanthac on 19/07/16.
 */
public class VariablePricingDAO extends AbstractDAO<VariablePricingDAO> implements VariablePricingRepository {

    public VariablePricingDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public VariablePricing create(VariablePricing variablePricing) {
        currentSession().persist(variablePricing);
        return variablePricing;
    }

    public VariablePricing update(VariablePricing variablePricing) {
        currentSession().persist(variablePricing);
        return variablePricing;
    }

    public List<VariablePricing> find(List<SimpleExpression> givenCriteria) {
        Criteria criteria = currentSession().createCriteria(VariablePricing.class);
        for(SimpleExpression expression : givenCriteria){
            criteria.add(expression);
        }
        return criteria.list();
    }

    public List<VariablePricing> find(String sqlQuery, Map<String, Object> parameters) {
        SQLQuery query = currentSession().createSQLQuery(sqlQuery);
        query.addEntity(VariablePricing.class);

        for(Map.Entry<String, Object> entry : parameters.entrySet()){
            if(entry.getValue() instanceof List){
                query.setParameterList(entry.getKey(),(List)entry.getValue());
            } else {
                query.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return query.list();
    }

    public void delete(VariablePricing variablePricing) {
        //persist(variablePricing);
    }
}
