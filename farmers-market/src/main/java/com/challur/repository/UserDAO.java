package com.challur.repository;

import com.challur.models.User;
import com.challur.models.VendorTransaction;
import com.challur.repository.interfaces.UserRepository;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by srinand.pc on 20/12/15.
 */
public class UserDAO extends AbstractDAO<User> implements UserRepository {

    /**
     * Creates a new DAO with a given session provider.
     *
     * @param sessionFactory a session provider
     */
    public UserDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public void create(User user) {
        currentSession().persist(user);
    }

    public void update(User user) {
        currentSession().persist(user);
    }

    public void delete(User user) {
        user.setStatus(User.UserStatus.DELETED.toString());
        currentSession().persist(user);
    }

    public User getUser(String userName, String password) {
        List<User> users = list(currentSession().createCriteria(User.class).
                add(Restrictions.eq("phoneNo", userName))
                .add(Restrictions.eq("password", password))
                .add(Restrictions.eq("status", User.UserStatus.ACTIVE.toString())));
        return users.get(0);
    }

    public User getUser(String userName) {
        List<User> users = list(currentSession().createCriteria(User.class).
                add(Restrictions.eq("phoneNo", userName)));
        return (users.size() > 0 ? users.get(0):null);
    }

    public User getUser(Long id){
        List<User> users = list(currentSession().createCriteria(User.class).
                add(Restrictions.eq("id", id)));
        return (users.size() > 0 ? users.get(0):null);
    }

    public List<User> find(String query) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(User.class);
        return sqlQuery.list();
    }
}
