package com.challur.repository;

import com.challur.models.Wastage;
import com.challur.repository.interfaces.WastageRepository;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by srinand.pc on 16/12/15.
 */
public class WastageDAO extends AbstractDAO<Wastage> implements WastageRepository {
    /**
     * Creates a new DAO with a given session provider.
     *
     * @param sessionFactory a session provider
     */
    public WastageDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public void create(Wastage wastage) {
        currentSession().persist(wastage);
    }

    public List<Wastage> getWastage(Long storeId) {
        return list(currentSession().createCriteria(Wastage.class).add(Restrictions.eq("storeId", storeId)));
    }

    public void update(Wastage wastage) {
        currentSession().persist(wastage);
    }

    public Wastage delete(int id) {
        return null;
    }
}
