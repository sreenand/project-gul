package com.challur.repository;

import com.challur.models.Locality;
import com.challur.repository.interfaces.LocalityRepository;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.SimpleExpression;

import java.util.List;

/**
 * Created by srinandchallur on 23/03/16.
 */
public class LocalityDAO extends AbstractDAO<Locality> implements LocalityRepository {
    public LocalityDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Locality create(Locality locality) {
        persist(locality);
        return locality;
    }

    public Locality update(Locality locality) {
        persist(locality);
        return locality;
    }

    public List<Locality> find(List<SimpleExpression> givenCriteria) {

        Criteria criteria = currentSession().createCriteria(Locality.class);

        for(SimpleExpression expression : givenCriteria){
            criteria.add(expression);
        }
        return criteria.list();

    }

    public void delete(Locality locality) {

    }
}
