package com.challur.repository;

import com.challur.models.Transaction;
import com.challur.models.TransactionItem;
import com.challur.repository.interfaces.TransactionRepository;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;

import java.util.List;
import java.util.Map;

/**
 * Created by srinandchallur on 09/05/16.
 */
public class TransactionDAO extends AbstractDAO<Transaction> implements TransactionRepository {
    /**
     * Creates a new DAO with a given session provider.
     *
     * @param sessionFactory a session provider
     */
    public TransactionDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Transaction create(Transaction transaction) {
        persist(transaction);
        return transaction;
    }

    public List<Transaction> find(String query, Map<String,Object> parameters) {


        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(Transaction.class);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){

            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(), (List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return sqlQuery.list();

    }

    public List<TransactionItem> findTransactionItems(String query, Map<String, Object> parameters) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(TransactionItem.class);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){

            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(), (List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return sqlQuery.list();
    }

    public Transaction update(Transaction transaction) {
        persist(transaction);
        return transaction;
    }
}
