package com.challur.repository;

import com.challur.models.Vendor;
import com.challur.models.Vendor;
import com.challur.repository.interfaces.VendorRepository;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;

import java.util.List;
import java.util.Map;

/**
 * Created by hariprasanthac on 10/08/16.
 */
public class VendorDAO extends AbstractDAO<Vendor> implements VendorRepository {

    public VendorDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Vendor create(Vendor vendor) {
        currentSession().persist(vendor);
        return vendor;
    }

    public Vendor update(Vendor vendor) {
        currentSession().persist(vendor);
        return vendor;
    }

    public void update(String query, Map<String, Object> parameters) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(Vendor.class);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){
            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(),(List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        sqlQuery.executeUpdate();
    }

    public Vendor find(long vendorId) {
        return uniqueResult(currentSession().createCriteria(Vendor.class).
                add(Restrictions.eq("vendor_id", vendorId)));
    }

    public List<Vendor> find(String query) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(Vendor.class);
        return sqlQuery.list();
    }

    public List<Vendor> find(String query, Map<String, Object> parameters) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(Vendor.class);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){

            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(),(List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return sqlQuery.list();
    }

    public int count(List<SimpleExpression> givenCriteria) {
        return 0;
    }

    public int count(String query, Map<String, Object> parameters) {
        return 0;
    }

    public List<Vendor> find(String query, Map<String, Object> parameters, int limit, int offset) {
        return null;
    }
}
