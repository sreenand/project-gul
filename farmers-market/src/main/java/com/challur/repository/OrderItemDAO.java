package com.challur.repository;

import com.challur.models.OrderItem;
import com.challur.repository.interfaces.OrderItemRepository;
import com.google.inject.Inject;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.Map;

/**
 * Created by srinand.pc on 16/12/15.
 */
public class OrderItemDAO extends AbstractDAO<OrderItem> implements OrderItemRepository {


    @Inject
    public OrderItemDAO(SessionFactory sessionFactory){
        super(sessionFactory);
    }

    public void create(OrderItem orderItem) {
        currentSession().persist(orderItem);
    }

    public void create(List<OrderItem> orderItems) {

        for(OrderItem orderItem : orderItems) {
            currentSession().persist(orderItem);
        }
    }

    public List<OrderItem> getOrderItems(Long orderId, int limit, int offset) {

        Criteria criteria = currentSession().createCriteria(OrderItem.class).
                add(Restrictions.eq("orderId", orderId)).setFirstResult(offset).setMaxResults(limit);
        return list(criteria);
    }

    public List<OrderItem> getOrderItems(String query, Map<String, Object> parameters) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(OrderItem.class);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){
            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(),(List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return sqlQuery.list();
    }

    public OrderItem getOrderItem(Long orderId) {
        return uniqueResult(currentSession().createCriteria(OrderItem.class).add(Restrictions.eq("orderId", orderId)));
    }

    public OrderItem update(OrderItem orderItem) {
        currentSession().persist(orderItem);
        return orderItem;
    }

    public OrderItem delete(int id) {
        return null;
    }
}
