package com.challur.repository;

import com.challur.models.Order;
import com.challur.models.OrderPayment;
import com.challur.models.Product;
import com.challur.repository.interfaces.OrderRepository;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by srinand.pc on 08/02/16.
 */
public class OrderDAO extends AbstractDAO<Order> implements OrderRepository {

    /**
     * Creates a new DAO with a given session provider.
     *
     * @param sessionFactory a session provider
     */
    public OrderDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Order create(Order order){
        currentSession().persist(order);
        return order;
    }

    public Order find(long orderId) {
        return (Order)uniqueResult(currentSession().createCriteria(Order.class).
                add(Restrictions.eq("id", orderId)));
    }

    public List<Order> find(List<SimpleExpression> givenCriteria, int limit, int offset) {
        Criteria criteria = currentSession().createCriteria(Order.class);
        for(SimpleExpression expression : givenCriteria){
            criteria.add(expression);
        }
        criteria = criteria.setFirstResult(offset);
        if(limit > 0){
            criteria = criteria.setMaxResults(limit);
        }
        criteria.addOrder(org.hibernate.criterion.Order.desc("id"));
        return criteria.list();
    }

    public List<Order> find(String query, Map<String,Object> parameters) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){

            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(),(List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return sqlQuery.list();
    }

    public List<OrderPayment> findOrderPayments(String query, Map<String,Object> parameters) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(OrderPayment.class);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){

            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(),(List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return sqlQuery.list();
    }

    public int count(List<SimpleExpression> givenCriteria) {
        Criteria criteria = currentSession().createCriteria(Order.class);
        for(SimpleExpression expression : givenCriteria){
            criteria.add(expression);
        }
        criteria.setProjection(Projections.rowCount());

        return (Integer)criteria.uniqueResult();
    }

    public int count(String query, Map<String, Object> parameters) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){
            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(), (List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return ((BigInteger) sqlQuery.uniqueResult()).intValue();
    }

    public List<Order> find(String query, Map<String, Object> parameters, int limit, int offset) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(Order.class);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){

            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(), (List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        sqlQuery.setFirstResult(offset);
        if(limit > 0){
            sqlQuery.setMaxResults(limit);
        }
        return sqlQuery.list();
    }

    public Order update(Order order) {
        currentSession().persist(order);
        return order;
    }

    public void update(String query, Map<String, Object> parameters) {
        SQLQuery sqlQuery = currentSession().createSQLQuery(query);
        sqlQuery.addEntity(Order.class);
        for(Map.Entry<String, Object> entry : parameters.entrySet()){
            if(entry.getValue() instanceof List){
                sqlQuery.setParameterList(entry.getKey(),(List)entry.getValue());
            } else {
                sqlQuery.setParameter(entry.getKey(), entry.getValue());
            }
        }
        sqlQuery.executeUpdate();
    }
}
