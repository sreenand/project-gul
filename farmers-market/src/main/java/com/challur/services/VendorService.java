package com.challur.services;

import com.challur.models.Vendor;
import com.challur.repository.interfaces.VendorRepository;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hariprasanthac on 10/08/16.
 */
public class VendorService {
    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final Logger logger = LoggerFactory.getLogger(VendorService.class);
    private VendorRepository vendorRepo;

    @Inject
    public VendorService(VendorRepository vendorRepo){
        this.vendorRepo = vendorRepo;
    }

    public List<Vendor> getAllVendors(String marketSelected){
        StringBuffer sql = new StringBuffer("select v.* from vendors v where sourcing_mkt_id = :marketSelected");
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("marketSelected",marketSelected);
        return vendorRepo.find(sql.toString(), parameters);
    }

    public List<Vendor> getVendors() {
        StringBuffer sql = new StringBuffer("select v.* from vendors v join sourcing_point s on (v.sourcing_mkt_id = s.sourcing_mkt_id)");
        return vendorRepo.find(sql.toString());
    }

    public Vendor addVendor(Vendor vendor) {
        return vendorRepo.create(vendor);
    }
}
