package com.challur.services;

import com.challur.models.Locality;
import com.challur.repository.interfaces.LocalityRepository;
import com.google.inject.Inject;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by srinandchallur on 18/04/16.
 */
public class LocalityService {

    private LocalityRepository localityRepository;

    @Inject
    public LocalityService(LocalityRepository localityRepository){
        this.localityRepository = localityRepository;
    }

    public Locality createLocality(Locality locality){
        return localityRepository.create(locality);
    }

    public Locality findLocality(String localityName, Long cityId){

        List<SimpleExpression> criteria = new ArrayList<SimpleExpression>();

        criteria.add(Restrictions.eq("name",localityName));
        criteria.add(Restrictions.eq("cityId",cityId));
        List<Locality> localityList = localityRepository.find(criteria);

        if(localityList == null || localityList.size() == 0){
            return null;
        }
        return localityList.get(0);
    }

}
