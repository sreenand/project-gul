package com.challur.services;

import com.challur.models.*;
import com.challur.repository.interfaces.*;
import com.google.inject.Inject;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by srinand.pc on 31/12/15.
 */

public class ProductService {

    private ProductRepository productRepository;
    private CategoryRepository categoryRepository;
    private SubCategoryRepository subCategoryRepository;
    private ProductPricingRepo productPricingRepo;
    private VariablePricingRepository variablePricingRepo;

    @Inject
    public ProductService(ProductRepository productRepository,
                          CategoryRepository categoryRepository, ProductPricingRepo productPricingRepo,
                          SubCategoryRepository subCategoryRepository, VariablePricingRepository variablePricingRepo) {
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
        this.subCategoryRepository = subCategoryRepository;
        this.productPricingRepo = productPricingRepo;
        this.variablePricingRepo = variablePricingRepo;
    }

    public List<Product> getAllSkus() {

        String sql = "select *  FROM products";
        return productRepository.getProducts(sql, null);
    }

    public List<Product> getAllSkus(Long subCatId) {

        String sql = "select *  FROM products where sub_category_id = :subCategoryId";
        HashMap<String, Object> paramlist = new HashMap<String, Object>();
        paramlist.put("subCategoryId", subCatId);
        return productRepository.getProducts(sql, paramlist);
    }


    public List<Product> getSkus(List<Long> ids) {
        return productRepository.getProducts(ids);
    }

    public Product create(Product product) {
        return productRepository.create(product);
    }

    public List<ProductPricing> getProductsWithPricing(Long cityId, Long subCategoryId, boolean onlyEnabled) {

        List<ProductPricing> consolidatedProductPricingOne = new ArrayList<ProductPricing>();


        StringBuffer sqlQuery = new StringBuffer("select b.*, d.*, a.id as a_id, a.name as a_name, a.sub_category_id, a.image_url, a.description, a.local_names as a_local_names, c.id as c_id, c.unit_name as c_name, c.unit_quantity, c.unit_type, c.unit_metric, c.unit_description, s.id as s_id, s.name as s_name, s.category_id, s.local_names as s_local_names, t.id as t_id, t.name as t_name, t.local_names as t_local_names from products a join sub_categories s on (a.sub_category_id = s.id) join categories t on (s.category_id = t.id) join product_pricing b on (a.sub_category_id = :sub_cat_id ");

        if (onlyEnabled) {
            sqlQuery.append("&& b.status = \"ENABLED\" ");
        }
        sqlQuery.append("&& a.id = b.product_id && b.city_id = :city_id) join product_units c on (b.unit_id = c.id) left join variable_pricing d on (b.id = d.product_pricing_id)");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("sub_cat_id", subCategoryId);
        parameters.put("city_id", cityId);
        consolidatedProductPricingOne.addAll(productPricingRepo.fetchVaribalePricing(sqlQuery.toString(), parameters, 30, 0));
        return consolidatedProductPricingOne;
    }


    public List<VariablePricing> getProductsVariablePricing(Long productId) {

        StringBuffer sqlQuery = new StringBuffer("select v.* from variable_pricing v where v.product_id = :product_id");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("product_id", productId);
        return variablePricingRepo.find(sqlQuery.toString(), parameters);
    }

    public List<Category> getAllCategories() {
        List<SimpleExpression> criteria = new ArrayList<SimpleExpression>();
        return categoryRepository.find(criteria);
    }

    public List<SubCategory> getAllSubCategories(Long categoryId) {
        List<SimpleExpression> criteria = new ArrayList<SimpleExpression>();
        criteria.add(Restrictions.eq("categoryId", categoryId));
        return subCategoryRepository.find(criteria);
    }

    public List<Product> getByName(String name) {

        String sqlQuery = "select * from products where replace(name,' ','_') like %:name%";
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("name", name);
        return productRepository.getProducts(sqlQuery, parameters);
    }

    public void updatePricing(ProductPricing pricing) {

        List<SimpleExpression> criteria = new ArrayList<SimpleExpression>();
        criteria.add(Restrictions.eq("id", pricing.getId()));
        List<ProductPricing> pricings = productPricingRepo.find(criteria);
        pricings.get(0).setStatus(pricing.getStatus());
        pricings.get(0).setPricePerUnit(pricing.getPricePerUnit());
        pricings.get(0).setMinQuantity(pricing.getMinQuantity());
        pricings.get(0).setMaxQuantity(pricing.getMaxQuantity());
        productPricingRepo.update(pricings.get(0));
    }
}
