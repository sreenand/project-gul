package com.challur.services;

import com.challur.models.VendorTransaction;
import com.challur.repository.interfaces.VendorTransactionRepository;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hariprasanthac on 12/08/16.
 */
public class VendorTransactionService {
    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final Logger logger = LoggerFactory.getLogger(VendorTransactionService.class);
    private VendorTransactionRepository vendorTransactionRepo;

    @Inject
    public VendorTransactionService(VendorTransactionRepository vendorTransactionRepo){
        this.vendorTransactionRepo = vendorTransactionRepo;
    }

    public List<VendorTransaction> getAllVendorTransactions(){
        StringBuffer sql = new StringBuffer("select v.* from vendor_transaction v");
        return vendorTransactionRepo.find(sql.toString());
    }

    public VendorTransaction getVendorTransactions(Long order_id, Long vendor_id){
//        StringBuffer sql = new StringBuffer("select v.* from vendor_transaction v where v.vendor_id = :vendor_id and v.po_id = :order_id");
//        Map<String,Object> parameters = new HashMap<String, Object>();
//        parameters.put("vendor_id",vendor_id);
//        parameters.put("order_id",order_id);
        return vendorTransactionRepo.find(vendor_id,order_id);
    }

    public List<VendorTransaction> getVendorTransaction(Long vendor_id){
        StringBuffer sql = new StringBuffer("select v.* from vendor_transaction v where v.vendor_id = :vendor_id");
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("vendor_id",vendor_id);
        return vendorTransactionRepo.find(sql.toString(), parameters);
    }

    public void add(VendorTransaction vendorTransaction){

        StringBuffer sql = new StringBuffer("insert into vendor_transaction (vendor_id, po_id, payment_mode, payment_status, total_amount, amout_paid) values(:vendor_id, :po_id, :payment_mode, :payment_status, :total_amount, :amout_paid) on duplicate key update total_amount = total_amount, amout_paid = amout_paid + :amout_paid, payment_status = :payment_status");
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("vendor_id",vendorTransaction.getVendor_id());
        parameters.put("po_id",vendorTransaction.getPo_id());
        parameters.put("payment_mode",vendorTransaction.getPayment_mode());
        parameters.put("payment_status",vendorTransaction.getPayment_status());
        parameters.put("total_amount",vendorTransaction.getTotal_amout());
        parameters.put("amout_paid",vendorTransaction.getAmout_paid());
        vendorTransactionRepo.create(sql.toString(), parameters);
    }
}
