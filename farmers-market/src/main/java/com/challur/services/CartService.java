package com.challur.services;

import com.challur.models.Cart;
import com.challur.repository.interfaces.CartRepository;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hariprasanthac on 10/08/16.
 */
public class CartService {
    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final Logger logger = LoggerFactory.getLogger(CartService.class);
    private CartRepository cartRepo;

    @Inject
    public CartService(CartRepository cartRepo){
        this.cartRepo = cartRepo;
    }

    public List<Cart> getAllCarts(){
        StringBuffer sql = new StringBuffer("select c.* from purchase_cart c join vendors v on (c.vendor_id = v.vendor_id) join products p on (c.sku_code = p.id) join sourcing_point s on (c.sourcing_mkt_id = s.sourcing_mkt_id)");
        return cartRepo.find(sql.toString());
    }

    public Object addToCart(Cart cart){
        return cartRepo.create(cart);
    }

    public Object removeFromCart(Cart cart){
//        cartRepo.delete(cart);
        Long id = cart.getId();
        StringBuffer sql = new StringBuffer("delete from purchase_cart where id = :id");
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("id",id);
        cartRepo.delete(sql.toString(), parameters);
        return null;
    }
}
