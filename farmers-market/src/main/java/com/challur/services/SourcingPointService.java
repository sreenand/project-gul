package com.challur.services;

import com.challur.models.SourcingPoint;
import com.challur.repository.interfaces.SourcingPointRepository;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hariprasanthac on 10/08/16.
 */
public class SourcingPointService {
    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final Logger logger = LoggerFactory.getLogger(SourcingPointService.class);
    private SourcingPointRepository sourcingPointRepo;

    @Inject
    public SourcingPointService(SourcingPointRepository sourcingPointRepo){
        this.sourcingPointRepo = sourcingPointRepo;
    }

    public List<SourcingPoint> getAllMarkets(){
        StringBuffer sql = new StringBuffer("select s.* from sourcing_point s");
        return sourcingPointRepo.find(sql.toString());
    }
}
