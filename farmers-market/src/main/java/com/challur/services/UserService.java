package com.challur.services;

import com.challur.configuration.SystemUsersConfiguration;
import com.challur.models.*;
import com.challur.repository.interfaces.OrderRepository;
import com.challur.repository.interfaces.TransactionRepository;
import com.challur.repository.interfaces.UserRepository;
import com.google.inject.Inject;
import org.apache.commons.lang.RandomStringUtils;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.*;

/**
 * Created by srinand.pc on 20/12/15.
 */
public class UserService {


    private UserRepository userRepository;
    private TransactionRepository transactionRepository;
    private OrderRepository orderRepository;
    private SystemUsersConfiguration systemUsersConfiguration;

    @Inject
    public UserService (UserRepository repository,
                        TransactionRepository transactionRepository,
                        OrderRepository orderRepository,
    SystemUsersConfiguration configuration){
        this.userRepository = repository;
        this.transactionRepository = transactionRepository;
        this.orderRepository = orderRepository;
        this.systemUsersConfiguration = configuration;
    }


    public User getUser(String userName, String password){
        return userRepository.getUser(userName, MD5(password));
    }

    public User getUser(String userName){
        return userRepository.getUser(userName);
    }

    public User getUser(Long id){
        return userRepository.getUser(id);
    }

    public void createUser(User user){

        userRepository.create(user);
        UserWallet wallet = new UserWallet();
        wallet.setUpperLimit(5000.0);
        wallet.setLowerLimit(-5000.0);
        wallet.setCurrentBalance(0.0);
        user.setUserWallet(wallet);
        user.setPassword(MD5(user.getPassword()));
        wallet.setUserId(user.getId());
        userRepository.update(user);
    }

    public void updateUser(User updatedUser, User user){
        user.setStatus(updatedUser.getStatus());
        user.setType(updatedUser.getType());
        user.setName(updatedUser.getName());
        user.setAddress(updatedUser.getAddress());
        user.getUserWallet().setLowerLimit(updatedUser.getUserWallet().getLowerLimit());
        user.getUserWallet().setUpperLimit(updatedUser.getUserWallet().getUpperLimit());
        userRepository.update(user);
    }

    public void resetPassword(User sessionUser, Long userId, String newPassword){

        if(sessionUser.getId().equals(userId) || sessionUser.getType().equals(User.UserType.ADMIN.toString())){
            User user = userRepository.getUser(userId);
            user.setPassword(MD5(newPassword));
        }  else{
            throw new WebApplicationException(Response.Status.UNAUTHORIZED);
        }

    }

    public void reconcileCash(User donor, Long cityId){

        if(donor.getUserWallet().getCurrentBalance() >= 0){
            return;
        }

        User systemUser = getSystemUser(cityId);
        List<Transaction> transactions = findPendingCashTransaction(donor.getId());
        Double amount = donor.getUserWallet().getCurrentBalance() * (-1);
        Transaction reconcileTransaction = new Transaction(null,null, RandomStringUtils.randomAlphabetic(10),donor.getId(),systemUser.getId(),amount,amount,
                Transaction.RECON_STATUS.PENDING.toString(),Transaction.INR, Transaction.TRANSACTION_TYPE.MONEY_RECON.toString(), Transaction.PAYMENT_MODE.CASH.toString(),new Date());

        reconcileTransaction.setTransactionItems(new ArrayList<TransactionItem>());
        TransactionItem item = new TransactionItem(null,reconcileTransaction,RandomStringUtils.randomAlphanumeric(11),amount, Transaction.RECON_STATUS.PENDING.toString(),null);
        reconcileTransaction.getTransactionItems().add(item);

        Transaction reconcileCreditTransaction = new Transaction(null,null, RandomStringUtils.randomAlphabetic(10),systemUser.getId(),donor.getId(),amount,amount,
                Transaction.RECON_STATUS.COMPLETE.toString(),Transaction.INR, Transaction.TRANSACTION_TYPE.WALLET_CREDIT.toString(), Transaction.PAYMENT_MODE.STORE_CREDIT.toString(),new Date());
        reconcileCreditTransaction.setTransactionItems(new ArrayList<TransactionItem>());
        TransactionItem creditItem = new TransactionItem(null,reconcileCreditTransaction,RandomStringUtils.randomAlphanumeric(11),amount, Transaction.RECON_STATUS.COMPLETE.toString(),null);
        reconcileTransaction.getTransactionItems().add(creditItem);

        for (Transaction transaction : transactions){
            transaction.setReconcilationStatus(Transaction.RECON_STATUS.COMPLETE.toString());
            transaction.getTransactionItems().get(0).setReconcilationStatus(Transaction.RECON_STATUS.COMPLETE.toString());
            transaction.setAmountReconciled(transaction.getAmount());
            transaction.getTransactionItems().get(0).setReconcilationTxnItemId(item.getExternalId());
        }

        transactionRepository.create(reconcileTransaction);
        transactionRepository.create(reconcileCreditTransaction);
        donor.getUserWallet().setCurrentBalance(0.0);
        double systemBalance = systemUser.getUserWallet().getCurrentBalance();
        systemUser.getUserWallet().setCurrentBalance(systemBalance - reconcileTransaction.getAmount());

    }

    public List<Transaction> findPendingCashTransaction(Long userId){

        String sqlQuery = "select * from transactions where type = 'MONEY_DEBIT' and recepient = :user_id and reconcilation_status <> 'COMPLETE'";
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("user_id",userId);
        List<Transaction> payments = transactionRepository.find(sqlQuery,parameters);
        return payments;
    }

    public void collectCashAndAddMoney(User donor, User recepient, Double amount, Long storeId){


        Transaction cashTransaction = new Transaction(null,null,RandomStringUtils.randomAlphanumeric(10), recepient.getId()
                ,donor.getId(),amount,0.0, Transaction.RECON_STATUS.PENDING.toString(),Transaction.INR,Transaction.TRANSACTION_TYPE.MONEY_DEBIT.toString(),
                Transaction.PAYMENT_MODE.CASH.toString(),new Date());

        TransactionItem transactionItem = new TransactionItem(null,cashTransaction,RandomStringUtils.randomAlphanumeric(11),amount,Transaction.RECON_STATUS.PENDING.toString(),null);

        List<TransactionItem> items = new ArrayList<TransactionItem>();
        items.add(transactionItem);
        cashTransaction.setTransactionItems(items);


        Transaction creditTransaction = new Transaction(null,null,cashTransaction.getExternalId(),donor.getId(),
                recepient.getId(),amount,0.0,Transaction.RECON_STATUS.PENDING.toString(),Transaction.INR,Transaction.TRANSACTION_TYPE.WALLET_CREDIT.toString(),
                Transaction.PAYMENT_MODE.STORE_CREDIT.toString(),new Date());

        TransactionItem creditTransactionItem = new TransactionItem(null,creditTransaction,RandomStringUtils.randomAlphanumeric(11),
                amount,Transaction.RECON_STATUS.PENDING.toString(),null);

        List<TransactionItem> creditItems = new ArrayList<TransactionItem>();
        creditItems.add(creditTransactionItem);
        creditTransaction.setTransactionItems(creditItems);

        Double unRecoAmount = 0.0;

        if(recepient.getUserWallet().getCurrentBalance() >= 0){
            TransactionItem lastCredit = getLastUnreconciledCreditTransactionItems(recepient);
            if(lastCredit != null) {
                unRecoAmount = lastCredit.getAmount();
                lastCredit.setReconcilationTxnItemId(creditTransactionItem.getExternalId());
                lastCredit.setReconcilationStatus(Transaction.RECON_STATUS.COMPLETE.toString());
                lastCredit.getTransaction().setReconcilationStatus(Transaction.RECON_STATUS.COMPLETE.toString());
                lastCredit.getTransaction().setAmountReconciled(lastCredit.getTransaction().getAmount());
            }
            creditTransactionItem.setAmount(amount + unRecoAmount);
            creditTransaction.setAmount(amount+ unRecoAmount);
        } else {
            reconcileCreditTransaction(recepient,creditTransaction);
        }

        transactionRepository.create(cashTransaction);
        transactionRepository.create(creditTransaction);

        Double currentBalance = recepient.getUserWallet().getCurrentBalance();
        recepient.getUserWallet().setCurrentBalance(new Double(currentBalance + amount));
        currentBalance = donor.getUserWallet().getCurrentBalance();
        donor.getUserWallet().setCurrentBalance(new Double(currentBalance - amount));
        reconcileOrders(storeId);

    }

    public void reconcileOrders(Long storeId){

        List<OrderPayment> payments = findPaymentPendingOrders(storeId);
        for(OrderPayment payment: payments){
            checkPaymentsAndMarkComplete(payment);
        }
    }

    public List<OrderPayment> findPaymentPendingOrders(Long storeId){

        String sqlQuery = "select b.* from orders a join order_payments b on b.order_id = a.id where b.status <> 'COMPLETED' && a.store_id = :store_id";
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("store_id",storeId);
        List<OrderPayment> payments = orderRepository.findOrderPayments(sqlQuery,parameters);
        return payments;
    }

    public void checkPaymentsAndMarkComplete(OrderPayment payment){

        if( payment.getTransaction() != null && payment.getTransaction().getReconcilationStatus().equals(Transaction.RECON_STATUS.COMPLETE.toString())){
            payment.setStatus(OrderPayment.OrderPaymentStatus.RECEIVED.toString());
        }
    }

    public List<TransactionItem> getAllUnreconciledDebitTransactionItems(User user){
        String sqlQuery = "select b.* from transactions a join transaction_items b on a.id = b.transaction_id and a.type = 'WALLET_DEBIT' " +
                "and a.donor = :user_id and b.reconcilation_status <> 'COMPLETE' ";
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("user_id",user.getId());
        List<TransactionItem> transactionItems = (List<TransactionItem>)transactionRepository.findTransactionItems(sqlQuery,parameters);
        return transactionItems;
    }

    public TransactionItem getLastUnreconciledCreditTransactionItems(User user){
        String sqlQuery = "select * from transactions where type = 'WALLET_CREDIT' and reconcilation_status <> 'COMPLETE' and recepient = :user_id";
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("user_id",user.getId());
        List<Transaction> transactions = (List<Transaction>)transactionRepository.find(sqlQuery,parameters);

        if(transactions == null || transactions.size() == 0){
            return null;
        }

        for(TransactionItem item : transactions.get(0).getTransactionItems()){
            if(!item.getReconcilationStatus().equals(Transaction.RECON_STATUS.COMPLETE.toString())){
                return item;
            }
        }
        return null;
    }

    public void reconcileCreditTransaction(User user, Transaction transaction){

        Double difference = user.getUserWallet().getCurrentBalance() + (transaction.getAmount());
        List<TransactionItem> unReconciledItems = getAllUnreconciledDebitTransactionItems(user);
        if(difference >= 0){
            if(difference > 0){
                TransactionItem item = new TransactionItem(null,transaction,RandomStringUtils.randomAlphanumeric(11),difference,
                        Transaction.RECON_STATUS.PENDING.toString(),null);
                transaction.getTransactionItems().add(item);
            }
            transaction.getTransactionItems().get(0).setAmount(transaction.getTransactionItems().get(0).getAmount() - difference);
            transaction.getTransactionItems().get(0).setReconcilationStatus(Transaction.RECON_STATUS.COMPLETE.toString());
            for( TransactionItem transactionItem : unReconciledItems){
                transactionItem.setReconcilationStatus(Transaction.RECON_STATUS.COMPLETE.toString());
                transactionItem.setReconcilationTxnItemId(transaction.getTransactionItems().get(0).getExternalId());
                transactionItem.getTransaction().setAmountReconciled(transactionItem.getTransaction().getAmountReconciled() +  transactionItem.getAmount());

                if(transactionItem.getTransaction().getAmount().equals(transactionItem.getTransaction().getAmountReconciled())){
                    transactionItem.getTransaction().setReconcilationStatus(Transaction.RECON_STATUS.COMPLETE.toString());
                }

            }

            transaction.setAmountReconciled(transaction.getAmount() - difference);
            if(difference == 0.0){
                transaction.setReconcilationStatus(Transaction.RECON_STATUS.COMPLETE.toString());
                transaction.getTransactionItems().get(0).setReconcilationStatus(Transaction.RECON_STATUS.COMPLETE.toString());
            }
        } else{

            transaction.setReconcilationStatus(Transaction.RECON_STATUS.COMPLETE.toString());
            transaction.setAmountReconciled(transaction.getAmount());
            transaction.getTransactionItems().get(0).setReconcilationStatus(Transaction.RECON_STATUS.COMPLETE.toString());
            List<TransactionItem> reconcilableItems = new ArrayList<TransactionItem>();
            Double transactionAmount = transaction.getAmount();
            for(TransactionItem transactionItem : unReconciledItems){
                transactionAmount = transactionAmount - transactionItem.getAmount();

                reconcilableItems.add(transactionItem);
                if(transactionAmount == 0){
                    break;
                } else if(transactionAmount > 0){
                    continue;
                } else if(transactionAmount < 0){
                    TransactionItem transactionItem1 = new TransactionItem(null,transactionItem.getTransaction(),
                            RandomStringUtils.randomAlphanumeric(11),(-1)*transactionAmount,Transaction.RECON_STATUS.PENDING.toString(),null);
                    transactionItem.setAmount(transactionItem.getAmount() - transactionItem1.getAmount());
                    transactionItem.getTransaction().getTransactionItems().add(transactionItem1);
                    break;
                }

            }

            for(TransactionItem transactionItem : reconcilableItems){
                transactionItem.setReconcilationStatus(Transaction.RECON_STATUS.COMPLETE.toString());
                transactionItem.setReconcilationTxnItemId(transaction.getTransactionItems().get(0).getExternalId());
                transactionItem.getTransaction().setAmountReconciled(transactionItem.getTransaction().getAmountReconciled() +  transactionItem.getAmount());

                if(transactionItem.getTransaction().getAmount().equals(transactionItem.getTransaction().getAmountReconciled())){
                    transactionItem.getTransaction().setReconcilationStatus(Transaction.RECON_STATUS.COMPLETE.toString());
                }
            }
        }

    }

    public void reconcileDebitTransaction(User donor, User recepient, Transaction transaction, Long storeId){


        Double difference = donor.getUserWallet().getCurrentBalance() - (transaction.getAmount());
        TransactionItem item = getLastUnreconciledCreditTransactionItems(donor);
        if(donor.getUserWallet().getCurrentBalance() > 0){

            if(difference >= 0 ){

                transaction.setReconcilationStatus(Transaction.RECON_STATUS.COMPLETE.toString());
                transaction.setAmountReconciled(transaction.getAmount());
                transaction.getTransactionItems().get(0).setReconcilationTxnItemId(item.getExternalId());
                transaction.getTransactionItems().get(0).setReconcilationStatus(Transaction.RECON_STATUS.COMPLETE.toString());
                item.setAmount(transaction.getAmount());

                if(difference > 0){
                    TransactionItem splitItem = new TransactionItem(null,item.getTransaction(),RandomStringUtils.randomAlphanumeric(11),
                            difference,Transaction.RECON_STATUS.PENDING.toString(),null);
                    item.getTransaction().getTransactionItems().add(splitItem);
                }

            } else {

                TransactionItem splitItem = new TransactionItem(null,transaction,RandomStringUtils.randomAlphanumeric(11),
                        difference*(-1),Transaction.RECON_STATUS.PENDING.toString(),null);

                transaction.getTransactionItems().add(splitItem);
                transaction.getTransactionItems().get(0).setAmount(transaction.getTransactionItems().get(0).getAmount()
                        + difference);

                transaction.getTransactionItems().get(0).setReconcilationStatus(Transaction.RECON_STATUS.COMPLETE.toString());
                transaction.getTransactionItems().get(0).setReconcilationTxnItemId(item.getExternalId());
                transaction.setAmountReconciled(transaction.getAmount() + difference);
            }

            item.setReconcilationStatus(Transaction.RECON_STATUS.COMPLETE.toString());
            item.getTransaction().setAmountReconciled(
                    item.getTransaction().getAmountReconciled() + item.getAmount());
            if(item.getTransaction().getAmount().equals(item.getTransaction().getAmountReconciled())){
                item.getTransaction().setReconcilationStatus(Transaction.RECON_STATUS.COMPLETE.toString());
            }

        }

        Double currentBalance = donor.getUserWallet().getCurrentBalance();
        donor.getUserWallet().setCurrentBalance(currentBalance - transaction.getAmount());
        recepient.getUserWallet().setCurrentBalance(recepient.getUserWallet().getCurrentBalance()+ transaction.getAmount());
    }



    public static String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }


    public User getSystemUser(Long cityId){
        Long sysUserId = (Long)systemUsersConfiguration.getSystemUsers().get(cityId);
        return userRepository.getUser(sysUserId);
    }


    public List<User> getSalesUser() {
        String sqlQuery = "select * from users where type = 'SALES' and status = 'active'";
        return userRepository.find(sqlQuery);
    }
}

