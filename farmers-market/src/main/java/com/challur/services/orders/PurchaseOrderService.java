package com.challur.services.orders;

import com.challur.models.*;
import com.challur.repository.interfaces.CartRepository;
import com.challur.repository.interfaces.PurchaseOrderItemRepository;
import com.challur.repository.interfaces.PurchaseOrderRepository;
import com.challur.repository.interfaces.UserRepository;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hariprasanthac on 06/08/16.
 */
public class PurchaseOrderService {

    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final Logger logger = LoggerFactory.getLogger(PurchaseOrderService.class);
    private PurchaseOrderRepository purchaseOrderRepository;
    private PurchaseOrderItemRepository purchaseOrderItemRepository;
    private UserRepository userRepository;
    private CartRepository cartRepository;

    @Inject
    public PurchaseOrderService(PurchaseOrderRepository purchaseOrderRepository, PurchaseOrderItemRepository purchaseOrderItemRepository, CartRepository cartRepository, UserRepository userRepository){
        this.purchaseOrderRepository = purchaseOrderRepository;
        this.purchaseOrderItemRepository = purchaseOrderItemRepository;
        this.cartRepository = cartRepository;
        this.userRepository = userRepository;
    }

    public List<PurchaseOrder> getOpenPurchaseOrders(){
        StringBuffer sql = new StringBuffer("select * from purchase_orders where status = \"PLACED\" or status = \"UNDER_PROCUREMENT\"");
        return purchaseOrderRepository.find(sql.toString());
    }

    public Object updateOrder(long id, String status, Double total_cost, Double amount_paid, String payment_mode, String payment_status, DummyObject purchaseOrder) {
        StringBuffer sql = new StringBuffer("update purchase_orders p set p.total_cost = :total_cost, p.status = :status where p.po_id = :id");
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("id",id);
        parameters.put("status",status);
        parameters.put("total_cost",total_cost);
        parameters.put("amount_paid",amount_paid);
        parameters.put("payment_mode",payment_mode);
        parameters.put("payment_status",payment_status);
        purchaseOrderRepository.update(sql.toString(),parameters);
        return null;
    }

    public void placeOrder(PurchaseOrder order, List<Cart> orderItems){
        PurchaseOrder newOrder = purchaseOrderRepository.create(order);
        for(int i=0;i<orderItems.size();i++) {
            PurchaseOrderItem purchaseOrderItem = new PurchaseOrderItem();
            purchaseOrderItem.setPo_id(newOrder.getId());
            purchaseOrderItem.setSku_code(orderItems.get(i).getSku_code());
            purchaseOrderItem.setOrder_qty(orderItems.get(i).getOrder_qty());
            purchaseOrderItem.setSourcing_mkt_id(orderItems.get(i).getSourcing_mkt_id());
            purchaseOrderItem.setVendor_id(orderItems.get(i).getVendor_id());
            purchaseOrderItemRepository.create(purchaseOrderItem);
        }
        StringBuffer sql = new StringBuffer("delete from purchase_cart");
        cartRepository.delete(sql.toString());
    }

    public int getpOrdersCount(String statusString, String fromOrderDate, String toOrderDate){

        StringBuffer joinCondition = new StringBuffer("a.status in (:status)");
        List<String> statuslist = Arrays.asList(statusString.split(","));
        StringBuffer sql = new StringBuffer("select count(*) from purchase_orders a join users u on (a.user_id = u.id) where a.status in (:status) and a.order_date >= :fromDate and a.order_date < :toDate");
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("status", statuslist);
        parameters.put("fromDate", fromOrderDate);
        parameters.put("toDate", toOrderDate);
        return purchaseOrderRepository.count(sql.toString(), parameters);

    }

    public List<PurchaseOrder> getpOrders(String statusString, String fromOrderDate, String toOrderDate, int limit, int offset){

        List<String> statuslist = Arrays.asList(statusString.split(","));
        StringBuffer sql = new StringBuffer("select a.* from purchase_orders a join users u on (a.user_id = u.id) where a.status in (:status) and a.order_date >= :fromDate and a.order_date < :toDate");
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("status", statuslist);
        parameters.put("fromDate", fromOrderDate);
        parameters.put("toDate", toOrderDate);

        return purchaseOrderRepository.find(sql.toString(),parameters,limit,offset);

    }

    public User getUser(Long id){
        return userRepository.getUser(id);
    }

    public PurchaseOrder update(PurchaseOrder purchaseOrder){
        return purchaseOrderRepository.update(purchaseOrder);
    }

    public Object updateStatus(String ids, String status){
        List<String> idList = Arrays.asList(ids.split(","));
        StringBuffer sql = new StringBuffer("update purchase_orders set status = :status where po_id in (:idList)");
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("idList", idList);
        parameters.put("status", status);
        purchaseOrderRepository.update(sql.toString(),parameters);
        return null;
    }
}
