package com.challur.services.orders;

import com.challur.exceptions.AppError;
import com.challur.exceptions.FarmersMarketServiceException;
import com.challur.models.Order;
import com.challur.models.OrderItem;

/**
 * Created by srinandchallur on 03/05/16.
 */
public enum  OrderStateMachine {


       PROCESSING() {
           @Override
           public void execute(Order order, OrderService orderService) {
               if(!order.getStatus().equals(Order.OrderStatus.RECEIVED.toString())){
                  throw new FarmersMarketServiceException(403, AppError.NOT_AUTHORIZED,"Illegal State Change");
               }
               order.setStatus(Order.OrderStatus.PROCESSING.toString());
               for(OrderItem orderItem : order.getOrderItems()){
                   orderItem.setStatus(Order.OrderStatus.PROCESSING.toString());
               }

           }
       },

        DELIVERED() {
            @Override
            public void execute(Order order, OrderService orderService) {
                if(!order.getStatus().equals(Order.OrderStatus.DISPATCHED.toString()) || order.getPriceCalculated() == null
                        || order.getPriceCalculated() < 0){
                    throw new FarmersMarketServiceException(403, AppError.NOT_AUTHORIZED,"Illegal State Change");
                }
                order.setStatus(Order.OrderStatus.DELIVERED.toString());
                for(OrderItem orderItem : order.getOrderItems()){
                    orderItem.setStatus(Order.OrderStatus.DELIVERED.toString());
                }
                orderService.updateOrder(order);
            }
        },

        DISPATCHED() {
            @Override
            public void execute(Order order, OrderService orderService) {
                if(!order.getStatus().equals(Order.OrderStatus.PACKED.toString()) || order.getPriceCalculated() == null
                        || order.getPriceCalculated() < 0){
                    throw new FarmersMarketServiceException(403, AppError.NOT_AUTHORIZED,"Illegal State Change");
                }
                order.setStatus(Order.OrderStatus.DISPATCHED.toString());
                for(OrderItem orderItem : order.getOrderItems()){
                    orderItem.setStatus(Order.OrderStatus.DISPATCHED.toString());
                }

            }
        },


        CANCELLED() {
            @Override
            public void execute(Order order, OrderService orderService) {
                order.setStatus(Order.OrderStatus.CANCELLED.toString());
                for(OrderItem orderItem : order.getOrderItems()){
                    orderItem.setStatus(Order.OrderStatus.CANCELLED.toString());
                }

            }
        },

        PACKED() {
            @Override
            public void execute(Order order, OrderService orderService) {
                if(!order.getStatus().equals(Order.OrderStatus.PROCESSING.toString())){
                    throw new FarmersMarketServiceException(403, AppError.NOT_AUTHORIZED,"Illegal State Change");
                }
                order.setStatus(Order.OrderStatus.PACKED.toString());
                for(OrderItem orderItem : order.getOrderItems()){
                    orderItem.setStatus(Order.OrderStatus.PACKED.toString());
                }
            }
        };


    public abstract void execute(Order order, OrderService orderService);

}
