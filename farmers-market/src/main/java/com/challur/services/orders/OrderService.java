package com.challur.services.orders;

import com.challur.exceptions.AppError;
import com.challur.exceptions.FarmersMarketServiceException;
import com.challur.models.*;
import com.challur.repository.interfaces.*;
import com.challur.services.StoreService;
import com.challur.services.UserService;
import com.google.inject.Inject;
import org.apache.commons.lang.RandomStringUtils;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by srinand.pc on 08/02/16.
 */
public class OrderService {

    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final Logger logger = LoggerFactory.getLogger(OrderService.class);
    private OrderRepository orderRepository;
    private OrderItemRepository orderItemRepository;
    private UserService userService;
    private TransactionRepository transactionRepository;
    private StoreService storeService;

    @Inject
    public OrderService(OrderRepository orderRepository, OrderItemRepository orderItemRepository, UserService userService,
                        TransactionRepository transactionRepository, StoreService storeService){
        this.orderRepository = orderRepository;
        this.orderItemRepository = orderItemRepository;
        this.userService = userService;
        this.transactionRepository = transactionRepository;
        this.storeService = storeService;
    }

    public Order createOrder(Order order){

        Store store = storeService.getStoreById(order.getStoreId());
        User storeUser = userService.getUser(store.getUserId());

        if(storeUser.getUserWallet().getCurrentBalance() <= storeUser.getUserWallet().getLowerLimit()){

            logger.info("No enough balance to make this order for store {},{}",store.getName(), store.getPhoneNo());
            throw new FarmersMarketServiceException(403, AppError.FORBIDDEN,"Not Enough balance in the wallet");
        }
        order.setStatus(Order.OrderStatus.RECEIVED.toString());
        OrderPayment orderPayment = new OrderPayment(null,order, null,
                null, OrderPayment.OrderPaymentStatus.PENDING.toString());
        order.setOrderPayment(orderPayment);
        return orderRepository.create(order);
    }

    public Order updateOrder(Order order){
        return orderRepository.update(order);
    }

    public Order findOrder(long id){
        return orderRepository.find(id);
    }

    public List<Order> findOrders(List<Long> ids){
        StringBuffer sql = new StringBuffer("select o.* from orders o where o.id in (:ids)");
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("ids",ids);
        return orderRepository.find(sql.toString(),parameters,ids.size(),0);
    }

    public void updateOrderStatus(List<Long> ids, String status){
        StringBuffer sql = new StringBuffer("update orders set status = :status where id in (:ids)");
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("ids",ids);
        parameters.put("status",status);
        orderRepository.update(sql.toString(),parameters);
    }

    public void applyOrderStatusUpdate(List<Long> ids, String status){
        List<Order> orders = findOrders(ids);
        OrderStateMachine osm = OrderStateMachine.valueOf(status.toUpperCase());
        for(Order order : orders){
            osm.execute(order,this);
            orderRepository.update(order);
        }
    }


    public List getProcList(List<Long> ids){
        StringBuffer sql = new StringBuffer("select a.sku_code, b.name,p.id, sum(a.quantity*p.unit_quantity) as quantity_sum from product_units p join order_items a on p.id = a.product_unit_id " +
                "join products b  on a.sku_code = b.id where a.order_id in (:ids) group by a.sku_code, b.name, p.id");
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("ids",ids);
        return orderRepository.find(sql.toString(),parameters);
    }

    public List getFreqList(List<Long> ids){
        StringBuffer sql = new StringBuffer("select b.name,concat(a.quantity*d.unit_quantity,\" \", d.unit_metric) as quantity, count(*) " +
                "as frequency from order_items a join products b on sku_code = b.id join stores c on a.store_id = c.id join product_units d on a.product_unit_id = d.id " +
                "where  a.order_id in (:ids) group by b.name,quantity order by b.name");
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("ids",ids);
        return orderRepository.find(sql.toString(),parameters);
    }

    public int getOrdersCount(Long cityId, String statusString, String fulfillStatusString, String fromOrderDate, String toOrderDate){

        StringBuffer joinCondition = new StringBuffer("a.status in (:status)");
        List<String> statuslist = Arrays.asList(statusString.split(","));
        if(fulfillStatusString != null){
            joinCondition.append(" && a.fulfillment_status in (:fulfillStatus)");
        }
        StringBuffer sql = new StringBuffer("select count(*) from orders a join stores b on ("+ joinCondition +" && a.store_id = b.id) " +
                "join localities c on (b.locality_id = c.id) " +
                "join cities d on  (c.city_id = d.id && c.city_id = :cityId) where order_date >= :fromDate and order_date < :toDate");
        Map<String,Object> parameters = new HashMap<String, Object>();

        parameters.put("status", statuslist);
        parameters.put("cityId", cityId.toString());
        if(fulfillStatusString != null) {
            List<String> ffstatusList = Arrays.asList(fulfillStatusString.split(","));
            parameters.put("fulfillStatus", ffstatusList);
        }
        parameters.put("fromDate", fromOrderDate);
        parameters.put("toDate", toOrderDate);

        return orderRepository.count(sql.toString(), parameters);

    }

    public List<Order> getOrders(Long cityId, String statusString, String fulfillStatusString, String fromOrderDate, String toOrderDate, int limit, int offset){

        StringBuffer joinCondition = new StringBuffer("a.status in (:status)");
        List<String> statuslist = Arrays.asList(statusString.split(","));
        if(fulfillStatusString != null){
            joinCondition.append(" && a.fulfillment_status in (:fulfillStatus)");
        }
        StringBuffer sql = new StringBuffer("select a.* from orders a join stores b on ("+ joinCondition +" && a.store_id = b.id) " +
                "join localities c on (b.locality_id = c.id) " +
                "join cities d on  (c.city_id = d.id && c.city_id = :cityId) where order_date >= :fromDate and order_date < :toDate");
        Map<String,Object> parameters = new HashMap<String, Object>();

        parameters.put("status", statuslist);
        parameters.put("cityId", cityId.toString());
        if(fulfillStatusString != null) {
            List<String> ffstatusList = Arrays.asList(fulfillStatusString.split(","));
            parameters.put("fulfillStatus", ffstatusList);
        }
        parameters.put("fromDate", fromOrderDate);
        parameters.put("toDate", toOrderDate);

        return orderRepository.find(sql.toString(),parameters,limit,offset);

    }


    public List<Order> getOrders(Long storeId, int limit, int offset){

        List<SimpleExpression> criteria = new ArrayList<SimpleExpression>();
        criteria.add(Restrictions.eq("storeId",storeId));
        List<Order> orderList = orderRepository.find(criteria,limit,offset);

        if(orderList == null){
            orderList = new ArrayList<Order>();
        }

        return orderList;
    }

    public void applyUpdate(Order persistentOrder, Order updatedOrder){

        persistentOrder.setStatus(updatedOrder.getStatus());
        persistentOrder.setFulfillmentStatus(updatedOrder.getFulfillmentStatus());
        persistentOrder.setPriceCalculated(updatedOrder.getPriceCalculated());
        persistentOrder.setDiscount(updatedOrder.getDiscount());
        persistentOrder.setPricePromised(updatedOrder.getPricePromised());

        int index = 0;
        for(OrderItem orderItem : persistentOrder.getOrderItems()){

            OrderItem updatedItem = updatedOrder.getOrderItems().get(index);
            orderItem.setStatus(updatedItem.getStatus());
            orderItem.setFulfillmentStatus(updatedItem.getFulfillmentStatus());
            orderItem.setPriceOfferedPerUnit(updatedItem.getPriceOfferedPerUnit());
            orderItem.setQuantity(updatedItem.getQuantity());
            orderItem.setQuantityProcured(updatedItem.getQuantityProcured());
            orderItem.setDiscount(updatedItem.getDiscount());
            index++;
        }

        List<OrderItem> extraOrderItems = updatedOrder.getOrderItems().subList(persistentOrder.getOrderItems().size(),
                updatedOrder.getOrderItems().size());

        if(extraOrderItems != null && extraOrderItems.size() > 0){
            for(OrderItem orderItem : extraOrderItems){
                persistentOrder.getOrderItems().add(orderItem);
            }
        }

        if(updatedOrder.getStatus().equals(Order.OrderStatus.DELIVERED.toString())){
            updatePayment(persistentOrder);
        }
    }

    public void updatePayment(Order order){

        if(order.getOrderPayment().getStatus().equals(OrderPayment.OrderPaymentStatus.PENDING.toString())
                && order.getStatus().equals(Order.OrderStatus.DELIVERED.toString())){

            if(order.getOrderPayment().getTransaction() != null){
                return;
            }
            TransactionItem debitTransactionItem = new TransactionItem(null,null,RandomStringUtils.randomAlphanumeric(11),
                    order.getPriceCalculated(),Transaction.RECON_STATUS.PENDING.toString(),null);
            List<TransactionItem> items = new ArrayList<TransactionItem>();
            items.add(debitTransactionItem);
            Transaction debitTransaction = new Transaction(null,items, RandomStringUtils.randomAlphanumeric(10),order.getStore().getUserId(),
                    userService.getSystemUser(order.getStore().getLocality().getCityId()).getId(),order.getPriceCalculated(),0.0,Transaction.RECON_STATUS.PENDING.toString(),Transaction.INR,Transaction.TRANSACTION_TYPE.WALLET_DEBIT.toString(),
                    Transaction.PAYMENT_MODE.STORE_CREDIT.toString(),new Date());
            debitTransactionItem.setTransaction(debitTransaction);
            OrderPayment payment = order.getOrderPayment();
            payment.setTransaction(debitTransaction);
            transactionRepository.create(debitTransaction);
            orderRepository.update(order);
            userService.reconcileDebitTransaction(userService.getUser(order.getStore().getUserId()),
                    userService.getSystemUser(order.getStore().getLocality().getCityId()),debitTransaction, order.getStoreId());

            if(order.getOrderPayment().getTransaction().getReconcilationStatus().equals(Transaction.RECON_STATUS.COMPLETE.toString())){
                order.getOrderPayment().setStatus(OrderPayment.OrderPaymentStatus.RECEIVED.toString());
            }

        }
    }

}
