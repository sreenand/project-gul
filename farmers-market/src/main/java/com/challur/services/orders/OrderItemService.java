package com.challur.services.orders;

import com.challur.models.Order;
import com.challur.models.OrderItem;
import com.challur.models.Wastage;
import com.challur.repository.interfaces.OrderItemRepository;
import com.challur.repository.interfaces.WastageRepository;
import com.google.inject.Inject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by srinand.pc on 16/12/15.
 */
public class OrderItemService {


    private OrderItemRepository orderItemRepository;
    private WastageRepository wastageRepository;

    @Inject
    public OrderItemService(OrderItemRepository orderItemRepository, WastageRepository wastageRepository){
        this.orderItemRepository = orderItemRepository;
        this.wastageRepository = wastageRepository;
    }

    public void addDemand(OrderItem orderItem){
        orderItemRepository.create(orderItem);
    }

    public void addDemands(List<OrderItem> orderItems){
        orderItemRepository.create(orderItems);
    }

    public void addWastage(Wastage wastage){
        wastageRepository.create(wastage);
    }

    public List<OrderItem> getOrderItems(Long orderId, int limit, int offset){
        return orderItemRepository.getOrderItems(orderId,limit,offset);
    }

    public OrderItem getOrderItem(Long id){
        return orderItemRepository.getOrderItem(id);
    }

    public OrderItem updateOrderItem(OrderItem orderItem){
        return orderItemRepository.update(orderItem);
    }


    public List<OrderItem> populatePrices(Order order){

        String sqlQuery = "select a.id, a.store_id, a.sku_code, a.product_unit_id, a.quantity,a.quantity_procured,b.price_per_unit as price_per_unit," +
                "a.discount,a.created_at,a.updated_at,a.status, a.fulfillment_status,a.return_status,a.order_id, b.price_per_unit as price_offered_per_unit " +
                "from order_items a join product_pricing b on (b.city_id = :cityId && a.sku_code = b.product_id && a.product_unit_id = b.unit_id)where order_id = :orderId";


        Map<String,Object> parameters = new HashMap<String, Object>();

        parameters.put("cityId",order.getStore().getLocality().getCityId());
        parameters.put("orderId",order.getId());
        return orderItemRepository.getOrderItems(sqlQuery,parameters);

    }


}
