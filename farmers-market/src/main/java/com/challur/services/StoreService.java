package com.challur.services;

import com.challur.models.Locality;
import com.challur.models.Store;
import com.challur.models.User;
import com.challur.models.UserWallet;
import com.challur.repository.interfaces.LocalityRepository;
import com.challur.repository.interfaces.StoreRepository;
import com.google.inject.Inject;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;

import java.beans.Expression;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by srinand.pc on 22/12/15.
 */
public class StoreService {

    private StoreRepository storeRepository;
    private LocalityService localityService;
    private UserService userService;


    @Inject
    public StoreService(StoreRepository storeRepository, LocalityService localityService, UserService userService){
        this.storeRepository = storeRepository;
        this.localityService = localityService;
        this.userService = userService;
    }


    public Store addStore(Store store){


        Locality locality = store.getLocality();
        Locality locality1 = localityService.findLocality(locality.getName(),locality.getCityId());

        if(locality1 == null){
            locality1 = localityService.createLocality(locality);
        }
        store.setLocality(locality1);
        store.setLocalityId(locality1.getId());
        store.setStatus(Store.StoreStatus.ENABLED.toString());
        return storeRepository.create(store);
    }


    public Store addStore(Store store, boolean createUser){

        Locality locality = store.getLocality();
        Locality locality1 = localityService.findLocality(locality.getName(),locality.getCityId());

        if(locality1 == null){
            locality1 = localityService.createLocality(locality);
        }

        store.setLocalityId(locality1.getId());
        store.setLocality(null);
        store.setStatus(Store.StoreStatus.ENABLED.toString());

        if(createUser) {
            User user = new User();
            user.setName(store.getContactPerson());
            user.setStatus(User.UserStatus.ACTIVE.toString());
            user.setPhoneNo(store.getPhoneNo());
            user.setType(User.UserType.STORE.toString());
            user.setPassword("12345678");
            userService.createUser(user);
            user.getUserWallet().setUserId(user.getId());
            store.setUserId(user.getId());
        }
        return storeRepository.create(store);
    }

    public Store updateStore(Store store){

        String query = "select * from stores where id = :id";
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("id",store.getId());
        List<Store> persistenStores = storeRepository.getStores(query,parameters);
        Store persistentStore = persistenStores.get(0);
        persistentStore.setName(store.getName());
        persistentStore.setContactPerson(store.getContactPerson());
        persistentStore.setAddress(store.getAddress());
        persistentStore.setType(store.getType());
        persistentStore.setStatus(store.getStatus());

        Locality locality = store.getLocality();
        Locality locality1 = localityService.findLocality(locality.getName(),locality.getCityId());

        if(locality1 == null){
            locality1 = localityService.createLocality(locality);
        }
        persistentStore.setLocality(locality1);
        return storeRepository.update(persistentStore);
    }


    public Store getStoreById(Long id){

        List<SimpleExpression> criteria = new ArrayList<SimpleExpression>();
        criteria.add(Restrictions.eq("id",id));
        List<Store> persistenStores = storeRepository.getStores(criteria,10,0);
        return persistenStores.get(0);
    }

    public List<Store> getStoresByname(String name){

        String sqlQuery = "select * from stores where replace(name,' ','_') like :name and status = 'ENABLED'";
        Map<String,Object> parameters = new HashMap<String, Object>();
        name = name.replace(" ", "_");
        parameters.put("name","%"+name+"%");
        return storeRepository.getStores(sqlQuery,parameters);
    }

    public List<Store> getStoresByCityAndLocality(String name){

        String sqlQuery = "select * from stores where status = 'ENABLED' and ";
        Map<String,Object> parameters = new HashMap<String, Object>();
        name = name.replace(" ", "_");
        parameters.put("name","%"+name+"%");
        return storeRepository.getStores(sqlQuery,parameters);
    }

    public List<Store> getAllStores(int limit, int offset, boolean all, Long cityId){

        StringBuffer sqlQuery = new StringBuffer();
        if(all) {
            sqlQuery = new StringBuffer("select * from stores s join localities l on s.`locality_id` = l.id and l.city_id = :city_id limit :limit offset :offset");
        } else{
            sqlQuery = new StringBuffer("select * from stores s join localities l on s.`locality_id` = l.id and l.city_id = :city_id and status = 'ENABLED' limit :limit offset :offset");
        }
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("limit",new Integer(limit));
        parameters.put("offset",new Integer(offset));
        parameters.put("city_id",cityId);
        return storeRepository.getStores(sqlQuery.toString(),parameters);
    }


    public List<Store> getStoresForUser(User user, int limit, int offset){

        List<SimpleExpression> criteria = new ArrayList<SimpleExpression>();

        if(user.isStore()){
            criteria.add(Restrictions.eq("userId",user.getId()));
        } else if(user.isSalesPerson()){
            criteria.add(Restrictions.eq("salesUserId",user.getId()));
        }
        return storeRepository.getStores(criteria,limit,offset);

    }



}
