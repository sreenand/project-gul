package com.challur.services;

import com.challur.models.PurchaseOrderItem;
import com.challur.repository.interfaces.PurchaseOrderItemRepository;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hariprasanthac on 07/08/16.
 */
public class PurchaseOrderItemService {
    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final Logger logger = LoggerFactory.getLogger(PurchaseOrderItemService.class);
    private PurchaseOrderItemRepository purchaseOrderItemRepository;

    @Inject
    public PurchaseOrderItemService(PurchaseOrderItemRepository purchaseOrderItemRepository){
        this.purchaseOrderItemRepository = purchaseOrderItemRepository;
    }

    public List<PurchaseOrderItem> getOrderItems(Long orderId){
        StringBuffer sql = new StringBuffer("select p.* from purchase_order_items p join sourcing_point s on (p.sourcing_mkt_id = s.sourcing_mkt_id) join vendors v on (p.vendor_id = v.vendor_id) where p.po_id = :orderId");
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("orderId",orderId);
        return purchaseOrderItemRepository.find(sql.toString(),parameters);
    }

    public List<PurchaseOrderItem> getVendorOfMarket(Long orderId, Long marketId){
        StringBuffer sql = new StringBuffer("select p.* from purchase_order_items p join sourcing_point s on (p.sourcing_mkt_id = s.sourcing_mkt_id) join vendors v on (p.vendor_id = v.vendor_id && v.sourcing_mkt_id = :marketId) where p.po_id = :orderId");
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("orderId",orderId);
        parameters.put("marketId",marketId);
        return purchaseOrderItemRepository.find(sql.toString(),parameters);
    }

    public List<PurchaseOrderItem> getAllOrderItems(Long orderId, Long marketId, Long vendorId){
        StringBuffer sql = new StringBuffer("select p.* from purchase_order_items p join products s on (p.sku_code = s.id) where p.po_id = :orderId && p.sourcing_mkt_id = :marketId && p.vendor_id = :vendorId");
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("orderId",orderId);
        parameters.put("marketId",marketId);
        parameters.put("vendorId",vendorId);
        return purchaseOrderItemRepository.find(sql.toString(),parameters);
    }

    public Object updateOrderItem(Long id, Integer procured_qty, Double unit_price) {
        StringBuffer sql = new StringBuffer("update purchase_order_items p set p.procured_qty = :procured_qty, p.unit_price = :unit_price, p.total_amount = (:procured_qty * :unit_price) where p.id = :id");
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("id",id);
        parameters.put("procured_qty",procured_qty);
        parameters.put("unit_price",unit_price);
        purchaseOrderItemRepository.update(sql.toString(),parameters);
        return null;
    }

    public List<PurchaseOrderItem> getPOrderItems(Long orderId){
        StringBuffer sql = new StringBuffer("select p.* from purchase_order_items p join purchase_orders l on (p.po_id = l.po_id) join sourcing_point s on (p.sourcing_mkt_id = s.sourcing_mkt_id) join vendors v on (p.vendor_id = v.vendor_id) where p.po_id = :orderId");
        Map<String,Object> parameters = new HashMap<String, Object>();
        parameters.put("orderId",orderId);
        return purchaseOrderItemRepository.find(sql.toString(),parameters);
    }

    public Object updateOrderItems(ArrayList<PurchaseOrderItem> purchaseOrderItems) {
        for(PurchaseOrderItem  p:purchaseOrderItems) {
            purchaseOrderItemRepository.update(p);
        }
        return null;
    }
}
