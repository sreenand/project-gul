package com.challur;


import com.challur.Strategy.CustomNamingStrategy;
import com.challur.configuration.FarmersMarketConfiguration;
import com.challur.models.*;
import com.challur.providers.SessionUserProvider;
import com.google.inject.Stage;
import com.hubspot.dropwizard.guice.GuiceBundle;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.server.session.SessionHandler;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.hibernate.cfg.Configuration;

import javax.persistence.Cacheable;
import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

/**
 * Created by srinand.pc on 14/12/15.
 */
public class FarmersMarketApplication extends Application<FarmersMarketConfiguration> {

    private final HibernateBundle<FarmersMarketConfiguration> hibernate = new HibernateBundle<FarmersMarketConfiguration>(
            OrderItem.class, Wastage.class, Store.class, User.class, Product.class, Order.class, OrderPayment.class,
            City.class, Locality.class, ProductPricing.class, Category.class, SubCategory.class, ProductUnit.class,
            UserWallet.class, Transaction.class, TransactionItem.class, VariablePricing.class, Bucket.class, PurchaseOrder.class,
            PurchaseOrderItem.class, SourcingPoint.class, Vendor.class, OrderReqModel.class, Cart.class,VendorTransaction.class) {

        public DataSourceFactory getDataSourceFactory(FarmersMarketConfiguration configuration) {
            return configuration.getDataSourceFactory();
        }

        @Override
        public void configure(Configuration configuration) {
            configuration.setNamingStrategy(new CustomNamingStrategy());
            configuration.addPackage("com.challur.models");
        }
    };

    @Override
    public void initialize(Bootstrap<FarmersMarketConfiguration> bootstrap) {

        GuiceBundle<FarmersMarketConfiguration> guiceBundle = GuiceBundle.<FarmersMarketConfiguration>newBuilder()
                .addModule(new FarmersMarketModule()).addModule(new HibernateModule(hibernate))
                .enableAutoConfig(getClass().getPackage().getName())
                .setConfigClass(FarmersMarketConfiguration.class)
                .build(Stage.DEVELOPMENT);

        bootstrap.addBundle(new AssetsBundle("/public", "/assets"));
        bootstrap.addBundle(hibernate);
        bootstrap.addBundle(guiceBundle);

    }

    @Override
    public void run(FarmersMarketConfiguration configuration, Environment environment) throws Exception {

        final FilterRegistration.Dynamic cors =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        cors.setInitParameter("allowedOrigins", "*");
        cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin");
        cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");

        // Add URL mapping
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
        environment.getApplicationContext().setSessionHandler(new SessionHandler());
        environment.jersey().packages(SessionUserProvider.class.getPackage().getName());
    }


    public static void main(String[] args) throws Exception {
        new FarmersMarketApplication().run(args);
    }
}
