package com.challur.exceptions;

/**
 * Created by srinand.pc on 08/03/16.
 */
public enum AppError {
    NOT_FOUND,
    INTERNAL_ERROR,
    BAD_REQUEST,
    NOT_AUTHORIZED,
    LOCKED,
    FORBIDDEN
}
