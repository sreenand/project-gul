package com.challur.exceptions;

/**
 * Created by srinand.pc on 08/03/16.
 */

public class FarmersMarketException extends RuntimeException   {
    public FarmersMarketException(String message)  {
        super(message);
    }

    public FarmersMarketException(String message, Throwable t) {
        super(message, t);
    }
}
