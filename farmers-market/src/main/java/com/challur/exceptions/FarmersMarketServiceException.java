package com.challur.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by srinand.pc on 08/03/16.
 */
public class FarmersMarketServiceException extends WebApplicationException {



    public FarmersMarketServiceException(Integer responseCode, AppError errorCode, String errorMessage) {
        super(Response.status(responseCode).entity(getError(errorCode, errorMessage)).build());
    }

    private static Map<String, String> getError(AppError errorCode, String errorMessage) {
        Map<String, String> error = new HashMap<String, String>();
        error.put("CODE", errorCode.name());
        error.put("MESSAGE", errorMessage);
        return error;
    }


}
