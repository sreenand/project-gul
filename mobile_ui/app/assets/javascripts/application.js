// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

var payload_for_demand = {};
$( document ).on( "pagecreate", "#store_view", function() {
    $( "#autocomplete" ).on( "filterablebeforefilter", function ( e, data ) {
        var $ul = $( this ),
            $input = $( data.input ),
            value = $input.val(),
            html = "";
        $ul.html( "" );
        if ( value && value.length > 2 ) {
            $ul.html( "<li><div class='ui-loader'><span class='ui-icon ui-icon-loading'></span></div></li>" );
            $ul.listview( "refresh" );
            $.ajax({
                url: "/store",
                dataType: "json",
                data: {
                    name: $input.val()
                },

                success:  function(response){
                    $ul.css('display','block');
                    $.each( response, function ( i, val ) {
                        html += "<li> <a href onclick =\"javascript: populateFields('"+ val.id +"','"+ val.name+"','"+ val.locality+"','"+val.phoneNo+"','"+val.address+"')\"> "+val.name+" </a> </li>";
                    });
                    $ul.html( html );
                    $ul.listview( "refresh" );
                    $ul.trigger( "updatelayout");
                }
            });
        }
    });
});


function populateFields(id, name, locality, phonenumber, address, element){

    $('#storename').val(name);
    $('#store_id').html(id);
    $('#storeId').val(id);
    $('#store_locality').html(locality);
    $('#store_phonenumber').html(phonenumber);
    $('#store_address').html(address);
    $( "#autocomplete" ).css('display','none');
    $('#demand_wastage_div').css('display','none');
}

    function add_demand() {
        var fruit_vegetable = (($("#fruit_vegetable").val()).toUpperCase() == "YES") ? 1 : 0;
        var payload = {};

        payload["storeId"] = $("#storeId").val();
        payload["skuCode"] = $("#skucode").val();
        payload["quantity"] = $("#quantity").val();
        payload["vegetableFruitFlag"] = fruit_vegetable;

        jQuery.ajax({
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'url': window.location.origin + "/demand/add",
            'Content-Type': 'application/json',
            'data': JSON.stringify(payload),
            'type': 'POST',
            'dataType': 'json',
            'success': function (response) {
                alert("Your demand has been accepted");
            },
            'error': function () {
                alert("Sorry something went wrong terribly");
            }
        });

    }

    function add_demands() {

        var payload = []
        $.each(payload_for_demand, function(key, value){
            payload.push(value);
        });
        console.log(JSON.stringify(payload));
        jQuery.ajax({
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'url': window.location.origin + "/demand/add",
            'Content-Type': 'application/json',
            'data': JSON.stringify(payload),
            'type': 'POST',
            'dataType': 'json',
            'success': function (response) {
                alert("Your demand has been accepted");
            },
            'error': function () {
                alert("Sorry something went wrong terribly");
            }
        });

    }

    function showDemandWastage(){
        var html = "";
        jQuery.ajax({
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'url': window.location.origin + "/skus",
            'dataType': 'json',
            'success': function(response){
                $.each( response, function ( i, val ) {
                    html += '<div class="ui-grid-c"> ' +
                        '<input type=text class = "ui-block-a" id = "sku_name_'+val.id+'"  value="'+val.name+'", readonly style="border:none"/> ' +
                        '<input type="text" class = "ui-block-b" id = "sku_quantity_'+val.id+'" value = "" placeholder="quantity"/> ' +
                        '<input type="checkbox" class = "ui-block-c" id = "sku_flag_'+val.id+'"/>' +
                        '<input type="checkbox" class = "ui-block-d" id = "sku_checkbox_'+val.id+'" onclick="javascript:addtoPayload(\''+val.id+'\')" /> </div>';
                });
                $('#sku_div').html(html);
            },
            'error':function (){
                alert("Sorry something went wrong terribly");
            }
        });
        $('#demand_wastage_div').css('display','block');
    }





    function addtoPayload(skudId){


         if(!$('#sku_checkbox_' + skudId).is(":checked")) {
             delete[payload_for_demand[val.id]];
             return;
         }
        var qunantity = $('#sku_quantity_' + skudId).val();
        var flag = $('#sku_flag_' + skudId).is(":checked");
        var storeId = $('#storeId').val();
        addPayload(skudId, qunantity, flag, storeId);
    }




    function addPayload(skucode, quanitity, vegetableFlag, storeId){

        var demand = {};
        demand['skuCode'] = skucode;
        demand['storeId'] = storeId;
        demand['vegetableFruitFlag'] = vegetableFlag;
        demand['quantity'] = quanitity;
        payload_for_demand[skucode] = demand;
    }


    function add_wastage (){
        var fruit_vegetable = (($("#fruit_vegetable").val()).toUpperCase() == "YES") ? 1 : 0;
        var payload = {};

        payload["storeId"] = $("#storeId").val();
        payload["skucode"] = $("#skucode").val();
        payload["quantity"] = $("#quantity").val();
        payload["fruit_vegetable"] = fruit_vegetable;

        jQuery.ajax({
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'url': window.location.origin + "/wastage/add",
            'data': JSON.stringify(payload),
            'type':'post',
            'dataType': 'json',
            'success': function(response){
                alert("Wastage has been noted");

            },
            'error':function (){
                alert("Sorry something went wrong terribly");
            }
        });
    }
