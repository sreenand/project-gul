class ApplicationController < ActionController::Base
  include ApplicationHelper
  before_filter :set_config

  def index
    if session[:user].nil?
      render :template => 'application/login' if session[:user].nil?
    else
      render :template => 'store/store_view'
    end
  end
end
