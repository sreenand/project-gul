class StoreController < ApplicationController
  include RestHelper
  before_filter :set_config


  def index
     render :template => 'store/store_view'
  end

  def find_store
    name = params[:name]
    id = params[:id]
    locality = params[:locality]
    total_url = (@backend_url + "/stores")
    total_url = session_url(total_url) + "?"
    total_url = total_url + "id=#{id}&" if !id.nil?
    total_url = total_url + "name=#{name}&" if !name.nil?
    render :json => JSON.parse((RestClient.get (total_url)))
  end

end