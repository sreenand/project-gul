require 'restclient'
require 'json'

class DemandController < ApplicationController
  before_filter :set_config
  include RestHelper

  def demand_view
    response_body = RestClient.get session_url(@backend_url + "/demand/store/#{params[:storeId]}")
    @Demands = JSON.parse(response_body)
    render :template => 'demand/demand_view'
  end


  def demand_add_action
    response_body = RestClient.get session_url(@backend_url + "/demand/store/#{params[:storeId]}")
    @Demands = JSON.parse(response_body)
  end

  def demand_add_view
    render :template => 'demand/demand_add_view'
  end

  def wastage_add_view
    render :template => 'wastage/wastage_add_view'
  end

  def wastage_view
    response_body = RestClient.get session_url(@backend_url + "/wastage/store/#{params[:storeId]}")
    @Demands = JSON.parse(response_body)
  end

  def add_demand
    payload = params["demand"]["_json"]
    begin
      response_body = RestClient.post session_url(@backend_url + "/demand/add/bulk"), payload.to_json, :content_type => 'application/json'
    rescue Exception => e
      print e.message
    end

    render :json => {}
  end

  def add_wastage
    payload = params["_json"]
    response_body = RestClient.post session_url(@backend_url + "/wastage/add"), payload.to_json, :content_type => 'application/json'
    render :json => {}
  end

  def get_skus
    response_body = RestClient.get session_url(@backend_url + "/sku")
    render :json => JSON.parse(response_body)
  end
end