module SessionsHelper


  def login_user(login_request)
    response = RestClient.post @backend_url + "/users/login", login_request.to_json, :content_type => :json
    cookies = response.headers[:set_cookie][0]
    session_data = {}
    split_cookies = cookies.to_s.split(";")
    split_cookies.each do |split_cookie|
      if(split_cookie.include? "JSESSIONID" )
        session_data[:jsessionId] = split_cookie.split("=")[1]
        break;
      end
    end
    current_user = SessionUser.new
    current_user.username = login_request["username"]
    current_user.session_data = session_data
    session[:user] = current_user
  end

  def get_current_user
     return session[:user]
  end

end