module RestHelper

  def session_url(given_url)
    raise("User Not Logged In") if(session[:user].nil?)
    print session[:user].to_s
    return given_url + ";jsessionid=#{session[:user]["session_data"]["jsessionId"]}"
  end

end