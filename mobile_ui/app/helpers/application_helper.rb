module ApplicationHelper

  def set_config
    @config = MobileUi::Application.config
    @backend_url = @config.myKey["backendUrl"]
  end

  def check_logged_in
    if(session[:user].nil?)
      render :template => 'default/index'
    end
  end
end
